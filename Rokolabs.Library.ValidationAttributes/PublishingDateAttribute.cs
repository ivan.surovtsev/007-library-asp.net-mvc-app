﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Rokolabs.Library.Validation.Attributes
{
    public class PublishingDateAttribute : ValidationAttribute, IClientValidatable
    {    
        private readonly string dateOfApplication;
        private DateTime? compareDate;

        public PublishingDateAttribute(string dateOfApplication) 
            : base("It must be >= then date of application and <= then current date")
        {
            this.dateOfApplication = dateOfApplication;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(ErrorMessage);
            rule.ValidationParameters.Add("comparedate", compareDate);
            rule.ValidationParameters.Add("currdate", DateTime.Now.ToString("yyyy-MM-dd"));
            rule.ValidationType = "publyearval";
            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {            
            var publicationDate = (DateTime?)value;

            if (!publicationDate.HasValue)
            {
                return ValidationResult.Success;
            }

            var property = validationContext.ObjectType.GetProperty(dateOfApplication);

            if (property == null)
                throw new ArgumentException("Property with this name not found");

            compareDate = (DateTime?)property.GetValue(validationContext.ObjectInstance);

            if (publicationDate < compareDate || publicationDate.Value > DateTime.Now)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}
