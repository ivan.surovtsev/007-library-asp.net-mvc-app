﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Rokolabs.Library.Validation.Attributes
{
    public class LastNameAttribute : ValidationAttribute, IClientValidatable
    {
        private Regex pattern = new Regex(@"(^[A-Z][a-z]*((-|')([A-Z]))?((\s[a-z]{2,3}\s)([A-Z]))?[a-z]*$)|(^[А-ЯЁ][а-яё]*((-|')([А-ЯЁ]))?((\s[а-яё]{2,3}\s)([А-ЯЁ]))?[а-яё]*$)");

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(metadata.GetDisplayName());
            rule.ValidationParameters.Add("pattern", pattern);
            rule.ValidationType = "lastnameval";
            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var lastName = value as string;

            if (lastName == null)
            {
                return ValidationResult.Success;
            }

            if (lastName.Length > 200)
            {
                return new ValidationResult("Last name must be less or equal 200 characters");
            }               

            if (!pattern.IsMatch(lastName))
            {
                return new ValidationResult("Last name isn't correct");
            }

            return ValidationResult.Success;
        }
    }
}
