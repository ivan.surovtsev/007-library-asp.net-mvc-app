﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Rokolabs.Library.Validation.Attributes
{
    public class FirstNameAttribute : ValidationAttribute, IClientValidatable
    {
        private Regex pattern = new Regex(@"^(([A-Z][a-z]*(-[A-Z])?[a-z]*)$|([А-ЯЁ][а-яё]*(-[А-ЯЁ])?[а-яё]*))$");

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(metadata.GetDisplayName());
            rule.ValidationParameters.Add("pattern", pattern);
            rule.ValidationType = "firstnameval";
            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var firstName = value as string;

            if (firstName == null)
            {
                return ValidationResult.Success;
            }

            if (firstName.Length > 50)
            {
                return new ValidationResult("First name must be less or equal 50 characters");
            }            

            if (!pattern.IsMatch(firstName))
            {
                return new ValidationResult("First name isn't correct");
            }

            return ValidationResult.Success;
        }
    }
}
