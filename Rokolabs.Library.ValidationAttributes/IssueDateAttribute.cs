﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Rokolabs.Library.Validation.Attributes
{
    public class IssueDateAttribute : ValidationAttribute, IClientValidatable
    {
        private short? publyear;

        public IssueDateAttribute()
            : base("Issue date don't match with publication year")
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {    
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(ErrorMessage);
            rule.ValidationParameters.Add("publyear", publyear);
            rule.ValidationType = "issudateval";
            yield return rule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            short? checkVal = value as short?;

            if (checkVal == null)
            {
                return ValidationResult.Success;
            }

            var property = validationContext.ObjectType.GetProperty("PublicationYear");

            if (property == null)
                throw new ArgumentException("Property with this name not found");

            publyear = (short?)property.GetValue(validationContext.ObjectInstance);

            if (checkVal != publyear)
            {
                return new ValidationResult(ErrorMessage);
            }

            return ValidationResult.Success;
        }
    }
}
