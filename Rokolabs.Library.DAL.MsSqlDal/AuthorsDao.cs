﻿using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class AuthorsDao : IAuthorsDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;

        public bool AddAuthor(Author author)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "[dbo].[AddAuthor]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@firstName", author.FirstName);
                command.Parameters.AddWithValue("@lastName", author.LastName == null ? DBNull.Value : (object)author.LastName);

                try
                {
                    connection.Open();

                    return command.ExecuteNonQuery() != -1;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "AuthorsDao", "AddAuthor", ex.Message);
                }

                return false;
            }
        }

        public HashSet<Author> GetAllAuthors()
        {
            HashSet<Author> authors = new HashSet<Author>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;

                command.CommandText = "[dbo].[GetAllAuthors]";

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Author author = new Author
                            {
                                AuthorID = (int)reader["AuthorID"],
                                FirstName = reader["FirstName"].ToString(),
                                LastName = reader["LastName"].ToString()
                            };

                            authors.Add(author);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "AuthorsDao", "GetAllAuthors", ex.Message);
                }
            }

            return authors;
        }

        public Author GetAuthorById(int id)
        {
            Author author = null;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = "[dbo].[GetAuthorById]",
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@authorId", id);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            author = new Author
                            {
                                AuthorID = (int)reader["AuthorID"],
                                FirstName = reader["FirstName"].ToString(),
                                LastName = reader["LastName"].ToString()
                            };
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "AuthorsDao", "GetAuthorById", ex.Message);
                }
            }

            return author;
        }
    }
}
