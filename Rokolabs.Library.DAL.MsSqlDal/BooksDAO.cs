﻿using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class BooksDao : IBooksDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;        

        public Book AddBook(Book book)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("[dbo].[AddBook]", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@name", book.Name);
                command.Parameters.AddWithValue("@publicationPlace", string.IsNullOrEmpty(book.PublicationPlace) ? DBNull.Value : (object)book.PublicationPlace);
                command.Parameters.AddWithValue("@publishingHouse", string.IsNullOrEmpty(book.PublishingHouse) ? DBNull.Value : (object)book.PublishingHouse);
                command.Parameters.AddWithValue("@publicationYear", book.PublicationYear.HasValue ? (object)book.PublicationYear : DBNull.Value);
                command.Parameters.AddWithValue("@numberOfPages", book.NumberOfPages.HasValue ? (object)book.NumberOfPages : DBNull.Value);
                command.Parameters.AddWithValue("@notes", string.IsNullOrEmpty(book.Notes) ? DBNull.Value : (object)book.Notes);
                command.Parameters.AddWithValue("@ISBN", string.IsNullOrEmpty(book.ISBN) ? DBNull.Value : (object)book.ISBN);
                command.Parameters.AddWithValue("@hash", book.Hash);
                command.Parameters.AddWithValue("@entryID", book.EntryId);

                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("FirstName", typeof(string)));
                table.Columns.Add(new DataColumn("LastName", typeof(string)));

                foreach (var author in book.Authors)
                {
                    table.Rows.Add(
                        author.FirstName == null ? DBNull.Value : (object)author.FirstName, 
                        author.LastName == null ? DBNull.Value : (object)author.LastName
                        );
                }

                SqlParameter parameter = command.Parameters.AddWithValue("@authors", table);
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "dbo.AuthorNames";

                List<Author> listAuthors = book.Authors.ToList();
                int idx = 0;

                try
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            return null;
                        }

                        while (reader.Read())
                        {
                            book.EntryId = (int)reader["Entry_ID"];
                            listAuthors[idx++].AuthorID = (int)reader["Author_ID"];
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "AddBook", ex.Message);
                }                
            }

            if (book.EntryId != 0)
            {
                return book;
            }

            return null;            
        }
       
        public bool DeleteBook(Book book)
        {
            int numOfRowsAffected = -1;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();

                command.Connection = connection;
                command.CommandText = "[dbo].[DeleteBook]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@entryID", book.EntryId);

                try
                {
                    connection.Open();
                    numOfRowsAffected = command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "DeleteBook", ex.Message);
                }                
            }           

            return numOfRowsAffected != -1;
        }

        public bool DeleteBookById(int id)
        {
            var book = GetBookById(id);

            if (book == null)
            {
                return false;
            }

            return DeleteBook(book);
        }

        public Book GetBookById(int id)
        {
            HashSet<Book> books = new HashSet<Book>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure,

                    CommandText = "[dbo].[GetBookById]"
                };

                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            books = GetBooksFromReader(reader, books);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "GetBookById", ex.Message);
                }
            }

            return books.FirstOrDefault(e => e.EntryId == id);
        }

        public HashSet<Book> GetAllBooks()
        {
            HashSet<Book> books = new HashSet<Book>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;

                command.CommandText = "[dbo].[GetAllBooks]";

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            books = GetBooksFromReader(reader, books, true);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "GetAllBooks", ex.Message);
                }
            }

            return books;
        }

        public Page<Book> GetBooksForPage(Page<Book> page)
        {
            HashSet<Book> books = new HashSet<Book>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[dbo].[GetBooksForPage]";
                command.Parameters.AddWithValue("@page", page.PageNumber);
                command.Parameters.AddWithValue("@pageSize", page.PageSize);
                command.Parameters.AddWithValue("@name", page.NameOfObject);
                command.Parameters.AddWithValue("@sort", page.Sort);
                command.Parameters.AddWithValue("@author", page.Author);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            books = GetBooksFromReader(reader, books, true);
                        }
                        page.Objects = books;
                        reader.NextResult();
                        while (reader.Read())
                        {
                            page.TotalCount = (int)reader["TotalCount"];
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "GetBooksForPage", ex.Message);
                }
            }

            return page;
        }

        public HashSet<Book> GetBooksByAuthor(string firstName, string lastName)
        {
            HashSet<Book> books = new HashSet<Book>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("[dbo].[GetBooksByAuthor]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@firstName", firstName);
                command.Parameters.AddWithValue("@lastName", lastName);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            books = GetBooksFromReader(reader, books);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "GetBooksByAuthor", ex.Message);
                }
            }

            return books; 
        }

        public HashSet<Book> GetBooksByPublishingHouse(string publishingHouseName)
        {
            HashSet<Book> books = new HashSet<Book>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("[dbo].[GetBooksByPublishingHouse]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@publishingHouseForSearching", publishingHouseName);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            books = GetBooksFromReader(reader, books);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "GetBooksByPublishingHouse", ex.Message);
                }
            }          

            return books; 
        }

        public HashSet<Book> GetBooksByName(string name)
        {
            HashSet<Book> books = new HashSet<Book>();
          
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@nameForSearching", name);
                command.CommandText = "[dbo].[GetBooksByName]";

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            books = GetBooksFromReader(reader, books);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "BooksDao", "GetBooksByName", ex.Message);
                }                
            }

            return books; 
        }

        private HashSet<Book> GetBooksFromReader(SqlDataReader reader, HashSet<Book> books, bool withAuthors = false)
        {
            Book book = new Book
            {
                EntryId = (int)reader["EntryID"],
                Name = reader["Name"].ToString(),
                PublicationPlace = reader["PublicationPlace"].ToString(),
                PublishingHouse = reader["PublishingHouse"].ToString(),
                PublicationYear = Convert.IsDBNull(reader["PublicationYear"]) ? null : (short?)Convert.ToInt16(reader["PublicationYear"]),
                NumberOfPages = Convert.IsDBNull(reader["NumberOfPages"]) ? 0 : Convert.ToInt32(reader["NumberOfPages"]),
                Notes = reader["Notes"] != DBNull.Value ? reader["Notes"].ToString() : null,
                ISBN = reader["ISBN"] != DBNull.Value ? reader["ISBN"].ToString() : null,
                Authors = new HashSet<Author>()
            };

            if (!withAuthors || reader["FirstName"] == DBNull.Value)
            {
                books.Add(book);
                return books;
            }

            Author author = new Author
            {              
                FirstName = reader["FirstName"].ToString(),
                LastName = reader["LastName"].ToString()
            };

            if (books.Contains(book))
            {
                books.TryGetValue(book, out book);              
                book.Authors.Add(author);
            }
            else
            {
                book.Authors.Add(author);
                books.Add(book);
            }

            return books;
        }              
    }
}
