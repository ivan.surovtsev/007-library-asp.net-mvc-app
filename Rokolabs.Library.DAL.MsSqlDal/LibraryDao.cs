﻿using Rokolabs.Library.CommonObjects;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class LibraryDao : ILibraryDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;

        public Page<LibraryEntry> GetCatalogForPage(Page<LibraryEntry> page)
        {
            page.Objects = new HashSet<LibraryEntry>();

            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(ConnectionString);
                {
                    string commandText = "[dbo].[GetCatalogForPage]";
                    int lastBookId = 0;
                    SqlCommand cmd = new SqlCommand(commandText, connection);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@page", page.PageNumber);
                    cmd.Parameters.AddWithValue("@pageSize", page.PageSize);
                    cmd.Parameters.AddWithValue("@name", page.NameOfObject);
                    cmd.Parameters.AddWithValue("@sort", page.Sort);
                    cmd.Parameters.AddWithValue("@author", page.Author);
                    connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (lastBookId == (int)reader["EntryID"])
                            {
                                Book book = (Book)page.Objects.Last();
                                book.Authors.Add(new Author()
                                {
                                    FirstName = reader["AuthorName"].ToString(),
                                    LastName = reader["AuthorSurname"].ToString()
                                });
                            }
                            else
                            {
                                if (reader["ObjectType"].ToString() == LibraryObjects.Book.ToString())
                                {
                                    Book book = new Book()
                                    {
                                        EntryId = (int)reader["EntryID"],
                                        Name = reader["Name"].ToString(),
                                        PublicationYear = (short)(int)reader["Year"],
                                        NumberOfPages = reader["NumberOfPages"] != DBNull.Value ? (int)reader["NumberOfPages"] : (int?)null,
                                        ISBN = reader["Identifier"] != DBNull.Value ? reader["Identifier"].ToString() : null
                                    };
                                    if (reader["AuthorName"] != null)
                                    {
                                        book.Authors = new HashSet<Author>()
                                            {
                                                new Author()
                                                {
                                                    FirstName = reader["AuthorName"].ToString(),
                                                    LastName = reader["AuthorSurname"].ToString()
                                                }
                                            };
                                        lastBookId = book.EntryId;
                                    }
                                    page.Objects.Add(book);
                                }
                                else if (reader["ObjectType"].ToString() == LibraryObjects.Newspaper.ToString())
                                {
                                    page.Objects.Add(new Newspaper()
                                    {
                                        EntryId = (int)reader["EntryId"],
                                        Name = reader["Name"].ToString(),
                                        ISSN = reader["Identifier"] != DBNull.Value ? reader["Identifier"].ToString() : null,
                                        PublicationYear = (short)(int)reader["Year"],
                                        NumberOfPages = reader["NumberOfPages"] != DBNull.Value ? (int)reader["NumberOfPages"] : (int?)null,
                                        IssueNumber = reader["IssueNumber"].ToString()
                                    });
                                }
                                else if (reader["ObjectType"].ToString() == LibraryObjects.Patent.ToString())
                                {
                                    page.Objects.Add(new Patent()
                                    {
                                        EntryId = (int)reader["EntryId"],
                                        Name = reader["Name"].ToString(),
                                        NumberOfPages = reader["NumberOfPages"] != DBNull.Value ? (int)reader["NumberOfPages"] : (int?)null,
                                        PublishingDate = DateTime.Parse(reader["PublishingDate"].ToString()),
                                        RegistrationNumber = reader["Identifier"] != DBNull.Value ? Convert.ToInt32(reader["Identifier"].ToString()) : (int?)null
                                    });
                                }
                            }
                        }
                        reader.NextResult();
                        while (reader.Read())
                        {
                            page.TotalCount = (int)reader["TotalCount"];
                        }
                    }
                }
                return page;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Error, "DAL", "LibraryDao", "GetCatalogForPage", ex.Message);
                throw ex;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
        }

        public Statistics GetCountOfActiveAndDeletedEntities()
        {
            Statistics statistics = null;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "[dbo].[GetCountOfActiveAndDeletedEntities]"
                };

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            statistics = new Statistics
                            {
                                CountOfActiveBooks = (int)reader["CountOfActiveBooks"],
                                CountOfActiveNewspapers = (int)reader["CountOfActiveNewspapers"],
                                CountOfActivePatents = (int)reader["CountOfActivePatents"],
                                CountOfDeletedBooks = (int)reader["CountOfDeletedBooks"],
                                CountOfDeletedNewspapers = (int)reader["CountOfDeletedNewspapers"],
                                CountOfDeletedPatents = (int)reader["CountOfDeletedPatents"],
                                CountOfActiveEntities = (int)reader["CountOfActiveEntities"],
                                CountOfDeletedEntities = (int)reader["CountOfDeletedEntities"]
                            };
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "LibraryDao", "GetCountOfActiveAndDeletedEntities", ex.Message);
                }
            }

            return statistics;
        }
    }
}
