﻿using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class CountriesDao : ICountriesDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;

        public bool AddCountry(Country country)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "[dbo].[AddCountry]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@name", country.CountryName);

                try
                {
                    connection.Open();

                    return command.ExecuteNonQuery() != -1;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "CountriesDao", "AddCountry", ex.Message);
                }

                return false;
            }
        }

        public HashSet<Country> GetAllCountries()
        {
            HashSet<Country> countries = new HashSet<Country>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure,

                    CommandText = "[dbo].[GetAllCountries]"
                };

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Country country = new Country
                            {
                                CountryId = (int)reader["CountryId"],
                                CountryName = reader["Name"].ToString()
                            };

                            countries.Add(country);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "CountriesDao", "GetAllCountries", ex.Message);
                }
            }

            return countries;
        }
    }
}
