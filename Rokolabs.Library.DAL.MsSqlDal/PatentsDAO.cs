﻿using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Rokolabs.Library.Tools;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class PatentsDao : IPatentsDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;       

        public Patent AddPatent(Patent patent)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("[dbo].[AddPatent]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@name", patent.Name);
                command.Parameters.AddWithValue("@numberOfPages", patent.NumberOfPages.HasValue ? (object)patent.NumberOfPages : DBNull.Value);
                command.Parameters.AddWithValue("@notes", string.IsNullOrEmpty(patent.Notes) ? DBNull.Value : (object)patent.Notes);
                command.Parameters.AddWithValue("@country", string.IsNullOrEmpty(patent.Country) ? DBNull.Value : (object)patent.Country);
                command.Parameters.AddWithValue("@registrationNumber", (object)patent.RegistrationNumber ?? DBNull.Value);
                command.Parameters.AddWithValue("@dateOfApplication", (object)patent.DateOfApplication ?? DBNull.Value);
                command.Parameters.AddWithValue("@publishingDate", (object)patent.PublishingDate ?? DBNull.Value);
                command.Parameters.AddWithValue("@hash", patent.Hash);
                command.Parameters.AddWithValue("@entryID", patent.EntryId);

                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("FirstName", typeof(string)));
                table.Columns.Add(new DataColumn("LastName", typeof(string)));

                foreach (var author in patent.Authors)
                {
                    table.Rows.Add(author.FirstName, (object)author.LastName ?? DBNull.Value);
                }

                SqlParameter parameter = command.Parameters.AddWithValue("@inventors", table);
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.TypeName = "dbo.AuthorNames";

                List<Author> listAuthors = patent.Authors.ToList();
                int idx = 0;

                try
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            return null;
                        }

                        while (reader.Read())
                        {
                            patent.EntryId = (int)reader["Entry_ID"];
                            listAuthors[idx++].AuthorID = (int)reader["Author_ID"];
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "PatentsDao", "AddPatent", ex.Message);
                }
            }

            if (patent.EntryId != 0)
            {
                return patent;
            }

            return null;
        }

        public bool DeletePatent(Patent patent)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = "[dbo].[DeletePatent]",
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@entryID", patent.EntryId);
                command.Parameters.AddWithValue("@entryHash", patent.GetHashCode());

                try
                {
                    connection.Open();
                    return command.ExecuteNonQuery() != -1;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "PatentsDao", "DeletePatent", ex.Message);
                }
            }
           
            return false;
        }

        public bool DeletePatentById(int id)
        {
            var patent = GetPatentById(id);

            if (patent == null)
            {
                return false;
            }

            return DeletePatent(patent);
        }

        public Patent GetPatentById(int id)
        {
            HashSet<Patent> patents = new HashSet<Patent>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure,

                    CommandText = "[dbo].[GetPatentById]"
                };

                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            patents = GetPatentsFromReader(reader, patents);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "PatentsDao", "GetPatentById", ex.Message);
                }                
            }

            return patents.FirstOrDefault(e => e.EntryId == id);
        }

        public HashSet<Patent> GetAllPatents()
        {
            HashSet<Patent> patents = new HashSet<Patent>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "[dbo].[GetAllPatents]"
                };

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            patents = GetPatentsFromReader(reader, patents, true);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "PatentsDao", "GetAllPatents", ex.Message);
                }
            }

            return patents;
        }

        public Page<Patent> GetPatentsForPage(Page<Patent> page)
        {
            HashSet<Patent> patents = new HashSet<Patent>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[dbo].[GetPatentsForPage]";
                command.Parameters.AddWithValue("@page", page.PageNumber);
                command.Parameters.AddWithValue("@pageSize", page.PageSize);
                command.Parameters.AddWithValue("@name", page.NameOfObject);
                command.Parameters.AddWithValue("@sort", page.Sort);
                command.Parameters.AddWithValue("@author", page.Author);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            patents = GetPatentsFromReader(reader, patents, true);
                        }
                        page.Objects = patents;
                        reader.NextResult();
                        while (reader.Read())
                        {
                            page.TotalCount = (int)reader["TotalCount"];
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "PatentsDao", "GetPatentsForPage", ex.Message);
                }
            }

            return page;
        }

        public HashSet<Patent> GetPatentByName(string name)
        {
            HashSet<Patent> patents = new HashSet<Patent>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@nameForSearching", name);
                command.CommandText = "[dbo].[GetPatentsByName]";

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            patents = GetPatentsFromReader(reader, patents);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "PatentsDao", "GetBooksByName", ex.Message);
                }               
            }

            return patents;
        }

        public HashSet<Patent> GetPatentsByInventor(string firstName, string lastName)
        {
            HashSet<Patent> patents = new HashSet<Patent>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("[dbo].[GetPatentsByInventor]", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@firstName", firstName);
                command.Parameters.AddWithValue("@lastName", lastName);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            patents = GetPatentsFromReader(reader, patents);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "PatentsDao", "GetPatentsByInventor", ex.Message);
                }
            }            

            return patents;
        }

        private HashSet<Patent> GetPatentsFromReader(SqlDataReader reader, HashSet<Patent> patents, bool withInventors = false)
        {
            Patent patent = new Patent
            {
                EntryId = (int)reader["EntryID"],
                Name = reader["Name"].ToString(),
                NumberOfPages = Convert.IsDBNull(reader["NumberOfPages"]) ? 0 : Convert.ToInt32(reader["NumberOfPages"]),
                Notes = reader["Notes"] != null ? reader["Notes"].ToString() : null,
                Country = reader["Country"].ToString(),
                RegistrationNumber = Convert.IsDBNull(reader["RegistrationNumber"]) ? null : (int?)Convert.ToInt32(reader["RegistrationNumber"]),
                DateOfApplication = Convert.IsDBNull(reader["DateOfApplication"]) ? null : (DateTime?)Convert.ToDateTime(reader["DateOfApplication"]),
                PublishingDate = Convert.IsDBNull(reader["PublishingDate"]) ? null : (DateTime?)Convert.ToDateTime(reader["PublishingDate"]),
                Authors = new HashSet<Author>()
            };

            if (!withInventors || reader["FirstName"] == DBNull.Value)
            {
                patents.Add(patent);
                return patents;
            }

            Author author = new Author
            {                
                FirstName = reader["FirstName"].ToString(),
                LastName = reader["LastName"].ToString()
            };

            if (patents.Contains(patent))
            {
                patents.TryGetValue(patent, out patent);                
                patent.Authors.Add(author);
            }
            else
            {
                patent.Authors.Add(author);
                patents.Add(patent);
            }

            return patents;
        }        
    }
}
