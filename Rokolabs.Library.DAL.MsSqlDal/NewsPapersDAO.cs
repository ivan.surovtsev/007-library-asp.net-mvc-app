﻿using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class NewspapersDao : INewspapersDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;       

        public Newspaper AddNewspaper(Newspaper newspaper, bool isIssue)
        {
            object id = null;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("[dbo].[AddNewspaper]", connection)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@name", newspaper.Name);
                command.Parameters.AddWithValue("@publicationPlace", string.IsNullOrEmpty(newspaper.PublicationPlace) ? DBNull.Value : (object)newspaper.PublicationPlace);
                command.Parameters.AddWithValue("@publishingHouse", string.IsNullOrEmpty(newspaper.PublishingHouse) ? DBNull.Value : (object)newspaper.PublishingHouse);
                command.Parameters.AddWithValue("@publicationYear", newspaper.PublicationYear.HasValue ? (object)newspaper.PublicationYear : DBNull.Value);
                command.Parameters.AddWithValue("@numberOfPages", newspaper.NumberOfPages.HasValue ? (object)newspaper.NumberOfPages : DBNull.Value);
                command.Parameters.AddWithValue("@notes", string.IsNullOrEmpty(newspaper.Notes) ? DBNull.Value : (object)newspaper.Notes);
                command.Parameters.AddWithValue("@ISSN", string.IsNullOrEmpty(newspaper.ISSN) ? DBNull.Value : (object)newspaper.ISSN);
                command.Parameters.AddWithValue("@issueNumber", (object)newspaper.IssueNumber ?? DBNull.Value);
                command.Parameters.AddWithValue("@issueDate", (object)newspaper.IssueDate ?? DBNull.Value);
                command.Parameters.AddWithValue("@hash", newspaper.Hash);
                command.Parameters.AddWithValue("@entryID", newspaper.EntryId);
                command.Parameters.AddWithValue("@isIssue", isIssue == true ? 1 : 0);

                try
                {
                    connection.Open();
                    id = command.ExecuteScalar();
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "NewspapersDao", "AddNewspaper", ex.Message);
                }
            }

            if (id != null)
            {
                newspaper.EntryId = int.Parse(id.ToString());                 
              
                return newspaper;
            }

            return null;
        }

        public bool DeleteNewspaper(Newspaper newspaper)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = "[dbo].[DeleteNewspaper]",
                    CommandType = System.Data.CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@entryID", newspaper.EntryId);
                command.Parameters.AddWithValue("@entryHash", newspaper.GetHashCode());

                try
                {
                    connection.Open();
                    return command.ExecuteNonQuery() != -1;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "NewspapersDao", "DeleteNewspaper", ex.Message);
                }
            }           

            return false;
        }

        public bool DeleteNewspaperById(int id)
        {
            var newspaper = GetNewspaperById(id);

            if (newspaper == null)
            {
                return false;
            }

            return DeleteNewspaper(newspaper);
        }

        public Newspaper GetNewspaperById(int id)
        {
            HashSet<Newspaper> newspapers = new HashSet<Newspaper>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = System.Data.CommandType.StoredProcedure,

                    CommandText = "[dbo].[GetNewspaperById]"
                };
                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GetNewspapersFromReader(reader, newspapers);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "NewspapersDao", "GetNewspaperById", ex.Message);
                }                
            }

            return newspapers.FirstOrDefault(e => e.EntryId == id);
        }

        public HashSet<Newspaper> GetAllNewspapers()
        {
            HashSet<Newspaper> newspapers = new HashSet<Newspaper>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "[dbo].[GetAllNewspapers]";

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            newspapers = GetNewspapersFromReader(reader, newspapers);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "NewspapersDao", "GetAllNewspapers", ex.Message);
                }                
            }

            return newspapers;
        }

        public Page<Newspaper> GetNewspapersForPage(Page<Newspaper> page)
        {
            HashSet<Newspaper> newspapers = new HashSet<Newspaper>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "[dbo].[GetNewspapersForPage]";
                command.Parameters.AddWithValue("@page", page.PageNumber);
                command.Parameters.AddWithValue("@pageSize", page.PageSize);
                command.Parameters.AddWithValue("@name", page.NameOfObject);
                command.Parameters.AddWithValue("@sort", page.Sort);

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            newspapers = GetNewspapersFromReader(reader, newspapers);
                        }
                        page.Objects = newspapers;
                        reader.NextResult();
                        while (reader.Read())
                        {
                            page.TotalCount = (int)reader["TotalCount"];
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "NewspapersDao", "GetNewspapersForPage", ex.Message);
                }
            }

            return page;
        }

        public HashSet<Newspaper> GetNewspapersByName(string name)
        {
            HashSet<Newspaper> newspapers = new HashSet<Newspaper>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@nameForSearching", name);
                command.CommandText = "[dbo].[GetNewspapersByName]";

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            newspapers = GetNewspapersFromReader(reader, newspapers);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "NewsPapersDAO", "GetNewspapersByName", ex.Message);
                }
            }

            return newspapers;
        }

        private HashSet<Newspaper> GetNewspapersFromReader(SqlDataReader reader, HashSet<Newspaper> newspapers)
        {
            Newspaper newspaper = new Newspaper
            {
                EntryId = (int)reader["EntryID"],
                Name = reader["Name"].ToString(),
                PublicationPlace = reader["PublicationPlace"].ToString(),
                PublishingHouse = reader["PublishingHouse"].ToString(),
                PublicationYear = Convert.IsDBNull(reader["PublicationYear"]) ? null : (short?)Convert.ToInt16(reader["PublicationYear"]),
                NumberOfPages = Convert.IsDBNull(reader["NumberOfPages"]) ? 0 : Convert.ToInt32(reader["NumberOfPages"]),
                Notes = reader["Notes"] != DBNull.Value ? reader["Notes"].ToString() : null,
                ISSN = reader["ISSN"] != DBNull.Value ? reader["ISSN"].ToString() : null,
                IssueNumber = reader["IssueNumber"] != DBNull.Value ? reader["IssueNumber"].ToString() : null,
                IssueDate = Convert.IsDBNull(reader["IssueDate"]) ? null : (DateTime?)Convert.ToDateTime(reader["IssueDate"])
            };

            newspapers.Add(newspaper);

            return newspapers;
        }       
    }
}
