﻿using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class UsersDao : IUsersDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;

        public bool AddUser(User user)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "[dbo].[AddUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@login", user.Login);
                command.Parameters.AddWithValue("@passwordHash", user.PasswordHash);
                command.Parameters.AddWithValue("@roleID", (int)user.Role);

                SqlParameter returnValue = new SqlParameter();
                returnValue.Direction = ParameterDirection.ReturnValue;
                command.Parameters.Add(returnValue);

                try
                {
                    connection.Open();

                    command.ExecuteNonQuery();

                    return (int)returnValue.Value != -1 && (int)returnValue.Value != 0;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "AddUser", ex.Message);
                }
                catch (InvalidCastException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "AddUser", ex.Message);
                }
            }

            return false;
        }

        public bool EditUser(User user)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "[dbo].[EditUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@userId", Convert.ToInt32(user.UserId));
                command.Parameters.AddWithValue("@login", user.Login);
                command.Parameters.AddWithValue("@passwordHash", user.PasswordHash);
                command.Parameters.AddWithValue("@roleID", (int)user.Role);

                SqlParameter returnValue = new SqlParameter();
                returnValue.Direction = ParameterDirection.ReturnValue;
                command.Parameters.Add(returnValue);

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();

                    return (int)returnValue.Value != -1 && (int)returnValue.Value != 0;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "EditUser", ex.Message);
                }
                catch (InvalidCastException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "EditUser", ex.Message);
                }
            }

            return false;
        }

        public bool DeleteUserById(int id)
        {
            int numOfRowsAffected = -1;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();

                command.Connection = connection;
                command.CommandText = "[dbo].[DeleteUser]";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@userId", id);

                try
                {
                    connection.Open();
                    numOfRowsAffected = command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "DeleteUser", ex.Message);
                }
            }

            return numOfRowsAffected != -1 && numOfRowsAffected != 0;
        }

        public List<User> GetAllUsers()
        {
            List<User> users = new List<User>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "[dbo].[GetAllUsers]";
                command.CommandType = CommandType.StoredProcedure;

                try
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User
                            {
                                UserId = reader["Id"].ToString(),
                                Login = reader["UserName"].ToString(),
                                PasswordHash = reader["PasswordHash"].ToString(),
                                Role = (UserRoles)Enum.Parse(typeof(UserRoles), reader["Name"].ToString())
                            };

                            users.Add(user);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "GetAllUsers", ex.Message);
                }
            }

            return users;
        }

        public List<User> GetAllUsersFromSqlTable()
        {
            List<User> users = new List<User>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "[dbo].[GetAllUsersFromSqlTable]";
                command.CommandType = CommandType.StoredProcedure;

                try
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User
                            {
                                UserId = reader["UserID"].ToString(),
                                Login = reader["Login"].ToString(),
                                PasswordHash = reader["PasswordHash"].ToString(),
                                Role = (UserRoles)Enum.Parse(typeof(UserRoles), reader["Role"].ToString())
                            };

                            users.Add(user);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "GetAllUsersFromSqlTable", ex.Message);
                }
            }

            return users;
        }

        public User GetUserById(int id)
        {
            User user = null;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "[dbo].[GetUserById]";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new User
                            {
                                UserId = reader["UserID"].ToString(),
                                Login = reader["Login"].ToString(),
                                PasswordHash = reader["PasswordHash"].ToString(),
                                Role = (UserRoles)Enum.Parse(typeof(UserRoles), reader["Role"].ToString())
                            };
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "GetUserById", ex.Message);
                }
            }
            return user;
        }

        public bool SetUserRole(int userId, UserRoles role)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = "[dbo].[SetUserRole]",
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@userID", userId);
                command.Parameters.AddWithValue("@roleID", (int)role);

                try
                {
                    connection.Open();

                    return command.ExecuteNonQuery() != -1;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "UsersDao", "SetUserRole", ex.Message);
                }
            }

            return false;
        }
    }
}
