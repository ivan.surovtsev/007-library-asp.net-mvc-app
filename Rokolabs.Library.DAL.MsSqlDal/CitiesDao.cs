﻿using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Rokolabs.Library.DAL.Interfaces;

namespace Rokolabs.Library.DAL.MsSqlDal
{
    public class CitiesDao : ICitiesDao
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;

        public bool AddCity(City city)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = "[dbo].[AddCity]",
                    CommandType = CommandType.StoredProcedure
                };

                command.Parameters.AddWithValue("@name", city.CityName);

                try
                {
                    connection.Open();

                    return command.ExecuteNonQuery() != -1;
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "CitiesDao", "AddCity", ex.Message);
                }

                return false;
            }
        }

        public HashSet<City> GetAllCities()
        {
            HashSet<City> cities = new HashSet<City>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.StoredProcedure,

                    CommandText = "[dbo].[GetAllCities]"
                };

                try
                {
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            City city = new City
                            {
                                CityId = (int)reader["CityID"],
                                CityName = reader["Name"].ToString()
                            };

                            cities.Add(city);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.AddEvent(LogEventType.Error, "DAL", "CitiesDao", "GetAllCities", ex.Message);
                }
            }

            return cities;
        }
    }
}
