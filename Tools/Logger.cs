﻿using log4net;
using log4net.Config;
using System;

namespace Rokolabs.Library.Tools
{
    public static class Logger
    {
        static Logger()
        {
            XmlConfigurator.Configure();
            Log = LogManager.GetLogger("Logger");
        }

        public static ILog Log { get; private set; }

        public static void Debug(string message)
        {
            Log.Debug(message);
        }

        public static void Info(string message)
        {
            Log.Info(message);
        }

        public static void Warn(string message)
        {
            Log.Warn(message);
        }

        public static void Error(string message)
        {
            Log.Error(message);
        }

        public static void Fatal(string message)
        {
            Log.Fatal(message);
        }

        public static void AddEvent(LogEventType eventType, string layer, string @class, string method, string message)
        {
            // Example: [06:15:15] [User(User123)] [ERROR in Layer(DAL)->Class(BooksDAO)->Method(AddBook)][Message: "Message"]

            var userLogin = System.Web.HttpContext.Current.User.Identity.Name;
            userLogin = string.IsNullOrEmpty(userLogin) ? "Anonim" : userLogin;

            Log.Info($"[{DateTime.Now.ToUniversalTime()}] [User({userLogin})] [{eventType} in Layer({layer})->Class({@class})->Method({method})] [Message: \"{message}\"]");         
        }
    }
}
