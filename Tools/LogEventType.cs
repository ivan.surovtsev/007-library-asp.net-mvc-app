﻿namespace Rokolabs.Library.Tools
{
    public enum LogEventType
    {
        Info = 1,
        Warning = 2,
        Error = 3,
        Fatal = 4
    }
}
