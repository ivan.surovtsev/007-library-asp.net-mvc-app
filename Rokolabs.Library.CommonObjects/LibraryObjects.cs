﻿namespace Rokolabs.Library.CommonObjects
{
    public enum LibraryObjects
    {
        Empty = 0,
        Book = 1,
        Newspaper = 2,
        Patent = 3
    }
}
