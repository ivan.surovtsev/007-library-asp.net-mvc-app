﻿using Rokolabs.Library.BLL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Rokolabs.Library.WebApplication.Models
{
	public class JwtAuthorizeAttribute : AuthorizeAttribute
	{
		private IUsersLogic usersLogic;

		public JwtAuthorizeAttribute()
		{
			usersLogic = (IUsersLogic)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUsersLogic));
		}

		public override void OnAuthorization(HttpActionContext context)
		{
			var request = context.Request;
			var authorization = request.Headers.Authorization;

			if (authorization == null || authorization.Scheme != "Bearer")
			{
				context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
				return;
			}

			if (string.IsNullOrEmpty(authorization.Parameter))
			{
				context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
				return;
			}

			var token = authorization.Parameter;
			var principal = AuthenticateJwtToken(token);

			if (principal == null)
				context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
			else
				context.RequestContext.Principal = principal;
		}

		protected IPrincipal AuthenticateJwtToken(string token)
		{
			if (ValidateToken(token, out var username))
			{
				var claims = new List<Claim>
				{
					new Claim(ClaimTypes.Name, username)
                    // Add more claims if needed: Roles, ...
                };

				var identity = new ClaimsIdentity(claims, "Jwt");
				IPrincipal user = new ClaimsPrincipal(identity);

				return user;
			}

			return null;
		}

		private bool ValidateToken(string token, out string username)
		{
			username = null;

			var simplePrinciple = JwtManager.GetPrincipal(token);
			var identity = simplePrinciple?.Identity as ClaimsIdentity;

			if (identity == null)
				return false;

			if (!identity.IsAuthenticated)
				return false;

			var usernameClaim = identity.FindFirst(ClaimTypes.Name);
			username = usernameClaim?.Value;

			if (string.IsNullOrEmpty(username))
				return false;

			if (!IsUserInRole(username, Roles))
				return false;

			return true;
		}

		private bool IsUserInRole(string username, string roleName)
		{
			var user = usersLogic.GetAllUsersFromSqlTable().FirstOrDefault(u => u.Login == username);

			if (user != null)
			{
				return user.Role.ToString() == roleName;
			}

			return false;
		}
	}
}