﻿using Rokolabs.Library.WebApp.Models.BindingModels;
using System.Collections.Generic;

namespace Rokolabs.Library.WebApplication.Models.BindingModels
{
    public class CommonEntityBM
    {
        public HashSet<BookBM> Books { get; set; }

        public HashSet<NewspaperBM> Newspapers { get; set; }

        public HashSet<PatentBM> Patents { get; set; }
    }
}