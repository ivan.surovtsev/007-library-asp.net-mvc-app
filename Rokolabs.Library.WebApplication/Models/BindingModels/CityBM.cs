﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class CityBM
    {
        [DisplayName("Name")]
        [Required]
        [StringLength(200, ErrorMessage = "Length must be less or equal 200 characters")]
        [RegularExpression(@"^([A-Z][a-z\s]*((-[A-Z])?[a-z](\s[A-Z])?[a-z]*)*(-[a-z]*-[A-Z])?[a-z]*)$|^([А-ЯЁ][а-яё\s]*((-[А-ЯЁ])?[а-яё](\s[А-ЯЁ])?[а-яё]*)*(-[а-яё]*-[А-ЯЁ])?[а-яё]*)$",
            ErrorMessage = "City name isn't correct")]
        public string CityName { get; set; }
    }
}