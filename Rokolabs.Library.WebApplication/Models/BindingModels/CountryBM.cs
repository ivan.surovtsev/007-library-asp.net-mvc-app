﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class CountryBM
    {
        [DisplayName("Name")]
        [Required]
        [StringLength(200, ErrorMessage = "Length must be less or equal 200 characters")]
        [RegularExpression(@"^([A-Z]*)$|^([А-ЯЁ]*)$|^([A-Z][a-z]*)$|^([А-ЯЁ][а-яё]*)$",
         ErrorMessage = "Country name isn't correct")]
        public string CountryName { get; set; }    
    }
}