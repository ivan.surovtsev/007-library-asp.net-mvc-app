﻿namespace Rokolabs.Library.WebApp.Models.ViewModels
{
    public class AuthorVM
    {
        public string FirstName { get; set; }
   
        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}