﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Rokolabs.Library.WebApp.Models.ViewModels
{
    public class BookVM : LibraryEntryVM
    {
        [DisplayName("Publication place")]
        public string PublicationPlace { get; set; }

        public override short? PublicationYear { get; set; }
 
        public string Notes { get; set; }
    
        public string ISBN { get; set; }     

        public HashSet<AuthorVM> Authors { get; set; }       
        
        public string AuthorList
        {
            get
            {
                return string.Join(", ", Authors);
            }
        }

        public override string ToString()
        {
            string output = string.Empty;

            var query = Authors.Where(a => string.IsNullOrEmpty(a.FirstName) && string.IsNullOrEmpty(a.LastName));

            if (!query.Any())
            {
                output = string.Join(", ", Authors) + " - ";
            }            

            StringBuilder builder = new StringBuilder();

            builder.Append($"{Name} ");

            if (PublicationYear.HasValue)
            {
                builder.Append($"({PublicationYear})");
            }

            return output + builder.ToString();
        }
    }
}