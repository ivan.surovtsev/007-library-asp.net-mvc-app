﻿using Rokolabs.Library.Entities;

namespace Rokolabs.Library.WebApp.Models.ViewModels
{
    public class UserVM
    {
        public string UserId { get; set; }

        public string Login { get; set; }
        
        public string PasswordHash { get; set; }

        public UserRoles Role { get; set; }
    }
}