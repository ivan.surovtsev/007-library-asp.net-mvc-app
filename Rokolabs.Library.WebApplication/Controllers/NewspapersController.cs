﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApp.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Rokolabs.Library.WebApplication.Controllers
{
    public class NewspapersController : ApiController
    {
        private INewsapersLogic logic;
        private IMapper mapper;

        public NewspapersController(INewsapersLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        // GET: api/Newspapers
        public IHttpActionResult Get([FromUri] Page<NewspaperVM> pageOption)
        {
            var pageResult = logic.GetNewspapersForPage(mapper.Map<Page<NewspaperVM>, Page<Newspaper>>(pageOption));

            if (pageResult.Objects.Any())
            {
                return Ok<Page<NewspaperVM>>(mapper.Map<Page<Newspaper>, Page<NewspaperVM>>(pageResult));
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Newspapers/5
        public IHttpActionResult Get(int id)
        {
            var newspaper = logic.GetNewspaperById(id);

            if (newspaper != null)
            {
                var newspaperVM = mapper.Map<NewspaperVM>(newspaper);

                return Ok<NewspaperVM>(newspaperVM);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Newspapers
        public IHttpActionResult Post([FromBody] NewspaperBM newspaper)
        {
            if (ModelState.IsValid)
            {
                if (logic.AddNewspaper(mapper.Map<Newspaper>(newspaper), false))
                {
                    return Ok();
                }

                ModelState.AddModelError(key: "Uniqueness", errorMessage: "Newspaper is not unique");
            }

            return BadRequest(ModelState);
        }

        // PUT: api/Newspapers/5
        public IHttpActionResult Put(int id, [FromBody] NewspaperBM newspaperBM)
        {
            if (ModelState.IsValid)
            {
                var newspaper = logic.GetNewspaperById(id);

                if (newspaper == null)
                {
                    return NotFound();
                }

                newspaperBM.EntryID = id;

                if (logic.AddNewspaper(mapper.Map<Newspaper>(newspaperBM), false))
                {
                    return Ok();
                }

                ModelState.AddModelError(key: "Uniqueness", errorMessage: "Newspaper is not unique");
            }

            return BadRequest(ModelState);
        }

        // DELETE: api/Newspapers/5
        public IHttpActionResult Delete(int id)
        {
            if (logic.DeleteNewspaperById(id))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
