﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApp.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Rokolabs.Library.WebApplication.Controllers
{
    public class BooksController : ApiController
    {
        private IBooksLogic logic;
        private IMapper mapper;

        public BooksController(IBooksLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }
                
        public IHttpActionResult Get([FromUri]Page<BookVM> pageOption)
        {
            var pageResult = logic.GetBooksForPage(mapper.Map<Page<BookVM>, Page<Book>>(pageOption));

            if (pageResult.Objects.Any())
            {
                return Ok<Page<BookVM>>(mapper.Map<Page<Book>, Page<BookVM>>(pageResult));
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Book/5
        public IHttpActionResult Get(int id)
        {
            var book = logic.GetBookById(id);

            if (book != null)
            {
                var bookVM = mapper.Map<BookVM>(book);
                return Ok<BookVM>(bookVM);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Book
        public IHttpActionResult Post([FromBody]BookBM bookBM)
        {
            if (ModelState.IsValid)
            {
                if (logic.AddBook(mapper.Map<Book>(bookBM)))
                {
                    return Ok();
                }

                ModelState.AddModelError( key: "Uniqueness", errorMessage: "Book is not unique");
            }

            return BadRequest(ModelState);
        }

        // PUT: api/Book/5
        public IHttpActionResult Put(int id, [FromBody]BookBM bookBM)
        {
            if (ModelState.IsValid)
            {
                var book = logic.GetBookById(id);

                if (book == null)
                {
                    return NotFound();
                }

                bookBM.EntryID = id;                

                if (logic.AddBook(mapper.Map<Book>(bookBM)))
                {
                    return Ok();
                }

                ModelState.AddModelError(key: "Uniqueness", errorMessage: "Book is not unique");
            }

            return BadRequest(ModelState);
        }

        // DELETE: api/Book/5
        public IHttpActionResult Delete(int id)
        {
            if (logic.DeleteBookById(id))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }            
        }

        [Route("api/Books/groupingByPublishingHouse/{publishingHouse=}")]
        [HttpGet]
        public IHttpActionResult GetBooksGroupedByPublishingHouse(string publishingHouse)
        {
            if (publishingHouse == null)
            {
                publishingHouse = "";
            }

            var books = logic.GetBooksGroupedByPublishingHouse(publishingHouse);

            if (books.Any())
            {
                return Ok<HashSet<IGrouping<string, Book>>>(books);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
