﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Utils;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApplication.Models;
using System.Linq;
using System.Web.Http;

namespace Rokolabs.Library.WebApplication.Controllers
{
    public class AccountController : ApiController
    {
        private IUsersLogic usersLogic;
        private IMapper mapper;

        public AccountController(IUsersLogic usersLogic, IMapper mapper)
        {
            this.usersLogic = usersLogic;
            this.mapper = mapper;
        }

        [Route("login")]
        [HttpPost]
        public IHttpActionResult Login(UserLoginModel user)
        {
            if (ModelState.IsValid)
            {
                var findedUser = usersLogic.GetAllUsersFromSqlTable().FirstOrDefault(u =>
                    u.Login == user.Login &&
                    u.PasswordHash == HashUtils.GetHash(user.Password)
                    );

                if (findedUser == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(JwtManager.GenerateToken(user.Login));
                }
            }

            return BadRequest(ModelState);
        }

        [Route("register")]
        [HttpPost]
        public IHttpActionResult Register(UserBM userBM)
        {
            if (ModelState.IsValid)
            {
                if (userBM.IsValid)
                {
                    if (usersLogic.AddUser(mapper.Map<User>(userBM)))
                    {
                        return Ok(JwtManager.GenerateToken(userBM.Login));
                    }
                    else
                    {
                        ModelState.AddModelError("", "User with that login already exists");
                    }
                }
                else
                {
                    ModelState.AddModelError("Password", "Invalid password");
                }
            }

            return BadRequest(ModelState);
        }
    }
}
