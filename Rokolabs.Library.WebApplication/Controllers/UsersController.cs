﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApp.Models.ViewModels;
using System.Collections.Generic;
using System.Web.Http;

namespace Rokolabs.Library.WebApplication.Controllers
{
    public class UsersController : ApiController
    {
        private IUsersLogic usersLogic;
        private IMapper mapper;

        public UsersController(IUsersLogic usersLogic, IMapper mapper)
        {
            this.usersLogic = usersLogic;
            this.mapper = mapper;
        }

        // GET: api/Users
        public IHttpActionResult Get()
        {
            return Ok<HashSet<UserVM>>(mapper.Map<HashSet<UserVM>>(usersLogic.GetAllUsersFromSqlTable()));
        }

        // GET: api/Users/5
        public IHttpActionResult Get(int id)
        {
            var user = usersLogic.GetUserById(id);

            if (user != null)
            {
                return Ok<UserVM>(mapper.Map<UserVM>(user));
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Users
        public IHttpActionResult Post([FromBody] UserBM userBM)
        {
            if (ModelState.IsValid)
            {
                if (userBM.IsValid)
                {
                    if (usersLogic.AddUser(mapper.Map<User>(userBM)))
                    {
                        return Ok();
                    }
                    else
                    {
                        ModelState.AddModelError("", "User with that login already exists");
                    }
                }
                else
                {
                    ModelState.AddModelError("Password", "Invalid password");
                }
            }

            return BadRequest(ModelState);
        }

        // PUT: api/Users/5
        public IHttpActionResult Put(int id, [FromBody] UserBM userBM)
        {
            if (ModelState.IsValid)
            {
                if (userBM.IsValid)
                {
                    var user = usersLogic.GetUserById(id);

                    if (user == null)
                    {
                        return NotFound();
                    }

                    userBM.UserId = id;

                    if (usersLogic.EditUser(mapper.Map<User>(userBM)))
                    {
                        return Ok();
                    }
                    else
                    {
                        ModelState.AddModelError("", "User with that login already exists");
                    }
                }
                else
                {
                    ModelState.AddModelError("Password", "Invalid password");
                }
            }

            return BadRequest(ModelState);
        }

        // DELETE: api/Users/5
        public IHttpActionResult Delete(int id)
        {
            if (usersLogic.DeleteUserById(id))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
