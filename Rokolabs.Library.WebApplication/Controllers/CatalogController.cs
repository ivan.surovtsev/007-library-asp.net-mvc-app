﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.WebApp.Models.ViewModels;
using Rokolabs.Library.WebApplication.Models;
using Rokolabs.Library.WebApplication.Models.BindingModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace Rokolabs.Library.WebApplication.Controllers
{
    [RoutePrefix("catalog")]
    public class CatalogController : ApiController
    {
        private ILibraryLogic logic;
        private IMapper mapper;

        public CatalogController(ILibraryLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get([FromUri] Page<LibraryEntryVM> pageOption)
        {
            if (pageOption == null)
            {
                pageOption = new Page<LibraryEntryVM>();
            }
            var pageResult = logic.GetCatalogForPage(mapper.Map<Page<LibraryEntryVM>, Page<LibraryEntry>>(pageOption));

            if (pageResult.Objects.Any())
            {
                return Ok<Page<LibraryEntryVM>>(mapper.Map<Page<LibraryEntry>, Page<LibraryEntryVM>>(pageResult));
            }
            else
            {
                return NotFound();
            }
        }

        [Route("")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody] CommonEntityBM entitiesBM)
        {
            if (ModelState.IsValid)
            {
                var entities = mapper.Map<CommonEntity>(entitiesBM);

                if (logic.TryAddEntities(entities, out CommonEntity addedEntities))
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, mapper.Map<CommonEntityBM>(addedEntities));
                }
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [Route("groupingByYear")]
        [JwtAuthorize(Roles = "Admin")]
        [HttpGet]
        public IHttpActionResult GetCatalogGroupedByYear()
        {
            var catalog = logic.GetCatalogForGroupingByYear();

            if (catalog.Any())
            {
                return Ok<HashSet<IGrouping<short?, LibraryEntry>>>(catalog);
            }
            else
            {
                return NotFound();
            }
        }

        [Route("statistics")]
        [HttpGet]
        public IHttpActionResult GetCountOfActiveAndDeletedEntities()
        {
            return Ok<Statistics>(logic.GetCountOfActiveAndDeletedEntities());
        }

        [Route("statistics/toCSV")]
        [HttpGet]
        public HttpResponseMessage ConvertStatisticsToCsv()
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(Encoding.Unicode.GetBytes(logic.GetCountOfActiveAndDeletedEntitiesForCsv()));
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = "Statistics.csv";
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");

            return response;
        }
    }
}
