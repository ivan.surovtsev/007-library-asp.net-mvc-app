﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApp.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Rokolabs.Library.WebApplication.Controllers
{
    public class PatentsController : ApiController
    {
        private IPatentsLogic logic;
        private IMapper mapper;

        public PatentsController(IPatentsLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        // GET: api/Patent
        public IHttpActionResult Get([FromUri] Page<PatentVM> pageOption)
        {
            var pageResult = logic.GetPatentsForPage(mapper.Map<Page<PatentVM>, Page<Patent>>(pageOption));

            if (pageResult.Objects.Any())
            {
                return Ok<Page<PatentVM>>(mapper.Map<Page<Patent>, Page<PatentVM>>(pageResult));
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Patent/5
        public IHttpActionResult Get(int id)
        {
            var patent = logic.GetPatentById(id);

            if (patent != null)
            {
                var patentVM = mapper.Map<PatentVM>(patent);
                return Ok<PatentVM>(patentVM);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Patent
        public IHttpActionResult Post([FromBody]PatentBM patent)
        {
            if (ModelState.IsValid)
            {
                if (logic.AddPatent(mapper.Map<Patent>(patent)))
                {
                    return Ok();
                }

                ModelState.AddModelError(key: "Uniqueness", errorMessage: "Patent is not unique");
            }

            return BadRequest(ModelState);
        }

        // PUT: api/Patent/5
        public IHttpActionResult Put(int id, [FromBody]PatentBM patentBM)
        {
            if (ModelState.IsValid)
            {
                var patent = logic.GetPatentById(id);

                if (patent == null)
                {
                    return NotFound();
                }

                patentBM.EntryID = id;

                if (logic.AddPatent(mapper.Map<Patent>(patentBM)))
                {
                    return Ok();
                }

                ModelState.AddModelError(key: "Uniqueness", errorMessage: "Patent is not unique");
            }

            return BadRequest(ModelState);
        }

        // DELETE: api/Patent/5
        public IHttpActionResult Delete(int id)
        {
            if (logic.DeletePatentById(id))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
