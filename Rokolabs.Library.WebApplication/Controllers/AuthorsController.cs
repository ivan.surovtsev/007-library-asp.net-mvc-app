﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Rokolabs.Library.WebApplication.Controllers
{
    public class AuthorsController : ApiController
    {
        private IAuthorsLogic logic;
        private IMapper mapper;

        public AuthorsController(IAuthorsLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        // GET: api/Author
        public IHttpActionResult Get()
        {
            var authors = logic.GetAllAuthors();

            if (authors.Any())
            {
                return Ok<HashSet<Author>>(authors);
            }
            else
            {
                return NotFound();
            }            
        }
    }
}
