﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.DAL.Interfaces
{
    public interface IBooksDao
    {
        Book AddBook(Book book);

        bool DeleteBook(Book book);

        bool DeleteBookById(int id);

        Book GetBookById(int id);

        HashSet<Book> GetAllBooks();

        Page<Book> GetBooksForPage(Page<Book> page);

        HashSet<Book> GetBooksByAuthor(string firstName, string lastName);

        HashSet<Book> GetBooksByPublishingHouse(string publishingHouseName);

        HashSet<Book> GetBooksByName(string name);
    }
}
