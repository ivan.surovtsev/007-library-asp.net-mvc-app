﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.DAL.Interfaces
{
    public interface IAuthorsDao
    {
        bool AddAuthor(Author author);

        HashSet<Author> GetAllAuthors();

        Author GetAuthorById(int id);
    }
}
