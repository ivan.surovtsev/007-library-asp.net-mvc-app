﻿using Rokolabs.Library.Entities;

namespace Rokolabs.Library.DAL.Interfaces
{
    public interface ILibraryDao
    {
        Page<LibraryEntry> GetCatalogForPage(Page<LibraryEntry> pageOption);

        Statistics GetCountOfActiveAndDeletedEntities();
    }
}
