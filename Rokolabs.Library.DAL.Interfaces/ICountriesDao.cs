﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.DAL.Interfaces
{
    public interface ICountriesDao
    {
        bool AddCountry(Country country);

        HashSet<Country> GetAllCountries();
    }
}
