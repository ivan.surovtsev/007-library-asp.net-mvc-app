﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.DAL.Interfaces
{
    public interface IPatentsDao
    {
        Patent AddPatent(Patent patent);

        bool DeletePatent(Patent patent);

        bool DeletePatentById(int id);

        Patent GetPatentById(int id);

        HashSet<Patent> GetAllPatents();

        Page<Patent> GetPatentsForPage(Page<Patent> page);

        HashSet<Patent> GetPatentByName(string name);

        HashSet<Patent> GetPatentsByInventor(string firstName, string lastName);
    }
}
