﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.DAL.Interfaces
{
    public interface ICitiesDao
    {
        bool AddCity(City city);

        HashSet<City> GetAllCities();
    }
}
