﻿namespace Rokolabs.Library.Entities
{
    public class Statistics
    {
        public int CountOfActiveBooks { get; set; }

        public int CountOfActiveNewspapers { get; set; }

        public int CountOfActivePatents { get; set; }

        public int CountOfDeletedBooks { get; set; }

        public int CountOfDeletedNewspapers { get; set; }

        public int CountOfDeletedPatents { get; set; }

        public int CountOfActiveEntities { get; set; }

        public int CountOfDeletedEntities { get; set; }
    }
}
