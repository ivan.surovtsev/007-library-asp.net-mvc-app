﻿using System.Collections.Generic;

namespace Rokolabs.Library.Entities
{
    public class Book : LibraryEntry
    {        
        public string PublicationPlace { get; set; }
       
        public override short? PublicationYear { get; set; }       
        
        public string Notes { get; set; }
       
        public string ISBN { get; set; }        

        public HashSet<Author> Authors { get; set; }  

        public int Hash { get; set; }      

        public override bool Equals(object obj)
        {
            if (!(obj is Book))
            {
                return false;
            }

            Book otherEntry = obj as Book;

            return GetHashCode() == otherEntry.GetHashCode();
        }

        public override int GetHashCode()
        {    
            if (EntryId != 0)
            {
                return EntryId;
            }      

            return Hash;
        }        
    }
}
