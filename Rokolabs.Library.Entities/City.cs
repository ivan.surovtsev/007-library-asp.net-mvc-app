﻿namespace Rokolabs.Library.Entities
{
    public class City
    {
        public int CityId { get; set; }

        public Country Country { get; set; }
    
        public string CityName { get; set; }
    }
}
