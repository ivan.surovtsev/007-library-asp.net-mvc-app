﻿using System.Collections.Generic;

namespace Rokolabs.Library.Entities
{
    public class Page<T>
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; }
        public string NameOfObject { get; set; } = "";
        public string Sort { get; set; } = "DESC";
        public string Author { get; set; } = "";
        public int TotalCount { get; set; }
        public HashSet<T> Objects { get; set; }
    }
}
