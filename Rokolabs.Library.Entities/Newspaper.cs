﻿using System;

namespace Rokolabs.Library.Entities
{
    public class Newspaper : LibraryEntry, IComparable
    {      
        public string PublicationPlace { get; set; }      

        public override short? PublicationYear 
        { 
            get
            {
                return IssueDate.HasValue ? (short?)IssueDate.Value.Year : null;
            }
            set
            {
            }
        }
        
        public string Notes { get; set; }
    
        public string ISSN { get; set; }        
           
        public string IssueNumber { get; set; }    
        
        public DateTime? IssueDate { get; set; }      

        public int Hash { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is Newspaper))
            {
                return false;
            }

            Newspaper otherEntry = obj as Newspaper;

            return GetHashCode() == otherEntry.GetHashCode();
        }

        public override int GetHashCode()
        {
            if (EntryId != 0)
            {
                return EntryId;
            }

            return Hash;
        }        
    }
}
