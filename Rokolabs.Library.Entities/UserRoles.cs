﻿namespace Rokolabs.Library.Entities
{
    public enum UserRoles
    {
        Guest = 0,
        User = 1,
        Admin = 2
    }
}
