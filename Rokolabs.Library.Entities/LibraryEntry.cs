﻿using System;

namespace Rokolabs.Library.Entities
{
    public abstract class LibraryEntry : IComparable
    {
        public int EntryId { get; set; }

        public string Name { get; set; }
       
        public abstract short? PublicationYear { get; set; }

        public string PublishingHouse { get; set; }
    
        public int? NumberOfPages { get; set; }

        public int CompareTo(object obj)
        {
            if (!(obj is LibraryEntry))
            {
                throw new ArgumentException("\'obj\' must be type of LibraryEntry type");
            }

            LibraryEntry entry = obj as LibraryEntry;

            int valForCompare1 = 0, valForCompare2 = 0;

            // First value
            if (PublicationYear != null)
            {
                valForCompare1 = PublicationYear.Value;
            }

            // Second value
            if (entry.PublicationYear != null)
            {
                valForCompare2 = entry.PublicationYear.Value;
            }

            return valForCompare1 - valForCompare2;
        }
    }
}
