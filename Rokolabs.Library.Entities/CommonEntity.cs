﻿using System.Collections.Generic;

namespace Rokolabs.Library.Entities
{
    public class CommonEntity
    {
        public HashSet<Book> Books { get; set; } = new HashSet<Book>();

        public HashSet<Newspaper> Newspapers { get; set; } = new HashSet<Newspaper>();

        public HashSet<Patent> Patents { get; set; } = new HashSet<Patent>();
    }
}
