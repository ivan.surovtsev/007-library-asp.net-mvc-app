﻿using System;
using System.Collections.Generic;

namespace Rokolabs.Library.Entities
{
    public class Patent : LibraryEntry, IComparable
    {
        public override short? PublicationYear
        {
            get
            {
                return PublishingDate.HasValue ? (short?)PublishingDate.Value.Year : null;
            }
            set
            {
            }
        }

        public string Notes { get; set; }

        public string Country { get; set; }

        public int? RegistrationNumber { get; set; }

        public DateTime? DateOfApplication { get; set; }
   
        public DateTime? PublishingDate { get; set; }

        public HashSet<Author> Authors { get; set; }

        public int Hash { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is Patent))
            {
                return false;
            }

            Patent otherEntry = obj as Patent;

            return GetHashCode() == otherEntry.GetHashCode();
        }

        public override int GetHashCode()
        {
            if (EntryId != 0)
            {
                return EntryId;
            }

            return Hash;
        }
    }
}
