﻿namespace Rokolabs.Library.Entities
{
    public class Author
    {    
        public int AuthorID { get; set; } 
       
        public string FirstName { get; set; }        
  
        public string LastName { get; set; }     

        public override bool Equals(object obj)
        {
            if (!(obj is Author))
            {
                return false;
            }

            Author otherOriginator = obj as Author;

            return GetHashCode() == otherOriginator.GetHashCode();
        }

        public override int GetHashCode()
        {
            int hash = 0;

            for (int i = 0; i < FirstName.Length; i++)
            {
                hash += FirstName[i];
            }

            for (int i = 0; i < LastName.Length; i++)
            {
                hash += LastName[i];
            }

            return hash;
        }       
    }
}
