﻿namespace Rokolabs.Library.Entities
{
    public class User
    {
        public string UserId { get; set; }

        public string Login {  get; set; }
        
        public string PasswordHash { get; set; }

        public UserRoles Role { get; set; }
    }
}