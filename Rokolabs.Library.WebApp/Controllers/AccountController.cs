﻿using Rokolabs.Library.WebApp.Models.BindingModels;
using System.Web.Mvc;
using System.Linq;
using Rokolabs.Library.Tools;
using Rokolabs.Library.BLL.Interfaces;
using AutoMapper;
using Rokolabs.Library.WebApp.Security;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Rokolabs.Library.WebApp.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace Rokolabs.Library.WebApp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private IUsersLogic usersLogic;
        private IMapper mapper;

        public AccountController(IUsersLogic usersLogic, IMapper mapper)
        {
            this.usersLogic = usersLogic;
            this.mapper = mapper;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }           
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }           
        }

        // GET: Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: Account/Login
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLoginModel user, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                Logger.AddEvent(LogEventType.Info, "MVC", "AccountController", "Login", $"Attempting authentication with login \"{user.Login}\"");  

                var result = SignInManager.PasswordSignIn(user.Login, user.Password, isPersistent: false, shouldLockout: false);

                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
             
                    case SignInStatus.Failure:
                    default:

                        ModelState.AddModelError("", "Invalid login attempt");
                        return View(user);
                }

                //var findedUser = commLogic.GetAllUsers().FirstOrDefault(u =>
                //    u.Login == user.Login.ToLower() &&
                //    u.PasswordHash == HashUtils.GetHash(user.Password)
                //    );

                //if (findedUser == null)
                //{
                //    ModelState.AddModelError("", "Invalid user name or password!");
                //}
                //else
                //{
                //    FormsAuthentication.SetAuthCookie(user.Login, false);

                //    string controllerName = string.Empty;
                //    string actionName = string.Empty;
                //    string id = string.Empty;

                //    if (!string.IsNullOrEmpty(returnUrl))
                //    {
                //        var route = returnUrl.Split('/');

                //        if (route.Length > 1)
                //        {
                //            controllerName = route[1];

                //            if (route.Length > 2)
                //            {
                //                actionName = route[2];

                //                if (route.Length > 3)
                //                {
                //                    id = route[3];
                //                }
                //            }
                //            else
                //            {
                //                actionName = "Index";
                //            }
                //        }
                //    }

                //    if (!string.IsNullOrEmpty(controllerName))
                //    {
                //        return RedirectToAction(actionName, controllerName, new { id });
                //    }

                //    return RedirectToAction("Index", "Catalog");
                //}
            }

            return View(user);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            var result = new ChallengeResult
            {
                LoginProvider = provider,
                RedirectUri = Url.Action("ExternalLoginCallback", "Account", new { returnUrl })
            };

            return result;
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();

            if (loginInfo == null)
            {
                ModelState.AddModelError("", "Can't authenticate with external login provider");
                return RedirectToAction("Login");
            }

            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);

                case SignInStatus.Failure:
                default:

                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;

                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationVM { Login = loginInfo.DefaultUserName });                   
            }            
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationVM model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(returnUrl);              
            }

            if (ModelState.IsValid)
            {               
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();

                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }

                var user = new UserExternalLoginModel { UserName = model.Login };

                var result = await UserManager.CreateAsync(user);                

                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Admin");

                    result = await UserManager.AddLoginAsync(user.Id, info.Login);

                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }

                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        // GET: Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        // GET: Account/Register
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserBM userBM)
        {
            if (ModelState.IsValid)
            {
                Logger.AddEvent(LogEventType.Info, "MVC", "AccountController", "Register", $"Registering user with login \"{userBM.Login}\"");
              
                if (!userBM.IsValid)
                {
                    ModelState.AddModelError("Password", "Invalid password");
                    return View(userBM);
                }                

                //if (commLogic.AddUser(mapper.Map<User>(userBM)))
                //{
                //    FormsAuthentication.SetAuthCookie(userBM.Login, false);

                //    return RedirectToAction("Index", "Catalog");
                //}
                //else
                //{
                //    ModelState.AddModelError("", "User with that login already exists");
                //}                

                var user = new UserExternalLoginModel { UserName = userBM.Login };               

                var result = UserManager.Create(user, userBM.Password);

                UserManager.AddToRole(user.Id, "User");

                if (result.Succeeded)
                {
                    SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    return RedirectToAction("Index", "Catalog");
                }

                AddErrors(result);
            }

            return View(userBM);
        }

        // GET: Account/Logout       
        public ActionResult Logout()
        {
            Logger.AddEvent(LogEventType.Info, "MVC", "AccountController", "Logout", $"Logout user with login \"{User.Identity.Name}\"");

            //FormsAuthentication.SignOut();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Login");            
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Catalog");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}