﻿using Rokolabs.Library.BLL.Interfaces;
using System.Web.Mvc;
using Rokolabs.Library.WebApp.Models.ViewModels;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.Tools;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;
using Rokolabs.Library.Entities;

namespace Rokolabs.Library.WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NewspapersController : Controller
    {
        private INewsapersLogic logic;
        private IMapper mapper;

        public NewspapersController(INewsapersLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        // GET: Newspapers/Details/id
        public ActionResult Details(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "NewspapersController", "Details", $"Requesting for details newspaper with id={strId}");

            if (id.HasValue)
            {
                var newspaper = logic.GetNewspaperById(id.Value);

                if (newspaper != null)
                {                   
                    var newspaperWithSameName = from n in logic.GetAllNewspapers()
                                                where n.Name == newspaper.Name
                                                select n;

                    ViewBag.Issues = newspaperWithSameName;
                    var newspaperVM = mapper.Map<NewspaperVM>(newspaper);

                    return View(newspaperVM);
                }
            }

            return RedirectToAction("Index", "Catalog");            
        }

        // GET: Newspapers/Delete/id
        public ActionResult Delete(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "NewspapersController", "Details", $"Requesting for deleting newspaper with id={strId}");

            if (id.HasValue)
            {
                var newspaper = logic.GetNewspaperById(id.Value);

                if (newspaper != null)
                {
                    var newspaperVM = mapper.Map<NewspaperVM>(newspaper);

                    return View(newspaperVM);
                }
            }

            return RedirectToAction("Index", "Catalog");            
        }

        // POST: Newspapers/Delete/id
        [HttpPost]
        public ActionResult Delete(int? id, FormCollection collection)
        {
            if (id.HasValue)
            {
                logic.DeleteNewspaperById(id.Value);
            }

            return RedirectToAction("Index", "Catalog");
        }

        // GET: Newspapers/Create
        public ActionResult Create()
        {
            Logger.AddEvent(LogEventType.Info, "MVC", "NewspapersController", "Details", $"Requesting for creating a newspaper");

            return View();
        }

        // POST: Newspapers/Create
        [HttpPost]
        public ActionResult Create(NewspaperBM newspaper)
        {
            if (ModelState.IsValid)
            {
                var newspaperWithSameISSN = logic.GetAllNewspapers().FirstOrDefault(n => n.ISSN == newspaper.ISSN);

                if (newspaperWithSameISSN == null)
                {
                    Logger.AddEvent(LogEventType.Info, "MVC", "NewspapersController", "Create", $"Newspaper with name \"{newspaper.Name}\" was created");

                    logic.AddNewspaper(mapper.Map<Newspaper>(newspaper), false);          

                    return RedirectToAction("Index", "Catalog");
                }
                else
                {
                    ModelState.AddModelError("", "Newspaper with that ISSN already exists!");
                }
            }

            return View();
        }

        // GET: Newspapers/AddIssue
        public ActionResult AddIssue()
        {
            return View();
        }

        // POST: Newspapers/AddIssue
        [HttpPost]
        public ActionResult AddIssue(NewspaperIssueBM issue)
        {
            if (ModelState.IsValid)
            {
                int entryID;

                if (int.TryParse(issue.ForNewspaper, out entryID))
                {           
                    var existedNewspaper = logic.GetNewspaperById(entryID);

                    if (existedNewspaper == null)
                    {
                        return RedirectToAction("Index", "Catalog");
                    }

                    Logger.AddEvent(LogEventType.Info, "MVC", "NewspapersController", "AddIssue", $"Newspaper issue for newspaper \"{existedNewspaper.Name}\" was added");

                    Newspaper newspaper = new Newspaper
                    {
                        EntryId = existedNewspaper.EntryId,
                        ISSN = existedNewspaper.ISSN,
                        IssueDate = issue.IssueDate.HasValue ? issue.IssueDate : existedNewspaper.IssueDate,
                        IssueNumber = !string.IsNullOrEmpty(issue.IssueNumber) ? issue.IssueNumber : existedNewspaper.IssueNumber,
                        Name = existedNewspaper.Name,
                        Notes = existedNewspaper.Notes,
                        NumberOfPages = issue.NumberOfPages.HasValue ? issue.NumberOfPages : existedNewspaper.NumberOfPages,
                        PublicationPlace = existedNewspaper.PublicationPlace,
                        PublicationYear = issue.PublicationYear.HasValue ? issue.PublicationYear : existedNewspaper.PublicationYear,
                        PublishingHouse = existedNewspaper.PublishingHouse
                    };

                    logic.AddNewspaper(newspaper, true);

                    return RedirectToAction("Index", "Catalog");
                }
            }

            return View();
        }

        // GET: Newspapers/Edit/id
        public ActionResult Edit(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "NewspapersController", "Edit", $"Requesting for editing newspaper with id={strId}");

            if (id.HasValue)
            {
                var newspaper = logic.GetNewspaperById(id.Value);

                if (newspaper != null)
                {
                    var newspaperBM = mapper.Map<NewspaperBM>(newspaper);

                    TempData["city"] = newspaperBM.PublicationPlace;

                    return View(newspaperBM);
                }
            }

            return RedirectToAction("Index", "Catalog");
        }

        // POST: Books/Edit/id
        [HttpPost]
        public ActionResult Edit(NewspaperBM newspaperBM)
        {
            if (ModelState.IsValid)
            {
                logic.AddNewspaper(mapper.Map<Newspaper>(newspaperBM), false);

                return RedirectToAction("Index", "Catalog");
            }

            return View(newspaperBM);
        }
    }
}