﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using System.Web.Mvc;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using Rokolabs.Library.WebApp.Models.BindingModels;
using AutoMapper;
using Rokolabs.Library.WebApp.Models.ViewModels;

namespace Rokolabs.Library.WebApp.Controllers
{
    [RoutePrefix("spec")]
    public class ServiceController : Controller
    {
        private INewsapersLogic newspapersLogic;
        private IAuthorsLogic authorsLogic;
        private ICitiesLogic citiesLogic;
        private ICountriesLogic countriesLogic;
        private IMapper mapper;

        public ServiceController(IAuthorsLogic authorsLogic, ICitiesLogic citiesLogic, ICountriesLogic countriesLogic, INewsapersLogic newspapersLogic, IMapper mapper)
        {
            this.authorsLogic = authorsLogic;
            this.citiesLogic = citiesLogic;
            this.countriesLogic = countriesLogic;
            this.newspapersLogic = newspapersLogic;           
            this.mapper = mapper;
        }

        // ***************** CITIES
        [Route("search/city")]
        public ActionResult SearchCities(string name)
        {
            var cities = citiesLogic.GetAllCities();                     

            var query = from city in cities
                        where city.CityName.ToLower().Contains(name.ToLower())
                        select city;

            var json = JsonConvert.SerializeObject(query.ToList().Take(5));

            return Json(json, JsonRequestBehavior.AllowGet); 
        }

        // POST: Spec/AddCity
        [HttpPost]
        [Route("add/city")]
        public ActionResult AddCity(CityBM city)
        {
            if (ModelState.IsValid)
            {
                citiesLogic.AddCity(mapper.Map<City>(city));               
            }

            return new EmptyResult();
        }
        
        [Route("get/city")]
        public ActionResult GetPublicationPlaceForEditedItem()
        {
            var city = TempData["city"] as string;

            if (city != null)
            {
                return Json(city, JsonRequestBehavior.AllowGet);
            }

            return new EmptyResult();
        }

        [ChildActionOnly]
        public ActionResult InsertCityModalDiv()
        {
            return PartialView("_CityModalDiv");
        }

        // ***************** AUTHORS
        [Route("search/authors")]
        public ActionResult SearchAuthors(string name)
        {
            var authors = authorsLogic.GetAllAuthors();
            var authorsVM = mapper.Map<HashSet<AuthorVM>>(authors);

            var query = from author in authorsVM
                        where author.FullName.ToLower().Contains(name.ToLower())
                        select author;

            var json = JsonConvert.SerializeObject(query.ToList().Take(5));

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        // POST: Spec/AddAuthor
        [HttpPost]
        [Route("add/authors")]
        public ActionResult AddAuthor(AuthorBM authorModel)
        {
            if (ModelState.IsValid)
            {
                var author = mapper.Map<Author>(authorModel);
                authorsLogic.AddAuthor(author);
            }

            return new EmptyResult();
        }     

        [Route("get/authors")]
        public ActionResult GetAuthorsForEditedItem()
        {
            HashSet<Author> authors = TempData["authors"] as HashSet<Author>;
            var authorsVM = mapper.Map<HashSet<AuthorVM>>(authors);

            if (authors != null)
            {
                return Json(authorsVM, JsonRequestBehavior.AllowGet);
            }

            return new EmptyResult();
        }

        [ChildActionOnly]
        public ActionResult InsertAuthorModalDiv()
        {
            return PartialView("_AuthorModalDiv");
        }

        // ***************** COUNTRIES
        [Route("search/country")]
        public ActionResult SearchCountries(string name)
        {
            var countries = countriesLogic.GetAllCountries();

            var query = from country in countries
                        where country.CountryName.ToLower().Contains(name.ToLower())
                        select country;

            var json = JsonConvert.SerializeObject(query.Take(5));

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        // POST: Spec/AddCountry
        [HttpPost]
        [Route("add/country")]
        public ActionResult AddCountry(CountryBM country)
        {
            if (ModelState.IsValid)
            {
                countriesLogic.AddCountry(mapper.Map<Country>(country));                        
            }

            return new EmptyResult();
        }

        [Route("get/country")]
        public ActionResult GetCountryForEditedItem()
        {
            var country = TempData["country"] as string;

            if (country != null)
            {
                return Json(country, JsonRequestBehavior.AllowGet);
            }

            return new EmptyResult();
        }

        [ChildActionOnly]
        public ActionResult InsertCountryModalDiv()
        {
            return PartialView("_CountryModalDiv");
        }

        // ***************** NEWSPAPERS
        [Route("search/newspaper")]
        public ActionResult SearchNewspapers(string name)
        {
            var newspapers = newspapersLogic.GetAllNewspapers();

            var query = from newspaper in newspapers
                        where newspaper.Name.ToLower().Contains(name.ToLower())
                        select newspaper;

            var json = JsonConvert.SerializeObject(query.Take(5));

            return Json(json, JsonRequestBehavior.AllowGet);
        }
    }
}