﻿using Rokolabs.Library.BLL.Interfaces;
using System.Web.Mvc;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApp.Models.ViewModels;
using Rokolabs.Library.Tools;
using System.Linq;
using AutoMapper;
using Rokolabs.Library.Entities;

namespace Rokolabs.Library.WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BooksController : Controller
    {        
        private IBooksLogic logic;
        private IMapper mapper;

        public BooksController(IBooksLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        // GET: Books/Details/id
        public ActionResult Details(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "BooksController", "Details", $"Requesting for details for book with id={strId}");

            if (id.HasValue)
            {
                var book = logic.GetBookById(id.Value);

                if (book != null)
                {
                    var bookVM = mapper.Map<BookVM>(book);
                    return View(bookVM);
                }              
            }    

            return RedirectToAction("Index", "Catalog");
        }

        // GET: Books/Create        
        public ActionResult Create()
        {
            Logger.AddEvent(LogEventType.Info, "MVC", "BooksController", "Create", $"Requesting for creating a book");

            return View();
        }

        // POST: Books/Create
        [HttpPost]
        public ActionResult Create(BookBM bookBM)
        {
            if (ModelState.IsValid)
            {
                var bookWithSameISBN = logic.GetAllBooks().FirstOrDefault(b => b.ISBN == bookBM.ISBN);

                if (bookWithSameISBN == null)
                {
                    Book book = mapper.Map<Book>(bookBM);

                    if (logic.AddBook(book))
                    {
                        Logger.AddEvent(LogEventType.Info, "MVC", "BooksController", "Create", $"Book with name \"{book.Name}\" was created");
                    }

                    return RedirectToAction("Index", "Catalog");
                }
                else
                {
                    ModelState.AddModelError("", "Book with that ISBN already exists!");
                } 
            }

            return View();
        }

        // GET: Books/Delete/id
        public ActionResult Delete(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "BooksController", "Delete", $"Requesting for deleting book with id={strId}");

            if (id.HasValue)
            {
                var book = logic.GetBookById(id.Value);

                if (book != null)
                {
                    var bookVM = mapper.Map<BookVM>(book);
                    return View(bookVM);
                }
            }   

            return RedirectToAction("Index", "Catalog");            
        }

        // POST: Books/Delete/id
        [HttpPost]
        public ActionResult Delete(int? id, FormCollection collection)
        {
            if (id.HasValue)
            {
                logic.DeleteBookById(id.Value);
            }                  

            return RedirectToAction("Index", "Catalog");
        }

        // GET: Books/Edit/id
        public ActionResult Edit(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "BooksController", "Edit", $"Requesting for editing book with id={strId}");

            if (id.HasValue)
            {
                var book = logic.GetBookById(id.Value);

                if (book != null)
                {
                    var bookBM = mapper.Map<BookBM>(book);

                    TempData["authors"] = book.Authors;
                    TempData["city"] = book.PublicationPlace;

                    return View(bookBM);
                }
            }       

            return RedirectToAction("Index", "Catalog");
        }                

        // POST: Books/Edit/id
        [HttpPost]
        public ActionResult Edit(BookBM bookBM)
        {
            if (ModelState.IsValid)
            {
                var book = mapper.Map<Book>(bookBM);
                logic.AddBook(book);

                return RedirectToAction("Index", "Catalog");
            }

            return View(bookBM);
        }
    }
}