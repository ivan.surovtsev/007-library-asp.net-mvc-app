﻿using System.Web.Mvc;
using Rokolabs.Library.WebApp.Models.ViewModels;
using Rokolabs.Library.WebApp.Models.BindingModels;
using PagedList;
using Rokolabs.Library.Tools;
using Rokolabs.Library.BLL.Interfaces;
using AutoMapper;
using System.Collections.Generic;
using Rokolabs.Library.Entities;
using System.Linq;

namespace Rokolabs.Library.WebApp.Controllers
{
    public class CatalogController : Controller
    {
        private const int quantityRowsOnPage = 20;
        private ILibraryLogic logic;
        private IAuthorsLogic authorsLogic;
        private IMapper mapper;

        public CatalogController(ILibraryLogic logic, IAuthorsLogic authorsLogic, IMapper mapper)
        {
            this.logic = logic;
            this.authorsLogic = authorsLogic;
            this.mapper = mapper;
        }

        // GET: Catalog
        public ActionResult Index(Page<LibraryEntryVM> page)
        {
            SetViewBagSortBy(page.Sort);
            var pageBM = mapper.Map<Page<LibraryEntryVM>, Page<LibraryEntry>>(page);
            return View(mapper.Map<Page<LibraryEntry>, Page<LibraryEntryVM>>(logic.GetCatalogForPage(pageBM)));
        }

        // GET: Catalog/Details/id
        public ActionResult Details(int? id, string entryType)
        {
            if (User.IsInRole("User"))
            {
                Logger.AddEvent(LogEventType.Warning, "MVC", "CatalogController", "Details", $"User with login \"{User.Identity.Name}\" is attempting retrieve {GetEntryName(entryType)} details");
            }

            var controllerName = GetControllerName(entryType);
            return RedirectToAction("Details", controllerName, new { id });                      
        }

        // GET: Catalog/Delete/id
        public ActionResult Delete(int? id, string entryType)
        {
            if (User.IsInRole("User"))
            {
                Logger.AddEvent(LogEventType.Warning, "MVC", "CatalogController", "Delete", $"User with login \"{User.Identity.Name}\" is attempting delete {GetEntryName(entryType)}");
            }

            var controllerName = GetControllerName(entryType);
            return RedirectToAction("Delete", controllerName, new { id });
        }

        // GET: Catalog/Create/
        public ActionResult Create(string entryType)
        {
            if (User.IsInRole("User"))
            {
                Logger.AddEvent(LogEventType.Warning, "MVC", "CatalogController", "Create", $"User with login \"{User.Identity.Name}\" is attempting create {GetEntryName(entryType)}");
            }

            var controllerName = GetControllerName(entryType);
            return RedirectToAction("Create", controllerName);
        }

        // GET: Catalog/Edit/id
        public ActionResult Edit(int? id, string entryType)
        {
            if (User.IsInRole("User"))
            {
                Logger.AddEvent(LogEventType.Warning, "MVC", "CatalogController", "Edit", $"User with login \"{User.Identity.Name}\" is attempting edit {GetEntryName(entryType)}");
            }

            var controllerName = GetControllerName(entryType);
            return RedirectToAction("Edit", controllerName, new { id });
        }        

        private string GetControllerName(string entryType)
        {
            string controllerName = null;

            if (entryType == nameof(BookVM) || entryType == nameof(BookBM))
            {
                controllerName = "Books";
            }
            else if (entryType == nameof(NewspaperVM) || entryType == nameof(NewspaperBM))
            {
                controllerName = "Newspapers";
            }
            else if (entryType == nameof(PatentVM) || entryType == nameof(PatentBM))
            {
                controllerName = "Patents";
            }

            return controllerName;
        }

        private string GetEntryName(string entryType)
        {
            string name = entryType.ToLower();

            if (name.Contains("book"))
            {
                name = "book";
            }
            else if (name.Contains("newspaper"))
            {
                name = "newspaper";
            }
            else if (name.Contains("patent"))
            {
                name = "patent";
            }

            return name;
        }

        private void SetViewBagSortBy(string sort)
        {
            if (sort == "ASC")
            {
                ViewBag.SortBy = new List<SelectListItem> {

                    new SelectListItem
                    {
                        Text = "Descending",
                        Value = "DESC"                        
                    },
                    new SelectListItem
                    {
                        Text = "Ascending",
                        Value = "ASC",
                        Selected = true
                    }
                };
            }
            else
            {
                ViewBag.SortBy = new List<SelectListItem> {

                    new SelectListItem
                    {
                        Text = "Descending",
                        Value = "DESC",
                        Selected = true
                    },
                    new SelectListItem
                    {
                        Text = "Ascending",
                        Value = "ASC"
                    }
                };
            }
        }       
    }
}