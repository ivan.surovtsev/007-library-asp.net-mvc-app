﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.WebApp.Models.ViewModels;
using Rokolabs.Library.WebApp.Security;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Rokolabs.Library.WebApp.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private IUsersLogic usersLogic;
        private IMapper mapper;

        public UsersController(IUsersLogic usersLogic, IMapper mapper)
        {
            this.usersLogic = usersLogic;
            this.mapper = mapper;
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var users = usersLogic.GetAllUsers();
            var usersVM = mapper.Map<List<UserVM>>(users);

            List<List<SelectListItem>> userRoles = new List<List<SelectListItem>>();         

            foreach (var user in users)
            {
                userRoles.Add(new List<SelectListItem> {
                    new SelectListItem
                    {
                        Text = UserRoles.Admin.ToString(),
                        Value = UserRoles.Admin.ToString()
                    },
                    new SelectListItem
                    {
                        Text = UserRoles.User.ToString(),
                        Value = UserRoles.User.ToString()
                    }
                });

                foreach (var role in userRoles[userRoles.Count - 1])
                {
                    role.Selected = user.Role.ToString() == role.Value;
                }
            }

            ViewBag.Roles = userRoles;

            return View(usersVM);
        }

        [ActionName("setroles")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult SetUserRoles(List<UserVM> users)
        {
            try
            {
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                foreach (var user in users)
                {
                    var roles = manager.GetRoles(user.UserId);

                    foreach (var role in roles)
                    {
                        manager.RemoveFromRole(user.UserId, role);
                    }

                    manager.AddToRole(user.UserId, user.Role.ToString());
                }

                return View("UsersWereSaved");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}