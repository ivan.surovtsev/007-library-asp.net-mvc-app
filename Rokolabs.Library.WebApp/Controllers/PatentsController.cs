﻿using AutoMapper;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApp.Models.ViewModels;
using System.Web.Mvc;

namespace Rokolabs.Library.WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PatentsController : Controller
    {
        private IPatentsLogic logic;
        private IMapper mapper;

        public PatentsController(IPatentsLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        // GET: Patents/Details/id
        public ActionResult Details(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "PatentsController", "Details", $"Requesting for details for patent with id={strId}");

            if (id.HasValue)
            {
                var patent = logic.GetPatentById(id.Value);

                if (patent != null)
                {
                    var patentVM = mapper.Map<PatentVM>(patent);
                    return View(patentVM);
                }
            }

            return RedirectToAction("Index", "Catalog");
        }

        // GET: Patents/Delete/id
        public ActionResult Delete(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "PatentsController", "Delete", $"Requesting for deleting patent with id={strId}");

            if (id.HasValue)
            {
                var patent = logic.GetPatentById(id.Value);

                if (patent != null)
                {
                    var patentVM = mapper.Map<PatentVM>(patent);
                    return View(patentVM);
                }
            }

            return RedirectToAction("Index", "Catalog");            
        }

        // POST: Patents/Delete/id
        [HttpPost]
        public ActionResult Delete(int? id, FormCollection collection)
        {
            if (id.HasValue)
            {
                logic.DeletePatentById(id.Value);
            }

            return RedirectToAction("Index", "Catalog");
        }

        // GET: Patents/Create
        public ActionResult Create()
        {
            Logger.AddEvent(LogEventType.Info, "MVC", "PatentsController", "Create", $"Requesting for creating a patent");

            return View();
        }

        // POST: Patents/Create
        [HttpPost]
        public ActionResult Create(PatentBM patent)
        {
            if (ModelState.IsValid)
            {
                if (patent.DateOfApplication.Value.Year < 1475)
                {
                    ModelState.AddModelError("DateOfApplication", "Date must be > 1473");
                    return View();
                }

                if (patent.PublishingDate.Value.Year < 1475)
                {
                    ModelState.AddModelError("PublishingDate", "Date must be > 1473");
                    return View();
                }

                if (patent.RegistrationNumber.Value.ToString().Length > 9)
                {
                    ModelState.AddModelError("PublishingDate", "Number must be contains less or equal 9 digit");
                    return View();
                }

                if (logic.AddPatent(mapper.Map<Patent>(patent)))
                {
                    Logger.AddEvent(LogEventType.Info, "MVC", "PatentsController", "Create", $"Patent with name \"{patent.Name}\" was created");
                }                

                return RedirectToAction("Index", "Catalog");
            }

            return View();
        }

        // GET: Patents/Edit/id
        public ActionResult Edit(int? id)
        {
            var strId = id?.ToString() ?? "empty";

            Logger.AddEvent(LogEventType.Info, "MVC", "PatentsController", "Edit", $"Requesting for editing patent with id={strId}");

            if (id.HasValue)
            {
                var patent = logic.GetPatentById(id.Value);

                if (patent != null)
                {
                    TempData["authors"] = patent.Authors;
                    TempData["country"] = patent.Country;

                    var patentBM = mapper.Map<PatentBM>(patent);
                    return View(patentBM);
                }
            }

            return RedirectToAction("Index", "Catalog");
        }

        // POST: Patents/Edit/id
        [HttpPost]
        public ActionResult Edit(PatentBM patent)
        {
            if (ModelState.IsValid)
            {
                logic.AddPatent(mapper.Map<Patent>(patent));

                return RedirectToAction("Index", "Catalog");
            }

            return View(patent);
        }
    }
}