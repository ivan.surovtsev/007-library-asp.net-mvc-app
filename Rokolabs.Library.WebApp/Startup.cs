﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Rokolabs.Library.WebApp.Startup))]
namespace Rokolabs.Library.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
