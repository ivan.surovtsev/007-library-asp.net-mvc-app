﻿$.validator.unobtrusive.adapters.addSingleVal("issudateval", "publyear");

$.validator.addMethod("issudateval", function (value, element, publyear) {

    if (value) {      
        
        return value == publyear;
    }

    return true;
});