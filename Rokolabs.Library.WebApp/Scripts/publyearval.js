﻿$.validator.unobtrusive.adapters.addSingleVal("publyearval", "comparedate");
$.validator.unobtrusive.adapters.addSingleVal("publyearval", "currdate");

$.validator.addMethod("publyearval", function (value, element, currdate) {

    if (value) {

        var comparedate = $("#DateOfApplication").val();
        console.log(value + ' ' + currdate)
        return value > comparedate && value <= currdate;
    }

    return true;
});