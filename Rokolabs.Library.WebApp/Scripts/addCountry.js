﻿(function () {

    var addCityURL = '/spec/add/country';

    $(function () {

        $(".addCountryLink").click(function () {

            var options = { "backdrop": "static", keyboard: true };

            $('#modalCountryWindow').modal(options);
            $('#modalCountryWindow').modal('show');
            registerSendBtnHandler();

        });

        $("#closbtn").click(function () {
            $('#modalCityWindow').modal('hide');
        });
    });

    function registerSendBtnHandler() {

        $("#addCountryBtn").click(function () {

            if ($('#CountryName').valid() == true) {

                $.ajax({
                    type: "POST",
                    url: addCityURL,
                    data:
                    {
                        "CountryName": $('#CountryName').val()
                    },
                    datatype: "json",
                    success: function (data) {

                        $('#modalCountryWindow').modal('hide');
                    },
                    error: function () {

                        $('#modalCountryWindow').modal('hide');
                        alert("Error sending!");
                    }
                });

            }
            
        });
    }

})();

