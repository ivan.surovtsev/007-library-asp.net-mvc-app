﻿(function () {

    function Search(selectId, url, getSel2ObjsFun, placeholder) {
        var $select2 = $('#' + selectId);

        if ($select2) {
            $select2.select2({
                ajax: {
                    url: url,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            name: params.term
                        };
                    },
                    processResults: function (data) {

                        return {
                            results: $.map(JSON.parse(data), function (item) {

                                return getSel2ObjsFun(item);
                            })
                        };
                    },
                    cache: true
                },
                placeholder: placeholder,
                minimumInputLength: 1,
                width: "280px"
            });
        }
    }

    function FetchPreselectedItems(selectId, url) {
        var $select2 = $('#' + selectId);

        if ($select2) {
            $.ajax({
                type: 'GET',
                url: url
            }).then(function (data) {

                if (data) {
                    var option;
                    var abort = false;

                    if ($.isArray(data)) {
                        $.each(data, function (idx, element) {

                            if (element.FullName == " ") {
                                abort = true;
                                return;
                            }

                            option = new Option(element.FullName, element.FullName, true, true);
                            $select2.append(option).trigger('change');
                        });
                    }
                    else {
                        option = new Option(data, data, true, true);   
                        $select2.append(option).trigger('change');
                    }

                    if (abort) {
                        return;
                    }

                    $select2.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                }
            });
        }       
    }

    Search(
        'PublicationPlace',
        '/spec/search/city',// SearchCities
        function (item) {
            return {
                text: item.CityName,
                id: item.CityName
            };
        },
        'Searching for a city'
    );

    FetchPreselectedItems(
        'PublicationPlace',
        '/spec/get/city',// GetPublicationPlaceForEditedItem
    );

    Search(
        'SelectedAuthors',
        '/spec/search/authors',// SearchAuthors
        function (item) {
            return {
                text: item.FullName,
                id: item.FullName
            };
        },
        'Searching for a author'
    );

    FetchPreselectedItems(
        'SelectedAuthors',
        '/spec/get/authors',// GetAuthorsForEditedItem
    );

    Search(
        'ForNewspaper',
        '/spec/search/newspaper',// SearchNewspaper
        function (item) {
            return {
                text: item.Name,
                id: item.EntryId
            };
        },
        'Searching for a newspaper'
    );

    Search(
        'Country',
        '/spec/search/country',// SearchCountries
        function (item) {
            return {
                text: item.CountryName,
                id: item.CountryName
            };
        },
        'Searching for a country'
    );

    FetchPreselectedItems(
        'Country',
        '/spec/get/country',// GetCountryForEditedItem
    );

})();

