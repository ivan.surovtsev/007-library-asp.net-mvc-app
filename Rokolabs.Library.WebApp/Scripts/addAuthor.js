﻿(function () {

    var addAuthorURL = '/spec/add/authors';

    $(function () {

        $(".addAuthorLink").click(function () {

            var options = { "backdrop": "static", keyboard: true };

            //$.ajax({
            //    type: "GET",
            //    url: addAuthorURL,
            //    success: function (data) {

            //        $('#modelAuthorWindowContent').html(data);
            //        $('#modalAuthorWindow').modal(options);
            //        $('#modalAuthorWindow').modal('show');

            //        registerSendBtnHandler();            
            //    },
            //    error: function () {
            //        alert("Error!");
            //    }
            //});

            $('#modalAuthorWindow').modal(options);
            $('#modalAuthorWindow').modal('show');
            registerSendBtnHandler();  

        });

        $("#closbtn").click(function () {
            $('#modalAuthorWindow').modal('hide');
        });
    });

    function registerSendBtnHandler() {

        $("#addAuthorBtn").click(function () {

            if ($('#FirstName').valid() == true && $('#LastName').valid() == true) {

                $.ajax({
                    type: "POST",
                    url: addAuthorURL,
                    data:
                    {
                        "FirstName": $('#FirstName').val(),
                        "LastName": $('#LastName').val(),
                    },
                    datatype: "json",
                    success: function (data) {
                        $('#modalAuthorWindow').modal('hide');
                    },
                    error: function () {
                        //console.log(arguments);
                        $('#modalAuthorWindow').modal('hide');
                        alert("Error sending!");
                    }
                });

            }

        });
    }

})();

