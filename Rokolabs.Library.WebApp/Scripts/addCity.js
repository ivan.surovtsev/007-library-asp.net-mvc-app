﻿(function () {

    var addCityURL = '/spec/add/city';

    $(function () {

        $(".addCityLink").click(function () {

            var options = { "backdrop": "static", keyboard: true };

            $('#modalCityWindow').modal(options);
            $('#modalCityWindow').modal('show');
            registerSendBtnHandler();

        });

        $("#closbtn").click(function () {
            $('#modalCityWindow').modal('hide');
        });
    });

    function registerSendBtnHandler() {

        $("#addCityBtn").click(function () {

            if ($('#CityName').valid() == true) {

                $.ajax({
                    type: "POST",
                    url: addCityURL,
                    data:
                    {
                        "CityName": $('#CityName').val()
                    },
                    datatype: "json",
                    success: function (data) {

                        $('#modalCityWindow').modal('hide');
                    },
                    error: function () {

                        $('#modalCityWindow').modal('hide');
                        alert("Error sending!");
                    }
                });

            }
            
        });
    }

})();

