﻿$.validator.unobtrusive.adapters.addSingleVal("firstnameval", "pattern");
$.validator.unobtrusive.adapters.addSingleVal("lastnameval", "pattern");

$.validator.addMethod("firstnameval", function (value, element, pattern) {

    if (value) {

        if (value.length > 50) {
            return false;
        }

        var firstName = new RegExp(pattern);

        if (!firstName.test(value)) {
            return false;
        }
    }

    return true;
});

$.validator.addMethod("lastnameval", function (value, element, pattern) {

    if (value) {

        if (value.length > 200) {
            return false;
        }

        var lastName = new RegExp(pattern);

        if (!lastName.test(value)) {
            return false;
        }
    }

    return true;
});