﻿$.validator.unobtrusive.adapters.addSingleVal("yearval", "year");

$.validator.addMethod("yearval", function (value, element, year) {

    if (value) {

        return value <= year && value >= 1400;
    }

    return true;
});