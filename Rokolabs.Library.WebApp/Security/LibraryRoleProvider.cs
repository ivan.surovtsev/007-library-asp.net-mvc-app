﻿using Rokolabs.Library.BLL.Interfaces;
using System.Web.Security;
using System.Linq;
using System.Web.Mvc;

namespace Rokolabs.Library.WebApp.Security
{
    public class LibraryRoleProvider : RoleProvider
    {     
        private IUsersLogic usersLogic;

        public LibraryRoleProvider()
        {
            usersLogic = DependencyResolver.Current.GetService<IUsersLogic>();
        }

        public override string ApplicationName { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            var user = usersLogic.GetAllUsers().FirstOrDefault(u => u.Login == username);

            if (user != null)
            {
                return new string[] { user.Role.ToString() };
            }
                
            return null;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var user = usersLogic.GetAllUsers().FirstOrDefault(u => u.Login == username);

            if (user != null)
            {
                return user.Role.ToString() == roleName;
            }
           
            return false;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }
    }
}