﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Rokolabs.Library.WebApp.Models.BindingModels;

namespace Rokolabs.Library.WebApp.Security
{
    public class ApplicationUserManager : UserManager<UserExternalLoginModel>
    {
        public ApplicationUserManager(IUserStore<UserExternalLoginModel> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<UserExternalLoginModel>(context.Get<ApplicationDbContext>())); 

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };  

            return manager;
        }
    }
}