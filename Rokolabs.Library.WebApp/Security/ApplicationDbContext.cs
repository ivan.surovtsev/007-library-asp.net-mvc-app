﻿using Microsoft.AspNet.Identity.EntityFramework;
using Rokolabs.Library.WebApp.Models.BindingModels;

namespace Rokolabs.Library.WebApp.Security
{
    public class ApplicationDbContext : IdentityDbContext<UserExternalLoginModel>
    {
        public ApplicationDbContext()
            : base("DefaultConnectionString")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}