﻿using Microsoft.Owin.Security;
using System.Web;
using System.Web.Mvc;

namespace Rokolabs.Library.WebApp.Security
{
    public class ChallengeResult : HttpUnauthorizedResult
    {
        public string RedirectUri { get; set; }

        public string LoginProvider { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };       

            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        }
    }
}