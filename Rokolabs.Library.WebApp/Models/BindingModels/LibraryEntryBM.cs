﻿using Rokolabs.Library.Validation.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public abstract class LibraryEntryBM
    {
        public int EntryID { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "Name must be less or equal 300 characters")]
        [RegularExpression(@"[A-Za-z\dА-ЯЁа-яё\s]*[A-Za-zА-ЯЁа-яё\d]+", ErrorMessage = "Invalid name")]
        public string Name { get; set; }

        [DisplayName("Publication year")]
        [PublicationYear(ErrorMessage = "Publication year must be less or equal current year and greater than 1399")]
        public abstract short? PublicationYear { get; set; }
        
        [DisplayName("Publishing house")]
        [StringLength(300, ErrorMessage = "Length must be less or equal 300 characters")]
        public virtual string PublishingHouse { get; set; }
    
        [DisplayName("Number of pages")]
        [Range(0, 100000, ErrorMessage = "Number of pages must be in range [0-100000]")]
        public virtual int? NumberOfPages { get; set; }
    }
}