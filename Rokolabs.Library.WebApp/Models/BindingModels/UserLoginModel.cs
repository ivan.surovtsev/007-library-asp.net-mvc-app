﻿using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class UserLoginModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }        
    }
}