﻿using Rokolabs.Library.Validation.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class AuthorBM
    {
        [Required]
        [DisplayName("First name")]
        [FirstName]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        [LastName]
        public string LastName { get; set; }        
    }
}