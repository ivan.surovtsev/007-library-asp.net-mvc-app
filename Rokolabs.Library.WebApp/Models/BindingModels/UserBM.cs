﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class UserBM
    {
        [Required]
        [RegularExpression(@"^[A-Za-z]+(_[A-Za-z\d]+)*([A-Za-z\d])$", ErrorMessage = "Incorrect login")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [StringLength(2000, MinimumLength = 3, ErrorMessage = "Password must contains 3 symbols and more")]      
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [Compare("Password", ErrorMessage = "Confirm password field and password do not match")]
        public string ConfirmPassword { get; set; }

        public bool IsValid 
        {
            get
            {
                var escChar = Password?.FirstOrDefault(ch => ch < 32);

                if (escChar != '\0')
                {                    
                    return false;
                }

                var isContainsLogin = Password?.ToUpper().Contains(Login.ToUpper()) ?? false;

                if (isContainsLogin)
                {                    
                    return false;
                }

                return true;
            }
        }
    }
}