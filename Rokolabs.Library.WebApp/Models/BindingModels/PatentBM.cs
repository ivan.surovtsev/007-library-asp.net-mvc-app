﻿using Rokolabs.Library.Entities;
using Rokolabs.Library.Validation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class PatentBM : LibraryEntryBM
    {
        public override short? PublicationYear
        {
            get
            {
                return PublishingDate.HasValue ? (short?)PublishingDate.Value.Year : null;
            }
            set
            {
            }
        }

        [DisplayName("Notes")]
        [StringLength(2000, ErrorMessage = "Length must be less or equal 2000 characters")]
        public string Notes { get; set; }

        [Required(ErrorMessage = "Please choose a country")]
        public string Country { get; set; }
   
        [Required(ErrorMessage = "Please choose a registration number")]
        [DisplayName("Registration number")]
        public int? RegistrationNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayName("Date of application")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]       
        public DateTime? DateOfApplication { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [PublishingDate("DateOfApplication")]
        [DisplayName("Publishing date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? PublishingDate { get; set; }

        [DisplayName("Inventors")] 
        [Required(ErrorMessage = "Please choose at least one inventor")]
        public string[] SelectedAuthors { get; set; }

        public HashSet<Author> Authors { get; set; }

        [Required]
        public override int? NumberOfPages { get; set; }
    }
}