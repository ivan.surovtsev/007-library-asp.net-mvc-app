﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class BookBM : LibraryEntryBM
    {
        [Required(ErrorMessage = "Please choose a publication place")]
        [DisplayName("Publication place")]
        public string PublicationPlace { get; set; }

        public override short? PublicationYear { get; set; }

        [DisplayName("Notes")]
        [StringLength(2000, ErrorMessage = "Notes must be contains less or equal 2000 characters")]
        public string Notes { get; set; }

        [DisplayName("ISBN")]
        [RegularExpression(@"^ISBN ([0—7]|((8[0-9])|(9[0-4]))|(9[5-9][0-3])|(99[4-8][0-9])|(999[0-9][0-9]))-([0-9]{1,7})-([0-9]{1,7})-([0-9X])$",
            ErrorMessage = "Here must be ISBN")]
        public string ISBN { get; set; }

        [DisplayName("Authors")]
        //[Required(ErrorMessage = "Please choose at least one author")]
        public string[] SelectedAuthors { get; set; }

        public HashSet<Author> Authors { get; set; }

        [Required]      
        public override int? NumberOfPages { get; set; }

        [Required]      
        public override string PublishingHouse { get; set; }
    }
}