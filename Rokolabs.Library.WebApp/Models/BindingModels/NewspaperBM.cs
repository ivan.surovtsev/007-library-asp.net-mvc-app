﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class NewspaperBM : LibraryEntryBM
    {     
        [Required]
        [DisplayName("Publication place")]
        public string PublicationPlace { get; set; }

        public override short? PublicationYear { get; set; }

        [DisplayName("Notes")]
        [StringLength(2000, ErrorMessage = "Length must be less or equal 2000 characters")]
        public string Notes { get; set; }

        [DisplayName("ISSN")]
        [RegularExpression(@"^ISSN [0-9]{4}-[0-9]{4}$",
            ErrorMessage = "Here must be ISSN")]
        public string ISSN { get; set; }
  
        public string IssueNumber { get; set; }
     
        public DateTime? IssueDate { get; set; }

        [Required]
        public override string PublishingHouse { get; set; }
    }
}