﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.BindingModels
{
    public class NewspaperIssueBM : IValidatableObject
    {
        [Required]
        [DisplayName("Issue number")]
        public string IssueNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayName("Issue date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? IssueDate { get; set; }

        [Required(ErrorMessage = "Please choose newspaper")]
        [DisplayName("For newspaper")]
        public string ForNewspaper { get; set; }

        [Required]
        [DisplayName("Number of pages")]
        [Range(0, 100000, ErrorMessage = "Number of pages must be in range [0-100000]")]
        public int? NumberOfPages { get; set; }

        public short? PublicationYear
        {
            get
            {
                return IssueDate.HasValue ? (short?)IssueDate.Value.Year : null;
            }
            set
            {
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (IssueDate.HasValue && PublicationYear.HasValue)
            {
                if (IssueDate.Value.Year != PublicationYear.Value)
                {
                    yield return new ValidationResult("Issue date don't match with publication year");
                }
            }
        }
    }
}