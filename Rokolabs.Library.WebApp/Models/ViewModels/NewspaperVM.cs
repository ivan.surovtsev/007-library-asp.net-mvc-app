﻿using System;
using System.ComponentModel;
using System.Text;

namespace Rokolabs.Library.WebApp.Models.ViewModels
{
    public class NewspaperVM : LibraryEntryVM
    {
        [DisplayName("Publication place")]
        public string PublicationPlace { get; set; }

        public override short? PublicationYear
        {
            get
            {
                return IssueDate.HasValue ? (short?)IssueDate.Value.Year : null;
            }
            set
            {
            }
        }

        public string Notes { get; set; }
     
        public string ISSN { get; set; }

        [DisplayName("Issue number")]
        public string IssueNumber { get; set; }
   
        [DisplayName("Issue date")]
        public DateTime? IssueDate { get; set; }       

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append($"{Name} ");

            if (!string.IsNullOrEmpty(IssueNumber))
            {
                builder.Append($"№{IssueNumber}/");
            }

            if (PublicationYear.HasValue)
            {
                builder.Append($"{PublicationYear}");
            }

            return builder.ToString();
        }    
    }
}