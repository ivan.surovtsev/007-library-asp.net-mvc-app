﻿using Rokolabs.Library.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Rokolabs.Library.WebApp.Models.ViewModels
{
    public class PatentVM : LibraryEntryVM
    {
        public override short? PublicationYear
        {
            get
            {
                return PublishingDate.HasValue ? (short?)PublishingDate.Value.Year : null;
            }
            set
            {
            }
        }

        public string Notes { get; set; }

        public string Country { get; set; }

        [DisplayName("Registration number")]
        public int? RegistrationNumber { get; set; }
   
        [DisplayName("Date of application")]
        public DateTime? DateOfApplication { get; set; }      

        [DisplayName("Publishing date")]
        public DateTime? PublishingDate { get; set; }

        [DisplayName("Inventors")]
        public HashSet<Author> Authors { get; set; }
       
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append($"\"{Name}\" ");

            if (PublishingDate.HasValue)
            {
                builder.Append($"от {PublishingDate.Value.ToShortDateString()}");
            }

            return builder.ToString();
        }
    }
}