﻿using System.ComponentModel.DataAnnotations;

namespace Rokolabs.Library.WebApp.Models.ViewModels
{
    public class ExternalLoginConfirmationVM
    {
        [Required]
        [RegularExpression(@"^[A-Za-z]+(_[A-Za-z\d]+)*([A-Za-z\d])$", ErrorMessage = "Incorrect login")]
        public string Login { get; set; }
    }
}