﻿using System.ComponentModel;

namespace Rokolabs.Library.WebApp.Models.ViewModels
{
    public abstract class LibraryEntryVM
    {
        public int EntryID { get; set; }

        public string Name { get; set; }

        [DisplayName("Publication year")]
        public abstract short? PublicationYear { get; set; }

        [DisplayName("Publishing house")]
        public string PublishingHouse { get; set; }

        [DisplayName("Number of pages")]
        public int? NumberOfPages { get; set; }        
    }
}