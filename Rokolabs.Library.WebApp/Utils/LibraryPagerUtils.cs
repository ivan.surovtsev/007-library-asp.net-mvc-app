﻿using PagedList;
using Rokolabs.Library.Entities;
using Rokolabs.Library.WebApp.Models.ViewModels;
using System;
using System.Web.Mvc;

namespace Rokolabs.Library.WebApp.Utils
{
    public static class LibraryPagerUtils
    {
        private const int deltaPage = 3;

        public static MvcHtmlString LibraryPager(this HtmlHelper helper, Page<LibraryEntryVM> list, Func<int, string> generatePageUrl)
        {
            int countOfPages = Convert.ToInt32(Math.Ceiling((double)list.TotalCount / (double)list.PageSize));

            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("pagination");

            // First page link
            ul = InsertLiIntoUl(ul, generatePageUrl(1), "<<", list.PageNumber != 1);

            // Prev page link
            if (list.PageNumber > 1)
            {
                ul = InsertLiIntoUl(ul, generatePageUrl(list.PageNumber - 1), "<");
            }
            else
            {
                ul = InsertLiIntoUl(ul, generatePageUrl(1), "<", false);
            }

            int begin = list.PageNumber - deltaPage;

            if (begin < 1)
            {
                begin = 1;
            }

            int end = list.PageNumber + deltaPage;

            if (end > countOfPages)
            {
                end = countOfPages;
            }

            for (int i = begin; i <= end; i++)
            {
                ul = InsertLiIntoUl(ul, generatePageUrl(i), i.ToString(), i != list.PageNumber);
            }

            // Next page link
            if (list.PageNumber < countOfPages)
            {
                ul = InsertLiIntoUl(ul, generatePageUrl(list.PageNumber + 1), ">");
            }
            else
            {
                ul = InsertLiIntoUl(ul, generatePageUrl(1), ">", false);
            }            

            // Last page link
            ul = InsertLiIntoUl(ul, generatePageUrl(countOfPages), ">>", list.PageNumber != countOfPages);

            return new MvcHtmlString(ul.ToString());
        }

        private static TagBuilder InsertLiIntoUl(TagBuilder ul, string url, string innerText, bool isActive = true)
        {
            TagBuilder li = new TagBuilder("li");
            li.AddCssClass("page-item");

            if (isActive)
            {
                TagBuilder a = new TagBuilder("a");
                a.AddCssClass("page-link");
                a.MergeAttribute("href", url);               
                a.SetInnerText(innerText);
                li.InnerHtml += a.ToString();
            }
            else
            {
                TagBuilder span = new TagBuilder("span");
                span.AddCssClass("page-link");                
                span.SetInnerText(innerText);
                li.InnerHtml += span.ToString();
            }
            
            ul.InnerHtml += li.ToString();

            return ul;
        }
    }
}
