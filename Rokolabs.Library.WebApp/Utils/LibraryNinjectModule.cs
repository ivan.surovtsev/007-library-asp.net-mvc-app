﻿using AutoMapper;
using Ninject.Modules;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.BLL.Logic;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.DAL.MsSqlDal;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Utils;
using Rokolabs.Library.Validators;
using Rokolabs.Library.WebApp.Models.BindingModels;
using Rokolabs.Library.WebApp.Models.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Rokolabs.Library.WebApp.Utils
{
    public class LibraryNinjectModule : NinjectModule    
    {
        public override void Load()
        {
            Kernel.Unbind<ModelValidatorProvider>();

            Bind<IBooksDao>().To<BooksDao>();          
            Bind<IBooksLogic>().To<BooksLogic>().WithPropertyValue("Validator", new BooksValidator());

            Bind<INewspapersDao>().To<NewspapersDao>();
            Bind<INewsapersLogic>().To<NewspapersLogic>().WithPropertyValue("Validator", new NewspapersValidator());

            Bind<IPatentsDao>().To<PatentsDao>();
            Bind<IPatentsLogic>().To<PatentsLogic>().WithPropertyValue("Validator", new PatentsValidator());

            Bind<ILibraryLogic>().To<LibraryLogic>();

            Bind<IAuthorsDao>().To<AuthorsDao>();
            Bind<ICitiesDao>().To<CitiesDao>();
            Bind<ICountriesDao>().To<CountriesDao>();
            Bind<IUsersDao>().To<UsersDao>();

            Bind<IAuthorsLogic>().To<AuthorsLogic>();
            Bind<ICitiesLogic>().To<CitiesLogic>();
            Bind<ICountriesLogic>().To<CountriesLogic>();
            Bind<IUsersLogic>().To<UsersLogic>();
            Bind<ILibraryDao>().To<LibraryDao>();

            var mapConfig = GetMapperConfiguration();
            Bind<IMapper>().To<Mapper>().InSingletonScope().WithConstructorArgument("configurationProvider", mapConfig);       
        }

        private MapperConfiguration GetMapperConfiguration()
        {
            MapperConfiguration mapConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Author, AuthorVM>();
                cfg.CreateMap<AuthorBM, Author>();

                cfg.CreateMap<Book, BookVM>();
                cfg.CreateMap<Newspaper, NewspaperVM>();
                cfg.CreateMap<Patent, PatentVM>();

                cfg.CreateMap<LibraryEntry, LibraryEntryVM>()
                    .Include<Book, BookVM>()
                    .Include<Newspaper, NewspaperVM>()
                    .Include<Patent, PatentVM>();

                cfg.CreateMap<Book, BookBM>();
                cfg.CreateMap<BookBM, Book>().ForMember(m => m.Authors, opt => opt.MapFrom<BookResolver>());

                cfg.CreateMap<NewspaperBM, Newspaper>();
                cfg.CreateMap<Newspaper, NewspaperBM>();

                cfg.CreateMap<PatentBM, Patent>().ForMember(m => m.Authors, opt => opt.MapFrom<PatentResolver>());
                cfg.CreateMap<Patent, PatentBM>();

                cfg.CreateMap<User, UserVM>();

                cfg.CreateMap<UserBM, User>().ConvertUsing(userBM => new User
                {
                    Login = userBM.Login,
                    PasswordHash = HashUtils.GetHash(userBM.Password),
                    Role = UserRoles.User
                });

                cfg.CreateMap<CityBM, City>();
                cfg.CreateMap<CountryBM, Country>();

                cfg.CreateMap<Page<LibraryEntryVM>, Page<LibraryEntry>>();
                cfg.CreateMap<Page<LibraryEntry>, Page<LibraryEntryVM>>();
            });

            return mapConfig;
        }        

        private class BookResolver : IValueResolver<BookBM, Book, HashSet<Author>>
        {
            public HashSet<Author> Resolve(BookBM source, Book destination, HashSet<Author> member, ResolutionContext context)
            {
               return Helper.GetAuthors(source.SelectedAuthors);
            }
        }

        private class PatentResolver : IValueResolver<PatentBM, Patent, HashSet<Author>>
        {
            public HashSet<Author> Resolve(PatentBM source, Patent destination, HashSet<Author> member, ResolutionContext context)
            {
                return Helper.GetAuthors(source.SelectedAuthors);
            }
        }       
    }

    internal static class Helper
    {
        public static HashSet<Author> GetAuthors(string[] authors)
        {
            var authorColl = new HashSet<Author>();

            if (authors == null)
            {
                return authorColl;
            }

            foreach (var author in authors)
            {
                var fullName = author.Split(' ');

                authorColl.Add(
                    new Author
                    {
                        FirstName = fullName[0],
                        LastName = fullName[1]
                    });
            }

            return authorColl;
        }
    }
}