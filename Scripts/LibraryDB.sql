USE [master]
GO
/****** Object:  Database [LibraryDB]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE DATABASE [LibraryDB] 
GO
ALTER DATABASE [LibraryDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LibraryDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LibraryDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LibraryDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LibraryDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LibraryDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LibraryDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LibraryDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LibraryDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LibraryDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LibraryDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LibraryDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LibraryDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LibraryDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LibraryDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LibraryDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LibraryDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LibraryDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LibraryDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LibraryDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LibraryDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LibraryDB] SET  MULTI_USER 
GO
ALTER DATABASE [LibraryDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LibraryDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LibraryDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LibraryDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LibraryDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LibraryDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [LibraryDB] SET QUERY_STORE = OFF
GO
USE [LibraryDB]
GO
/****** Object:  UserDefinedTableType [dbo].[AuthorNames]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE TYPE [dbo].[AuthorNames] AS TABLE(
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](200) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[ListOfId]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE TYPE [dbo].[ListOfId] AS TABLE(
	[Id] [int] NULL
)
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[AuthorID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Originators] PRIMARY KEY CLUSTERED 
(
	[AuthorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[PublicationPlace] [nvarchar](200) NULL,
	[PublishingHouse] [nvarchar](300) NULL,
	[PublicationYear] [smallint] NULL,
	[NumberOfPages] [int] NULL,
	[Notes] [nvarchar](2000) NULL,
	[ISBN] [char](18) NULL,
	[Hash] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BooksAndAuthors]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BooksAndAuthors](
	[Entry_ID] [int] NOT NULL,
	[Author_ID] [int] NOT NULL,
 CONSTRAINT [PK_BooksAndAuthors] PRIMARY KEY CLUSTERED 
(
	[Entry_ID] ASC,
	[Author_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAllBooksFn]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Function for getting all entries from catalog
-- =============================================
CREATE FUNCTION [dbo].[GetAllBooksFn]
(	
)
RETURNS TABLE 
AS
RETURN (SELECT * FROM (SELECT * FROM [dbo].[Books] WHERE IsDeleted = 0) as books
		LEFT JOIN [dbo].[BooksAndAuthors] as booksAndAuthors
		ON books.[EntryID] = booksAndAuthors.[Entry_ID]
		LEFT JOIN [dbo].[Authors] as authors
		ON booksAndAuthors.[Author_ID] = authors.[AuthorID])
GO
/****** Object:  Table [dbo].[Patents]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patents](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
	[Notes] [nvarchar](2000) NULL,
	[NumberOfPages] [int] NULL,
	[Country] [nvarchar](200) NULL,
	[RegistrationNumber] [nvarchar](50) NULL,
	[DateOfApplication] [date] NULL,
	[PublishingDate] [date] NULL,
	[Hash] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Patents] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PatentsAndInventors]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatentsAndInventors](
	[Entry_ID] [int] NOT NULL,
	[Author_ID] [int] NOT NULL,
 CONSTRAINT [PK_PatentsAndInventors] PRIMARY KEY CLUSTERED 
(
	[Entry_ID] ASC,
	[Author_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAllPatentsFn]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Function for getting all patents from catalog
-- =============================================
CREATE FUNCTION [dbo].[GetAllPatentsFn]
(	
)
RETURNS TABLE 
AS
RETURN (SELECT * FROM (SELECT * FROM [dbo].[Patents] WHERE IsDeleted = 0) as patents
		LEFT JOIN [dbo].[PatentsAndInventors] as patentsAndInventors
		ON patents.[EntryID] = patentsAndInventors.[Entry_ID]
		LEFT JOIN [dbo].[Authors] as authors
		ON patentsAndInventors.[Author_ID] = authors.[AuthorID])
GO
/****** Object:  Table [dbo].[Newspapers]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Newspapers](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
	[PublicationPlace] [nvarchar](200) NULL,
	[PublishingHouse] [nvarchar](300) NULL,
	[PublicationYear] [smallint] NULL,
	[NumberOfPages] [int] NULL,
	[Notes] [nvarchar](2000) NULL,
	[ISSN] [char](14) NULL,
	[IssueNumber] [nvarchar](50) NULL,
	[IssueDate] [date] NULL,
	[Hash] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Newspapers] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAllNewspapersFn]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Function for getting all newspapers from catalog
-- =============================================
CREATE FUNCTION [dbo].[GetAllNewspapersFn]
(	
)
RETURNS TABLE 
AS
RETURN (SELECT * FROM [dbo].[Newspapers] WHERE IsDeleted = 0)
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cities](
	[CityID] [int] IDENTITY(1,1) NOT NULL,
	[CountryID] [int] NULL,
	[Name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[CountryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] NOT NULL,
	[Role] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Login] [varchar](100) NOT NULL,
	[PasswordHash] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersAndRoles]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersAndRoles](
	[UserID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_UsersAndRoles] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'202011260857553_InitialCreate', N'Rokolabs.Library.WebApp.Security.ApplicationDbContext', 0x1F8B0800000000000400DD5CDD6EE33616BE2FD0771074D516A9959FCE6036B05B649C643768FE30CE747B37A025DA2146A254914A1D2CF6C9F6621F695F614989FAA34899B2655B2906184422F99DC3C343F2F0E8A3FFF79FFF8E7F5905BEF5026382423CB14F46C7B605B11B7A082F277642173F7EB07FF9F9DB6FC6575EB0B27ECBEB9DF17AAC252613FB99D2E8DC7188FB0C0340460172E390840B3A72C3C0015EE89C1E1FFFCD3939712083B01996658D3F2598A200A60FEC711A6217463401FE5DE8419F88F7AC6496A25AF7208024022E9CD89FC2AFA10FE664748BE631885F47FF84F38B281ACDA09BC488BEDAD6858F00D36B06FD856D018C430A28D3FAFC3381331A8778398BD80BE03FBD4690D55B009F40D19BF3B2BA69C78E4F79C79CB2610EE52684864147C093336129476EBE91BDEDC292CC9657CCE6F495F73AB5E7C4BEF160FAEA53E83303C802CFA77ECC2B4FECBB42C40589EE211DE50D4719E475CCE0FE0CE3AFA32AE29165DCEEA8F0ACD3D131FF77644D139F26319C6098D018F847D66332F791FB2B7C7D0ABF423C393B992FCE3EBC7B0FBCB3F73FC1B377D59EB2BEB27AB517ECD5631C463066BAC145D17FDB72EAED1CB961D1ACD226B30AF32536496CEB0EAC6E215ED267367D4E3FD8D6355A412F7F239CEB33466C4EB146344ED8E37DE23317F66151EEB4CAE4FFB7483D7DF7BE17A9F7E0052DD3A197E4B3891313DBFA04FDB4943CA3289B5EB5F1FE22AA5DC761C09FEBFE95957E998549ECF2CE84DA2A4F205E425AD76EEC94CE6BE4D21CAA7FB7CE5187EFDA5CD3A67B2BABF20E6D32137211FB9E0DB9BEBB956BEC71DC0C572B0A630CFCDB708970BA85B5F99D6E07CBF6BED14784F9EE2B9ED4F0479606A474B7135377C3CC0C7FE5D5F32A00C8EF61F93490C2E298058A0358F4F263C89C15E0CE3A3F0242D8EAE1FD0390E716D5D99F3DA89E474E330A8268E7D21E9F430CEF93600EE37DCAEA6D689EFE0CAF814BC3F80AF3565BE3DD86EED730A157D8BB04147EA66E0EC81F9F50600ED08B3A17AE0B09B966CE0CBD69C8C2F41CF006D3B3D3CE707CF93A74F832F5010AD4F18B7A75FD92B7284399D68A8DA8A6BDB62AC069D33F45E8A47FDE62ADFE594553FD45EDAEFA73CC4EEA8B066BB54FEB992A9F55EE2DB84CC7B2FFE832851D7E78B9DDDEAF5B4A2A669CB10516FE1D6218B355D07B04948F67390226CBCE21628D74F8B8D09D6F6DA9A4DF809FF42D6AA3D990CEB3FE67430A3BFCD990AAC95EBF208F07350667AEBC328337AAAF3ECEAD9F739266FB9E0EB56EEE5BF87ED600DD74B9202474513A0B14D936912BA9EBCF42406B7DE224EB8D9C7C611D638E8E22E6DAEC0DEB9B2D3BD503BE843EA4D0BA70B36CE4141017784D33B20E791D14CB775485626512A6AEDC0F0D99CCD361CC1B017E86226CA6224C9BD302611745C05F6B25A9A5E116C6FB5EC8904B2E61043117B8D61226C2D53917AE4021471A9475161A3B158F6B77C4F6A05737F4861170E905BAC4C85E1CD52C02D7F8AC88ED76E2B44666DC83FF1A19C8440F6DA6F180AE2CCE3F1D9D433E0C0DDB95A5C398C6954560B64F57AE9BF170AE5C37D05B75E5EC2CDCD137A483F1B01DB97E30DF7F186162C3C37971CD3A0373E22CF2656D286B01E33C8318F1114A2B5FCE79215C51C5D190E9294E874404DAB2DF70F019A4F52C52196D2BA360A71D4476A936C0D2EDD6808AEF9E0D20DD64EBA0639E886C5552842B1D60F3FC602BACD83A24D88A2B34B1AB9F812B15F51F8B651F353A02153D2B9CA2E1EB4627960A8EC22FE485ADDE7103A3ACC92D37EDD3212EDF2032AFF4568C508BD5CCC2678D01F31EEECA82B9F71A5B50150E6E1010F66141296AD35830EFE1AE2C285CDCD8808A20A47B18D287F9EAB1424F13384FE1141B5951367632FE997831763444B5F11D8822849715E29A7863CD32D6DAF4C7597702579061382E51F0B80A6D0B49348CC1124AA54C34D3F41AC5845E020AE68027B0A65ED0A8A6DCB6355B4A2EB2BA3337C732DF5BF2DAFCEFBC453B87AFBEA937A31E8178CDFA1BF0082AFD5AA0F00675738BD30A810F62C5078A69E82701D60774FAD6D957CE6AFBEC4D1361EC48FA3722B586ED1A41767D208C86A9394576326445D8B4F9B0E92174C6CF83DEAAF97581B01E25CFCA55517499BA830DA32E70DA62E834816AF7013405DACD0C14449F2A8078D511A3C215698055CACC51EB749E2A66BDC41C51E2EC5421A5A20E5A5699393525AB051BE1692CAAAE612EA1C9C5A9A2374BCD9115AC9C2AB4A278036C85CE729939AA82B8530556149B63972C1E79811DF01EA73D37F5B4C965C7EEED76390DC66ED6C77E36C90AC5A10A5479DD114B90181A60E2FD207D4B7BA2ECC9B7B2DCCB76BEA5C1D0AF4A35AA407D516AE537E8316BDFFF6B0B7F1BFF418FD7CD8377EA278D43A35CA5905E1C1EA543E2581CD8D65F796A9CE0B22AB6959B916DFAAF84C260C42B8C667FF8531F41BEC4E715EE00460B4868C679B14F8F4F4EA57B52C3B9B3E410E2F98A03AFEEE2527DCCF6405FC32F20769F41DC24936C71AFA7046D64CA6FB0075713FB5F69ABF33441C2FF4A5F1F5937E433467F24ACE0294EA0F5EF26B7B69F7B0EED87B381DE4A31B7EACDEF5FB2A647D643CC66CCB9752CD9729311AEDF55E9A44DD6740B6DB6BDC1F276E755EDC68712559A179B5FF09823DACBE58E5CCBEF02B0FABEAB6ACA0B1C5B212A2E69F485D78B0975973036C1D25EC0F0D8234D2F6074EBACFA42C626AA692F6320DC1D4CBE8A61BE1AE52D0FB8E3280E4AFB5892523BAFE5A26F454C3DF416D5A0AC6F35D19BB4F40E705B50CF37F08C37C6DAEE6D775490B27BC33EA46BEF9C893D14F275494C392CE77A9F34EB96AF4A7F6176F570B87B0ABED160C8D3FBF6435DE2F76D104A37E2460FD31105436D30D4E77D3BA22E4BFC361C711366F330FDF0509BF130BCD0785B3E3833B9C984D27CF851A599D7318FB39CFCC4F6E621F3852C4ACDAEABAAF9686DFCDC3502CB2A7AA17A229C2C58378D1AE27515DB95E8D673113DB4765DD46917AB21A1B6C9161B46AB6C51A75DB686BE7908EAB49224A962B5AF59DCDA585A6F9E2ADDE0A6AE67EDAF0B8E5BBFF7BF7966749F06ABCD3ACD47EC374E84EED35C7D4EC70EC4E7E6B76AB63B577EC4934508042D4B08FE939E18BAB57DB9A8738317611E1E481AE555A4BCD21DA4C0639BF6454CD102B89415F3CC787AA33FCD36F2EF3373E8DDE087844609655D86C1DCAFA5E97898D1263F6577D7751E3F44E92FD6F4D105A626E25F141EF0C704F95EA1F7B52293A581E0F18BC843F3B1A43C1FBD7C2D90EE436C0824CC57845D4F30887C06461EF00CBCC04D7463EE770B97C07D2DF3963A90F5035137FBF81281650C022230CAF6EC91F9B017AC7EFE3F19FF8A49CB560000, N'6.1.0-30225')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'5879ad60-9e81-4946-a9ec-d84ed32b3dd6', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd540d9b3-98c1-47a1-aa61-219a05795103', N'User')
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Google', N'107350463971397708409', N'69d52ca0-b3a6-472f-a7b8-dede168b223b')
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Google', N'107891147727303243587', N'884cff8e-2456-402a-bfbe-c6b30f5c1e5f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'69d52ca0-b3a6-472f-a7b8-dede168b223b', N'5879ad60-9e81-4946-a9ec-d84ed32b3dd6')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'884cff8e-2456-402a-bfbe-c6b30f5c1e5f', N'5879ad60-9e81-4946-a9ec-d84ed32b3dd6')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a9a18dae-2fcb-41c4-b0b4-713748590f38', N'5879ad60-9e81-4946-a9ec-d84ed32b3dd6')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e92d3b31-8dcf-4353-a37e-ce495bb7e50d', N'd540d9b3-98c1-47a1-aa61-219a05795103')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'69d52ca0-b3a6-472f-a7b8-dede168b223b', NULL, 0, NULL, N'e7b64766-b0bc-4d65-a00b-76202ac3753a', NULL, 0, 0, NULL, 0, 0, N'Ivan')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'884cff8e-2456-402a-bfbe-c6b30f5c1e5f', NULL, 0, NULL, N'349e3464-b710-424f-93c2-810c0edc53b1', NULL, 0, 0, NULL, 0, 0, N'ilya2')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a9a18dae-2fcb-41c4-b0b4-713748590f38', NULL, 0, N'ADd043bhheLHYOTf7dUa9VPMkiJwtLSGhALxbB/Sh369z+/cjr3gl7DFm9w6Q2IMHw==', N'85b93f77-e0d6-40cc-88ac-d1e8060d265d', NULL, 0, 0, NULL, 0, 0, N'ilya')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e92d3b31-8dcf-4353-a37e-ce495bb7e50d', NULL, 0, N'AFUP8MFz7wz6MwdMAt5wkeACKsfwn/J12I+i53gfS7OScLDYnZsS9bUHK2g3RR8Z9g==', N'a8992734-b666-448e-a2d9-24cdb75d05ff', NULL, 0, 0, NULL, 0, 0, N'User')
GO
SET IDENTITY_INSERT [dbo].[Authors] ON 

INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (274, N'Ab', N'On')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (252, N'Ac', N'Hn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (21, N'Ad', N'Hr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (229, N'Af', N'Ki')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (209, N'Ak', N'Gj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (147, N'Al', N'Mq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (205, N'An', N'Dr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (48, N'Ap', N'Mi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (26, N'Aq', N'Em')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (81, N'Aq', N'Fj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (116, N'Aq', N'Jk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (131, N'Ar', N'No')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (3, N'Artur-Conan', N'Doyle')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (185, N'As', N'Im')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (69, N'Av', N'Kk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (167, N'Aw', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (292, N'Bd', N'Cq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (174, N'Bg', N'Cm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (311, N'Bh', N'Dr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (325, N'Bj', N'Jr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (140, N'Bk', N'Ji')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (17, N'Br', N'Di')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (124, N'Bs', N'Gj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (238, N'Bs', N'Nq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (153, N'Bt', N'Hi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (276, N'Bv', N'Ni')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (126, N'By', N'Bq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (234, N'By', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (137, N'By', N'In')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (2, N'Chase', N'Hadley')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (119, N'Ck', N'Ek')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (269, N'Cl', N'Ep')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (193, N'Cl', N'Hi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (264, N'Cp', N'Br')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (222, N'Cp', N'Im')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (273, N'Cq', N'Di')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (337, N'Cr', N'Lk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (225, N'Ct', N'Fq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (192, N'Ct', N'Ni')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (216, N'Cv', N'Bn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (215, N'Cv', N'Br')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (170, N'Cx', N'Jo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (280, N'Dg', N'Ak')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (92, N'Dg', N'Hp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (333, N'Dg', N'Im')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (149, N'Di', N'Cm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (257, N'Dj', N'Nj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (202, N'Do', N'Ni')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (188, N'Dp', N'Jo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (63, N'Dw', N'Gp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (309, N'Ea', N'Ar')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (345, N'Ec', N'Np')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (189, N'Ed', N'Bq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (32, N'Ed', N'Hr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (186, N'Ee', N'Co')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (177, N'Eg', N'Nr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (338, N'Ei', N'Ok')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (308, N'Ej', N'Ji')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (266, N'Ej', N'Jn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (75, N'El', N'Km')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (214, N'Eo', N'Cl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (236, N'Eq', N'Hl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (155, N'Er', N'Kr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (250, N'Er', N'Lr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (74, N'Es', N'Ci')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (113, N'Es', N'Jk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (56, N'Ev', N'Bj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (223, N'Fb', N'Br')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (64, N'Ff', N'Fp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (339, N'Ff', N'Kj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (132, N'Ff', N'Ol')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (117, N'Fh', N'Gj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (247, N'Fk', N'Ko')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (281, N'Fl', N'Ii')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (195, N'Fm', N'Cr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (317, N'Fm', N'Km')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (106, N'Fn', N'Lj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (151, N'Fs', N'Lm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (16, N'Ft', N'Ok')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (37, N'Fw', N'Cq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (303, N'Fw', N'Iq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (107, N'Ga', N'Kq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (331, N'Gc', N'Di')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (123, N'Gd', N'Aj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (347, N'Gg', N'Dl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (76, N'Gg', N'Fn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (20, N'Gj', N'Oj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (55, N'Gn', N'No')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (315, N'Gp', N'Bo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (326, N'Gq', N'Fp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (323, N'Gq', N'Ki')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (336, N'Gr', N'Nj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (265, N'Gs', N'No')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (85, N'Gt', N'Jl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (290, N'Gv', N'Gm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (29, N'Gw', N'Ii')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (31, N'Gx', N'Jm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (61, N'Hd', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (329, N'Hf', N'Oq')
GO
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (319, N'Hh', N'On')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (182, N'Hi', N'On')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (191, N'Hj', N'Gp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (245, N'Hm', N'Cm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (112, N'Hm', N'Cq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (350, N'Hn', N'Hi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (28, N'Hr', N'Cl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (134, N'Hu', N'Kq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (136, N'Hv', N'Nq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (176, N'Hw', N'Em')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (82, N'Hy', N'Ck')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (30, N'Hy', N'Lp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (288, N'Ia', N'Fk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (221, N'Ia', N'Fq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (211, N'Ie', N'Ei')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (285, N'If', N'Gm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (36, N'Ig', N'Jq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (278, N'Ii', N'Dj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (125, N'Ij', N'Bi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (239, N'Ij', N'Io')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (246, N'Ij', N'Oo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (327, N'Io', N'In')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (127, N'Ip', N'Dm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (235, N'Ir', N'No')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (105, N'Is', N'Nj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (335, N'It', N'Cj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (255, N'It', N'Ij')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (244, N'It', N'Mo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (231, N'It', N'No')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (297, N'Jb', N'Hj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (157, N'Jc', N'Bi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (97, N'Je', N'Dl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (53, N'Jf', N'Ao')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (84, N'Jf', N'Eo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (150, N'Jf', N'Gm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (302, N'Jf', N'Mm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (279, N'Jk', N'Kr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (168, N'Jm', N'Ki')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (43, N'Jw', N'Fk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (287, N'Kb', N'Kn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (161, N'Kb', N'Mi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (233, N'Kd', N'Di')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (72, N'Kg', N'Hk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (23, N'Kk', N'Fi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (142, N'Kp', N'Lq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (39, N'Kq', N'Im')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (226, N'Kq', N'Ko')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (267, N'Kr', N'Io')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (144, N'Kv', N'Fj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (248, N'Kv', N'Nl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (70, N'Lc', N'Kn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (324, N'Ld', N'Cp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (93, N'Li', N'Aj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (340, N'Li', N'Ei')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (301, N'Lk', N'Fr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (203, N'Lm', N'Kk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (275, N'Lp', N'Bq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (100, N'Lt', N'Ck')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (73, N'Lt', N'Er')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (312, N'Lx', N'Cr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (272, N'Mc', N'Hi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (349, N'Md', N'Cq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (86, N'Md', N'Ll')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (183, N'Md', N'Lm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (197, N'Me', N'Dp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (108, N'Me', N'Mi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (332, N'Mg', N'Jj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (187, N'Mj', N'Jo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (346, N'Mj', N'Oq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (115, N'Mk', N'Er')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (237, N'Mk', N'Nl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (201, N'Mo', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (304, N'Mq', N'Ci')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (283, N'Mt', N'Ej')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (59, N'Mu', N'Hk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (58, N'Mw', N'Op')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (344, N'Na', N'Dn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (78, N'Nd', N'Dq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (207, N'Nd', N'Gr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (206, N'Nd', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (204, N'Ne', N'Br')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (230, N'Ng', N'Gi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (316, N'Nh', N'Eq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (54, N'Nh', N'Kn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (22, N'Ni', N'Gk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (87, N'Nj', N'Om')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (164, N'Nl', N'Jl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (198, N'Nl', N'Nl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (122, N'Nn', N'Dp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (262, N'Np', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (99, N'Nr', N'Fi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (173, N'Ns', N'Cl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (253, N'Nw', N'Ek')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (159, N'Oe', N'Ar')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (334, N'Oi', N'Jr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (171, N'Oj', N'Cp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (242, N'On', N'Ii')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (52, N'Oq', N'Im')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (224, N'Os', N'Mm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (289, N'Ou', N'Jn')
GO
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (343, N'Oy', N'Gr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (305, N'Pb', N'Oi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (342, N'Pc', N'Gr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (24, N'Pc', N'Lr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (213, N'Pd', N'Lk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (328, N'Pe', N'Ii')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (160, N'Pe', N'Oi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (110, N'Pi', N'Jm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (104, N'Pi', N'Ok')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (139, N'Pk', N'Ol')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (129, N'Pm', N'Ak')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (282, N'Pn', N'Gi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (199, N'Ps', N'Bp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (172, N'Qc', N'Bi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (299, N'Qe', N'Jk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (291, N'Qf', N'Cn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (318, N'Qf', N'Ei')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (50, N'Qj', N'Ip')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (146, N'Qj', N'Jp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (348, N'Ql', N'Hi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (60, N'Qm', N'Jj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (310, N'Qo', N'Lo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (321, N'Qr', N'In')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (217, N'Qw', N'Dj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (158, N'Qw', N'Lo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (25, N'Re', N'Hr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (259, N'Re', N'Jm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (138, N'Rf', N'Cm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (94, N'Ri', N'Dq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (179, N'Rl', N'Bn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (83, N'Ro', N'Ai')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (35, N'Ru', N'Br')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (141, N'Rv', N'Go')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (268, N'Rv', N'Im')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (42, N'Sa', N'Cp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (243, N'Sa', N'Lq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (270, N'Sc', N'Fn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (194, N'Sc', N'Ko')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (40, N'Sg', N'Im')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (165, N'Sh', N'Bo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (196, N'Sh', N'Fq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (241, N'Si', N'Hq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (44, N'Sl', N'Bi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (330, N'Ss', N'Cm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (34, N'St', N'Cn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (322, N'St', N'Dn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (15, N'Steven', N'King')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (295, N'Su', N'Jn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (88, N'Sv', N'Cq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (218, N'Sy', N'Cl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (240, N'Ta', N'Lo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (47, N'Td', N'Do')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (143, N'Te', N'Gj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (45, N'Tf', N'Oq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (200, N'Tk', N'Hp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (220, N'Tl', N'Mq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (103, N'To', N'Kq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (169, N'Tp', N'Ip')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (212, N'Tq', N'Mm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (190, N'Tr', N'Bq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (41, N'Tr', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (98, N'Tr', N'Li')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (135, N'Tt', N'Gq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (156, N'Tw', N'Om')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (260, N'Tx', N'Kj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (57, N'Tx', N'Om')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (152, N'Ty', N'Ap')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (145, N'Ua', N'Jk')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (114, N'Ue', N'Jr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (320, N'Ug', N'Eq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (219, N'Ug', N'Jl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (118, N'Uh', N'Gr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (180, N'Ui', N'Hq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (181, N'Uj', N'Bo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (27, N'Uj', N'Co')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (148, N'Uj', N'Ll')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (154, N'Uk', N'Gl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (294, N'Up', N'Gl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (314, N'Up', N'Ll')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (210, N'Ur', N'Lr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (162, N'Us', N'Hp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (111, N'Ux', N'Jl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (89, N'Uy', N'Go')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (102, N'Vc', N'Bm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (46, N'Ve', N'El')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (101, N'Vf', N'Om')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (254, N'Vh', N'Fl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (128, N'Vj', N'Bi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (120, N'Vk', N'Mp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (307, N'Vt', N'Kn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (296, N'Vu', N'Km')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (121, N'Vu', N'Kp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (286, N'Vu', N'Lp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (251, N'Vv', N'Ii')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (256, N'Vw', N'Jq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (33, N'Vy', N'Ok')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (306, N'Wc', N'Cp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (51, N'Wc', N'Fj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (261, N'Wd', N'Cn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (90, N'Wf', N'Dr')
GO
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (232, N'Wg', N'Ki')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (271, N'Wh', N'Dl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (80, N'Wj', N'Jp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (38, N'Wl', N'Ck')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (263, N'Wm', N'Hl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (18, N'Wn', N'Ci')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (71, N'Wq', N'Eo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (184, N'Wq', N'Fo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (300, N'Wt', N'Jo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (163, N'Wv', N'Do')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (178, N'Ww', N'Dq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (298, N'Ww', N'Lm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (166, N'Wy', N'Bj')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (258, N'Wy', N'Ho')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (95, N'Xa', N'Jr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (277, N'Xb', N'Do')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (77, N'Xc', N'Kl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (62, N'Xf', N'Al')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (249, N'Xf', N'Nq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (66, N'Xf', N'Oi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (227, N'Xl', N'Gr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (68, N'Xo', N'Ol')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (175, N'Xq', N'Ar')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (293, N'Xq', N'Ir')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (109, N'Xv', N'Bn')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (133, N'Ye', N'Nm')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (341, N'Yj', N'Gl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (91, N'Yk', N'Kr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (284, N'Ym', N'Hi')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (67, N'Ym', N'Oq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (65, N'Yq', N'Eq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (313, N'Yq', N'Jq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (96, N'Yr', N'Iq')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (19, N'Yr', N'Jr')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (79, N'Ys', N'Ok')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (228, N'Yv', N'Io')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (130, N'Yv', N'Lo')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (208, N'Yw', N'Mp')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (49, N'Yy', N'Gl')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (6, N'Александр', N'Дергачёв')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (7, N'Андрей', N'Брилин')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (1, N'Герберт', N'Уэллс')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (10, N'Иван', N'Иванов')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (9, N'Николай', N'Николаев')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (5, N'Николай', N'Разумовский')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (11, N'Пётр', N'Петров')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (14, N'Фёдор', N'Достоевский')
INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName]) VALUES (4, N'Харуки', N'Мураками')
SET IDENTITY_INSERT [dbo].[Authors] OFF
GO
SET IDENTITY_INSERT [dbo].[Books] ON 

INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (1, N'Война миров', N'Москва', N'Эксмо', 1965, 550, N'Фантастика', N'ISBN 99900-123-4-8', 25726, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (2, N'Невидимка', N'Москва', N'Эксмо', 1967, 330, N'Фантастика', N'ISBN 99900-321-4-8', 24596, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (3, N'It''s not my thing', N'Moscow', N'Eksmo', 1993, 127, N'Detective', NULL, 4620, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (4, N'Sign of four', N'Moscow', N'AST', 1999, 153, N'Detective', NULL, 4696, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (5, N'Бесцветный Цкуру Тадзаки', N'Москва', N'Белый город', 2016, 170, N'Роман', N'ISBN 83-357-4789-X', 40915, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (6, N'По ту сторону частиц', N'Санкт-Петербург', N'Питер', 2019, 320, NULL, NULL, 39983, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (32, N'Book 3', NULL, N'House', 2017, NULL, NULL, NULL, 2848, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (33, N'Book 4', NULL, N'House', 2016, NULL, NULL, NULL, 2864, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (34, N'Book 6', NULL, N'House', 2016, NULL, NULL, NULL, 2888, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (35, N'Book 7', NULL, N'House', 2016, NULL, NULL, NULL, 2860, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (36, N'Book 10', NULL, N'House', 2016, NULL, NULL, NULL, 2891, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (37, N'Book 11', NULL, N'House', 2016, NULL, NULL, NULL, 2902, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (38, N'Book 16', N'Stambul', N'House', 2016, 0, NULL, NULL, 2930, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (39, N'Book 17', N'Saratov', N'House', 2017, 0, NULL, NULL, 3954, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (40, N'Book 19', NULL, N'House', 2016, NULL, NULL, NULL, 2918, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (41, N'Book 20', NULL, N'House', 2016, NULL, NULL, NULL, 2897, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (42, N'Book 21', NULL, N'House', 2016, NULL, NULL, NULL, 2911, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (43, N'Book 28', NULL, N'House', 2017, NULL, NULL, NULL, 2905, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (44, N'Book 16', NULL, N'House', 2016, NULL, NULL, NULL, 2915, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (45, N'Book 26', NULL, N'House', 2017, NULL, NULL, NULL, 2929, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (46, N'Book 27', NULL, N'House', 2017, NULL, NULL, NULL, 2923, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (47, N'Book 3', NULL, N'House', 2016, NULL, NULL, NULL, 2887, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (48, N'Book 6', NULL, N'House', 2016, NULL, NULL, NULL, 2873, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (49, N'Book 10', NULL, N'House', 2016, NULL, NULL, NULL, 2919, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (50, N'Book 12', NULL, N'House', 2017, NULL, NULL, NULL, 2906, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (51, N'Book 13', NULL, N'House', 2017, NULL, NULL, NULL, 2913, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (52, N'Book 0', NULL, N'House', 2017, NULL, NULL, NULL, 2862, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (53, N'Book 284', NULL, N'House', 2016, NULL, NULL, NULL, 2972, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (54, N'Book 590', NULL, N'House', 2017, NULL, NULL, NULL, 2952, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (55, N'Book 550', NULL, N'House', 2016, NULL, NULL, NULL, 2964, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (56, N'Book 505', NULL, N'House', 2016, NULL, NULL, NULL, 2959, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (57, N'Book 546', NULL, N'House', 2016, NULL, NULL, NULL, 2968, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (58, N'Book 572', NULL, N'House', 2016, NULL, NULL, NULL, 2954, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (59, N'Book 582', NULL, N'House', 2017, NULL, NULL, NULL, 2956, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (60, N'Test book 1', N'Saratov', N'House', 2020, 123, NULL, NULL, 5081, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (61, N'Book 525', NULL, N'House', 2016, NULL, NULL, NULL, 2980, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (62, N'Book 268', NULL, N'House', 2016, NULL, NULL, NULL, 2951, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (63, N'Book 597', NULL, N'House', 2016, NULL, NULL, NULL, 2965, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (64, N'Book 103', NULL, N'House', 2016, NULL, NULL, NULL, 2943, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (65, N'Book 584', NULL, N'House', 2017, NULL, NULL, NULL, 2963, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (66, N'Book 559', NULL, N'House', 2017, NULL, NULL, NULL, 2967, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (67, N'Book 466', NULL, N'House', 2017, NULL, NULL, NULL, 2984, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (68, N'Book 574', NULL, N'House', 2016, NULL, NULL, NULL, 2977, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (69, N'Book 492', NULL, N'House', 2017, NULL, NULL, NULL, 2966, 1)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (70, N'Book 571', NULL, N'House', 2017, NULL, NULL, NULL, 2961, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (71, N'Book 195', NULL, N'House', 2016, NULL, NULL, NULL, 2946, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (72, N'Book 365', NULL, N'House', 2017, NULL, NULL, NULL, 2978, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (73, N'Book 387', NULL, N'House', 2017, NULL, NULL, NULL, 2987, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (74, N'Book 455', NULL, N'House', 2016, NULL, NULL, NULL, 2976, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (75, N'Book 533', NULL, N'House', 2016, NULL, NULL, NULL, 2955, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (76, N'Book 564', NULL, N'House', 2017, NULL, NULL, NULL, 2975, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (77, N'Book 465', NULL, N'House', 2017, NULL, NULL, NULL, 2950, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (78, N'Book 409', NULL, N'House', 2017, NULL, NULL, NULL, 2986, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (79, N'Book 538', NULL, N'House', 2016, NULL, NULL, NULL, 2981, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (80, N'Book 394', NULL, N'House', 2016, NULL, NULL, NULL, 2949, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (81, N'Book 341', NULL, N'House', 2017, NULL, NULL, NULL, 2962, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (82, N'Book 544', NULL, N'House', 2016, NULL, NULL, NULL, 2960, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (83, N'Book 566', NULL, N'House', 2017, NULL, NULL, NULL, 2974, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (84, N'Book 568', NULL, N'House', 2016, NULL, NULL, NULL, 2970, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (85, N'Book 482', NULL, N'House', 2017, NULL, NULL, NULL, 2989, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (86, N'Book 561', NULL, N'House', 2017, NULL, NULL, NULL, 2953, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (87, N'Book 557', NULL, N'House', 2017, NULL, NULL, NULL, 2948, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (88, N'Book 519', NULL, N'House', 2016, NULL, NULL, NULL, 2979, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (89, N'Book 573', NULL, N'House', 2016, NULL, NULL, NULL, 2957, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (90, N'Book 130', NULL, N'House', 2016, NULL, NULL, NULL, 2941, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (91, N'Book 141', NULL, N'House', 2017, NULL, NULL, NULL, 2936, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (92, N'Book 524', NULL, N'House', 2017, NULL, NULL, NULL, 2988, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (93, N'Book 596', NULL, N'House', 2016, NULL, NULL, NULL, 2973, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (94, N'Book 475', NULL, N'House', 2017, NULL, NULL, NULL, 2992, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (95, N'Book 457', NULL, N'House', 2016, NULL, NULL, NULL, 2958, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (96, N'Book 539', NULL, N'House', 2017, NULL, NULL, NULL, 2982, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (97, N'Book 311', NULL, N'House', 2016, NULL, NULL, NULL, 2947, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (98, N'Book 516', NULL, N'House', 2016, NULL, NULL, NULL, 2944, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (99, N'Book 299', NULL, N'House', 2017, NULL, NULL, NULL, 3005, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (100, N'Book 300', NULL, N'House', 2016, NULL, NULL, NULL, 2939, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (101, N'Book 339', NULL, N'House', 2017, NULL, NULL, NULL, 2983, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (102, N'Book 599', NULL, N'House', 2016, NULL, NULL, NULL, 2969, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (103, N'Book 451', NULL, N'House', 2016, NULL, NULL, NULL, 2940, 1)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (104, N'Book 589', NULL, N'House', 2017, NULL, NULL, NULL, 2985, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (105, N'Book 488', NULL, N'House', 2016, NULL, NULL, NULL, 2998, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (106, N'Book 495', NULL, N'House', 2017, NULL, NULL, NULL, 2994, 1)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (107, N'ItBook', N'Saratov', N'Times', 2015, 40, NULL, NULL, 5391, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (111, N'Test book BOOK', N'Stambul', N'House', 2020, 123, NULL, NULL, 3226, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (112, N'Test book BOOKBOOK', N'Stambul', N'House', 2020, 2131, NULL, NULL, 4969, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (113, N'Testbook', N'Moskow', N'Times', 2010, 100, NULL, NULL, 4288, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (114, N'Book', N'Saratov', N'Times', 2020, 20, NULL, NULL, 2415, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (115, N'Book', N'Saratov', N'Times', 2020, 20, NULL, NULL, 3850, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (116, N'IEditBook', N'Moscow', N'Peares', 2010, 30, NULL, NULL, 4303, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (117, N'CommonBook', N'Saratov', N'Times', 2020, 20, NULL, NULL, 4467, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (118, N'CommonBook2', N'Saratov', N'Times', 2020, 20, NULL, NULL, 4517, 0)
INSERT [dbo].[Books] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISBN], [Hash], [IsDeleted]) VALUES (119, N'CommonBook4', N'Saratov', N'Times', 2020, 20, NULL, NULL, 4519, 0)
SET IDENTITY_INSERT [dbo].[Books] OFF
GO
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (1, 1)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (2, 1)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (3, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (4, 3)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (5, 4)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (6, 5)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (32, 17)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (33, 18)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (34, 19)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (35, 20)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (36, 21)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (37, 22)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (38, 295)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (39, 15)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (39, 295)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (40, 25)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (41, 26)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (42, 27)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (43, 28)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (44, 29)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (45, 30)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (46, 31)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (47, 33)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (48, 34)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (49, 35)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (50, 36)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (51, 37)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (52, 39)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (53, 201)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (54, 347)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (55, 327)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (56, 305)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (57, 326)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (58, 339)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (59, 344)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (60, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (60, 15)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (61, 314)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (62, 189)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (63, 349)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (64, 53)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (65, 345)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (66, 332)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (67, 289)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (68, 341)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (69, 299)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (70, 338)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (71, 157)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (72, 235)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (73, 249)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (74, 284)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (75, 318)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (76, 334)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (77, 288)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (78, 260)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (79, 321)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (80, 252)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (81, 225)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (82, 325)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (83, 336)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (84, 337)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (85, 296)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (86, 333)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (87, 331)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (88, 312)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (89, 340)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (90, 119)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (91, 123)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (92, 313)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (93, 348)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (94, 293)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (95, 285)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (96, 322)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (97, 214)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (98, 309)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (99, 208)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (100, 209)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (101, 224)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (102, 350)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (103, 280)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (104, 346)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (105, 298)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (106, 300)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (107, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (107, 274)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (112, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (112, 222)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (113, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (113, 274)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (115, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (115, 274)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (116, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (116, 274)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (117, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (117, 274)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (118, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (118, 274)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (119, 2)
INSERT [dbo].[BooksAndAuthors] ([Entry_ID], [Author_ID]) VALUES (119, 274)
GO
SET IDENTITY_INSERT [dbo].[Cities] ON 

INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (1, NULL, N'Москва')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (2, NULL, N'Саратов')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (3, NULL, N'Самара')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (4, NULL, N'Волгоград')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (5, NULL, N'Санкт-Петербург')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (6, NULL, N'New York')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (7, NULL, N'Stambul')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (8, NULL, N'Paris')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (9, NULL, N'Астрахань')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (11, NULL, N'Ростов')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (12, NULL, N'Уфа')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (13, NULL, N'Praga')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (15, NULL, N'Moskow')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (16, NULL, N'Барнаул')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (17, NULL, N'Saratov')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (18, NULL, N'Ростов-на-Дону')
INSERT [dbo].[Cities] ([CityID], [CountryID], [Name]) VALUES (19, NULL, N'Moscow')
SET IDENTITY_INSERT [dbo].[Cities] OFF
GO
SET IDENTITY_INSERT [dbo].[Countries] ON 

INSERT [dbo].[Countries] ([CountryID], [Name]) VALUES (6, N'Canada')
INSERT [dbo].[Countries] ([CountryID], [Name]) VALUES (8, N'China')
INSERT [dbo].[Countries] ([CountryID], [Name]) VALUES (5, N'France')
INSERT [dbo].[Countries] ([CountryID], [Name]) VALUES (1, N'Russia')
INSERT [dbo].[Countries] ([CountryID], [Name]) VALUES (7, N'Spain')
INSERT [dbo].[Countries] ([CountryID], [Name]) VALUES (4, N'Канада')
INSERT [dbo].[Countries] ([CountryID], [Name]) VALUES (2, N'Россия')
SET IDENTITY_INSERT [dbo].[Countries] OFF
GO
SET IDENTITY_INSERT [dbo].[Newspapers] ON 

INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (1, N'АиФ', N'Москва', N'Москва Медиа', 2020, 35, NULL, NULL, N'125', CAST(N'2020-11-03' AS Date), 17020, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (2, N'Комсомольская правда', N'Москва', N'Комсомольская правда', 2020, 27, NULL, NULL, N'127', CAST(N'2020-11-03' AS Date), 43186, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (3, N'Associated Press', N'New-York City', N'Associated Press', 2020, 35, NULL, N'ISSN 1234-5678', N'327', CAST(N'2020-11-03' AS Date), 5158, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (5, N'АиФ', N'Москва', N'Москва Медиа', 2020, 35, NULL, N'ISSN 8005-4321', N'126', CAST(N'2020-11-04' AS Date), 17218, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (6, N'АиФ', N'Москва', N'Москва Медиа', 2020, 35, NULL, N'ISSN 8005-4321', N'127', CAST(N'2020-11-05' AS Date), 17139, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (52, N'test newspaper', N'Saratov', N'House', 2020, 123, NULL, NULL, N'137', CAST(N'2020-11-16' AS Date), 3906, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (53, N'test newspaper 131', N'Saratov', N'House', 2020, 123, NULL, NULL, N'250', CAST(N'2020-11-19' AS Date), 4326, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (54, N'test newspaper345', N'Saratov', N'House', 2020, 0, NULL, NULL, NULL, CAST(N'2020-12-01' AS Date), 2133, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (55, N'Testnewspaper', N'Moskow', N'Times', 2020, 10, NULL, NULL, N'1A', CAST(N'2020-12-01' AS Date), 3866, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (56, N'Testnewspaper2', N'Saratov', N'Times', 2020, 10, NULL, NULL, N'1A', CAST(N'2020-12-01' AS Date), 3916, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (57, N'Testnewspaper2', N'Saratov', N'Times', 2020, 20, NULL, NULL, N'2A', CAST(N'2020-12-02' AS Date), 4114, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (59, N'IEditNewspaper', N'Saratov', N'Times', 2020, 20, NULL, NULL, N'1AT', CAST(N'2020-10-02' AS Date), 3681, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (60, N'I2Newspaper', N'Saratov', N'Times', 2020, 20, NULL, NULL, N'1A', CAST(N'2020-12-02' AS Date), 3739, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (62, N'CommonNewspaper', N'Saratov', N'Times', 2020, 20, NULL, NULL, N'1A', CAST(N'2020-12-02' AS Date), 4233, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (63, N'CommonNewspaper2', N'Saratov', N'Times', 2020, 20, NULL, NULL, N'1A', CAST(N'2020-12-02' AS Date), 4283, 0)
INSERT [dbo].[Newspapers] ([EntryID], [Name], [PublicationPlace], [PublishingHouse], [PublicationYear], [NumberOfPages], [Notes], [ISSN], [IssueNumber], [IssueDate], [Hash], [IsDeleted]) VALUES (64, N'CommonNewspaper4', N'Saratov', N'Times', 2020, 20, NULL, NULL, N'1A', CAST(N'2020-12-02' AS Date), 4285, 0)
SET IDENTITY_INSERT [dbo].[Newspapers] OFF
GO
SET IDENTITY_INSERT [dbo].[Patents] ON 

INSERT [dbo].[Patents] ([EntryID], [Name], [Notes], [NumberOfPages], [Country], [RegistrationNumber], [DateOfApplication], [PublishingDate], [Hash], [IsDeleted]) VALUES (3, N'test patent, mine', NULL, 12, N'Canada', NULL, CAST(N'2020-11-14' AS Date), CAST(N'2020-11-15' AS Date), 568, 0)
INSERT [dbo].[Patents] ([EntryID], [Name], [Notes], [NumberOfPages], [Country], [RegistrationNumber], [DateOfApplication], [PublishingDate], [Hash], [IsDeleted]) VALUES (6, N'test patent PATENT123', NULL, 221, N'Russia', N'1213', CAST(N'2020-11-19' AS Date), CAST(N'2020-11-20' AS Date), 1844, 0)
INSERT [dbo].[Patents] ([EntryID], [Name], [Notes], [NumberOfPages], [Country], [RegistrationNumber], [DateOfApplication], [PublishingDate], [Hash], [IsDeleted]) VALUES (7, N'Testpatent', NULL, 100, N'Canada', N'123123123', CAST(N'2010-11-26' AS Date), CAST(N'2010-12-01' AS Date), 123123691, 0)
INSERT [dbo].[Patents] ([EntryID], [Name], [Notes], [NumberOfPages], [Country], [RegistrationNumber], [DateOfApplication], [PublishingDate], [Hash], [IsDeleted]) VALUES (8, N'IPatent', NULL, 20, N'Russia', N'123123123', CAST(N'2020-10-05' AS Date), CAST(N'2020-10-20' AS Date), 123123754, 0)
INSERT [dbo].[Patents] ([EntryID], [Name], [Notes], [NumberOfPages], [Country], [RegistrationNumber], [DateOfApplication], [PublishingDate], [Hash], [IsDeleted]) VALUES (9, N'IEditPatent', NULL, 30, N'China', N'123123123', CAST(N'2020-10-05' AS Date), CAST(N'2020-10-20' AS Date), 123123606, 0)
INSERT [dbo].[Patents] ([EntryID], [Name], [Notes], [NumberOfPages], [Country], [RegistrationNumber], [DateOfApplication], [PublishingDate], [Hash], [IsDeleted]) VALUES (10, N'CommonPatent', NULL, 20, N'China', N'0', CAST(N'2020-10-05' AS Date), CAST(N'2020-10-20' AS Date), 483, 0)
INSERT [dbo].[Patents] ([EntryID], [Name], [Notes], [NumberOfPages], [Country], [RegistrationNumber], [DateOfApplication], [PublishingDate], [Hash], [IsDeleted]) VALUES (11, N'CommonPatent2', NULL, 20, N'China', N'100000000', CAST(N'2020-10-05' AS Date), CAST(N'2020-10-20' AS Date), 100000483, 0)
SET IDENTITY_INSERT [dbo].[Patents] OFF
GO
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (3, 15)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (6, 2)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (6, 222)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (6, 337)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (7, 2)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (8, 2)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (8, 274)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (9, 2)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (9, 274)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (10, 2)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (10, 274)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (11, 2)
INSERT [dbo].[PatentsAndInventors] ([Entry_ID], [Author_ID]) VALUES (11, 274)
GO
INSERT [dbo].[Roles] ([RoleID], [Role]) VALUES (1, N'User')
INSERT [dbo].[Roles] ([RoleID], [Role]) VALUES (2, N'Admin')
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserID], [Login], [PasswordHash]) VALUES (1, N'ivan', N'SusgALneWFj15eC37aUvJTyvGVgsZ8u7RTvmmH7MG68n11Zw4594BY+x6+49Frg9HL3I02KGNjd7JFjqW/Ev8g==')
INSERT [dbo].[Users] ([UserID], [Login], [PasswordHash]) VALUES (11, N'petr', N'+xMbxXpHfIydBo8e5WIqwwQZWncWTMwtddgt/hpye6jWdO2H+WFDsrQWqs77VV4wRcNW+qI+bSHecrhYIuOf3Q==')
INSERT [dbo].[Users] ([UserID], [Login], [PasswordHash]) VALUES (12, N'EditUser', N'PJkJr+wlNU1VHa4hWQuybjjVPyFzuNPcPu5MBH56scHri4UQPjvnumE7MbtcnDYhTcnxSkL9ei/bhIVrylxEwg==')
INSERT [dbo].[Users] ([UserID], [Login], [PasswordHash]) VALUES (14, N'admin', N'PJkJr+wlNU1VHa4hWQuybjjVPyFzuNPcPu5MBH56scHri4UQPjvnumE7MbtcnDYhTcnxSkL9ei/bhIVrylxEwg==')
INSERT [dbo].[Users] ([UserID], [Login], [PasswordHash]) VALUES (16, N'TT', N'PJkJr+wlNU1VHa4hWQuybjjVPyFzuNPcPu5MBH56scHri4UQPjvnumE7MbtcnDYhTcnxSkL9ei/bhIVrylxEwg==')
INSERT [dbo].[Users] ([UserID], [Login], [PasswordHash]) VALUES (17, N'testr', N'PJkJr+wlNU1VHa4hWQuybjjVPyFzuNPcPu5MBH56scHri4UQPjvnumE7MbtcnDYhTcnxSkL9ei/bhIVrylxEwg==')
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
INSERT [dbo].[UsersAndRoles] ([UserID], [RoleID]) VALUES (1, 2)
INSERT [dbo].[UsersAndRoles] ([UserID], [RoleID]) VALUES (11, 1)
INSERT [dbo].[UsersAndRoles] ([UserID], [RoleID]) VALUES (12, 1)
INSERT [dbo].[UsersAndRoles] ([UserID], [RoleID]) VALUES (14, 2)
INSERT [dbo].[UsersAndRoles] ([UserID], [RoleID]) VALUES (16, 1)
INSERT [dbo].[UsersAndRoles] ([UserID], [RoleID]) VALUES (17, 1)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_RoleId]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 12/6/2020 8:51:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Originators]    Script Date: 12/6/2020 8:51:48 PM ******/
ALTER TABLE [dbo].[Authors] ADD  CONSTRAINT [IX_Originators] UNIQUE NONCLUSTERED 
(
	[FirstName] ASC,
	[LastName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Cities]    Script Date: 12/6/2020 8:51:48 PM ******/
ALTER TABLE [dbo].[Cities] ADD  CONSTRAINT [IX_Cities] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Countries]    Script Date: 12/6/2020 8:51:48 PM ******/
ALTER TABLE [dbo].[Countries] ADD  CONSTRAINT [IX_Countries] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Books] ADD  CONSTRAINT [DF_Books_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Newspapers] ADD  CONSTRAINT [DF_Newspapers_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Patents] ADD  CONSTRAINT [DF_Patents_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[BooksAndAuthors]  WITH CHECK ADD  CONSTRAINT [FK_BooksAndAuthors_Authors] FOREIGN KEY([Author_ID])
REFERENCES [dbo].[Authors] ([AuthorID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BooksAndAuthors] CHECK CONSTRAINT [FK_BooksAndAuthors_Authors]
GO
ALTER TABLE [dbo].[BooksAndAuthors]  WITH CHECK ADD  CONSTRAINT [FK_BooksAndAuthors_Books] FOREIGN KEY([Entry_ID])
REFERENCES [dbo].[Books] ([EntryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BooksAndAuthors] CHECK CONSTRAINT [FK_BooksAndAuthors_Books]
GO
ALTER TABLE [dbo].[PatentsAndInventors]  WITH CHECK ADD  CONSTRAINT [FK_PatentsAndInventors_Authors] FOREIGN KEY([Author_ID])
REFERENCES [dbo].[Authors] ([AuthorID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PatentsAndInventors] CHECK CONSTRAINT [FK_PatentsAndInventors_Authors]
GO
ALTER TABLE [dbo].[PatentsAndInventors]  WITH CHECK ADD  CONSTRAINT [FK_PatentsAndInventors_Patents] FOREIGN KEY([Entry_ID])
REFERENCES [dbo].[Patents] ([EntryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PatentsAndInventors] CHECK CONSTRAINT [FK_PatentsAndInventors_Patents]
GO
ALTER TABLE [dbo].[UsersAndRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersAndRoles_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsersAndRoles] CHECK CONSTRAINT [FK_UsersAndRoles_Roles]
GO
ALTER TABLE [dbo].[UsersAndRoles]  WITH CHECK ADD  CONSTRAINT [FK_UsersAndRoles_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsersAndRoles] CHECK CONSTRAINT [FK_UsersAndRoles_Users]
GO
/****** Object:  StoredProcedure [dbo].[AddAuthor]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 10/31/2020
-- Description:	Adding a new author in catalog
-- =============================================
CREATE PROCEDURE [dbo].[AddAuthor]	 
	@firstName nvarchar(50), @lastName nvarchar(200)	
AS
BEGIN	

	-- Adding author and getting his id
	DECLARE @authorId int, @entryId int

	SELECT @authorId = [AuthorID] FROM [dbo].[Authors] WHERE [FirstName] = @firstName AND [LastName] = @lastName

	IF @authorId IS NULL
	BEGIN

		INSERT INTO [dbo].[Authors]
           ([FirstName]
           ,[LastName])
		VALUES
           (@firstName
           ,@lastName)

		SET @authorId = SCOPE_IDENTITY()

	END			

	SELECT @authorId	

END
GO
/****** Object:  StoredProcedure [dbo].[AddBook]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Adding a new book in catalog
-- =============================================
CREATE PROCEDURE [dbo].[AddBook]
	@name nvarchar(300), @publicationPlace nvarchar(200), @publishingHouse nvarchar(300),
	@publicationYear smallint, @numberOfPages int, @notes nvarchar(2000) NULL,
	@ISBN char(18) NULL, @hash int, @entryID int, @authors as [dbo].[AuthorNames] READONLY
AS
BEGIN
	
	IF (@ISBN is NULL OR NOT EXISTS (SELECT [EntryID] FROM [Books] WHERE ISBN = @ISBN AND IsDeleted = 0 AND [EntryID] <> @entryID)) 
	AND NOT EXISTS (SELECT [EntryID] FROM [Books] WHERE [Hash] = @hash AND IsDeleted = 0 AND [EntryID] <> @entryID)
	BEGIN

		-- Add city for publication place	
		EXEC [dbo].[AddCity] @name = @publicationPlace		

		-- If entry isn't exists
		IF @entryID = 0
		BEGIN	
	
			-- Adding to 'Books' table
			INSERT INTO [dbo].[Books]
				   ([Name]
				   ,[PublicationPlace]
				   ,[PublishingHouse]
				   ,[PublicationYear]
				   ,[NumberOfPages]
				   ,[Notes]
				   ,[ISBN]	
				   ,[Hash])
			 VALUES
				   (@name
				   ,@publicationPlace
				   ,@publishingHouse
				   ,@publicationYear
				   ,@numberOfPages
				   ,@notes
				   ,@ISBN			
				   ,@hash)

			SET @entryID = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN

			UPDATE [dbo].[Books]
			SET [Name] = @name
				,[PublicationPlace] = @publicationPlace
				,[PublishingHouse] = @publishingHouse
				,[PublicationYear] = @publicationYear
				,[NumberOfPages] = @numberOfPages
				,[Notes] = @notes
				,[ISBN] = @ISBN
				,[Hash] = @hash
			WHERE [EntryID] = @entryID

		END

		-- Adding authors
		MERGE [dbo].[Authors] targetTable 
			USING @authors sourseTable
		ON (targetTable.[FirstName] = sourseTable.[FirstName] AND targetTable.[LastName] = sourseTable.[LastName])	
		WHEN NOT MATCHED BY TARGET 
			THEN INSERT ([FirstName], [LastName])
			VALUES (sourseTable.[FirstName], sourseTable.[LastName]);		
	
		DECLARE @tmpTable TABLE
		(
			[Entry_ID] int,
			[Author_ID] int
		)

		-- Getting author IDs for added book
		INSERT INTO @tmpTable 
			([Entry_ID], [Author_ID])
		SELECT @entryID, [AuthorID] FROM [dbo].[Authors] authors1
		JOIN @authors authors2 
		ON authors1.[FirstName] = authors2.[FirstName] AND authors1.[LastName] = authors2.[LastName]	

		-- Setting relationships
		MERGE [dbo].[BooksAndAuthors] targetTable 
			USING @tmpTable sourseTable
		ON (targetTable.[Author_ID] = sourseTable.[Author_ID] AND targetTable.[Entry_ID] = sourseTable.[Entry_ID])	
		WHEN NOT MATCHED BY TARGET 
			THEN INSERT ([Entry_ID], [Author_ID])
			VALUES (sourseTable.[Entry_ID], sourseTable.[Author_ID])
		WHEN NOT MATCHED BY SOURCE AND targetTable.[Entry_ID] = @entryId
			THEN DELETE;
		
		SELECT [Entry_ID], [Author_ID] FROM @tmpTable
	
	END
END
GO
/****** Object:  StoredProcedure [dbo].[AddCity]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/12/2020
-- Description:	Adding a new city in catalog
-- =============================================
CREATE PROCEDURE [dbo].[AddCity]
	@name nvarchar(300)
AS
BEGIN

	IF @name IS NOT NULL AND NOT EXISTS(SELECT [CityID] FROM [dbo].[Cities] WHERE [Name] = @name)
	BEGIN

		INSERT INTO [dbo].[Cities]
			   ([Name])
		 VALUES
			   (@name)

	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[AddCountry]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/12/2020
-- Description:	Adding a new country in catalog
-- =============================================
CREATE PROCEDURE [dbo].[AddCountry]
	@name nvarchar(200)
AS
BEGIN

	IF @name IS NOT NULL AND NOT EXISTS(SELECT [CountryID] FROM [dbo].[Countries] WHERE [Name] = @name)
	BEGIN

		INSERT INTO [dbo].[Countries]
			   ([Name])
		 VALUES
			   (@name)

	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[AddNewspaper]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Adding a new newspaper in catalog
-- =============================================
CREATE PROCEDURE [dbo].[AddNewspaper]
	@name nvarchar(300), @publicationPlace nvarchar(200), @publishingHouse nvarchar(300),
	@publicationYear smallint, @numberOfPages int, @notes nvarchar(2000) NULL,
	@ISSN char(14) NULL, @issueNumber nvarchar(50), @issueDate date,
	@hash int, @entryID int, @isIssue bit
AS
BEGIN
	IF (@ISSN is null OR NOT EXISTS (SELECT [EntryID] FROM [Newspapers] WHERE [ISSN] = @ISSN AND IsDeleted = 0 AND [EntryID] <> @entryID)) 
	AND NOT EXISTS (SELECT [EntryID] FROM [Newspapers] WHERE [Hash] = @hash AND IsDeleted = 0 AND [EntryID] <> @entryID)
	BEGIN

		-- Add city for publication place	
		EXEC [dbo].[AddCity] @name = @publicationPlace

		IF @entryID = 0 OR (@isIssue = 1 AND EXISTS (SELECT [EntryID] FROM [Newspapers] WHERE [EntryID] = @entryID AND [IssueNumber] is not null))
		BEGIN	
	
			-- Adding to 'Newspapers' table
			INSERT INTO [dbo].[Newspapers]
				   ([Name]
				   ,[PublicationPlace]
				   ,[PublishingHouse]
				   ,[PublicationYear]
				   ,[NumberOfPages]
				   ,[Notes]			
				   ,[ISSN]
				   ,[IssueNumber]
				   ,[IssueDate]			   
				   ,[Hash])
			 VALUES
				   (@name
				   ,@publicationPlace
				   ,@publishingHouse
				   ,@publicationYear
				   ,@numberOfPages
				   ,@notes			  
				   ,@ISSN
				   ,@issueNumber
				   ,@issueDate			   
				   ,@hash)

			SELECT SCOPE_IDENTITY()

		END
		ELSE
		BEGIN
			
			UPDATE [dbo].[Newspapers]
					SET [Name] = @name
					  ,[PublicationPlace] = @publicationPlace
					  ,[PublishingHouse] = @publishingHouse
					  ,[PublicationYear] = @publicationYear
					  ,[NumberOfPages] = @numberOfPages
					  ,[Notes] = @notes
					  ,[ISSN] = @ISSN
					  ,[IssueNumber] = @issueNumber
					  ,[IssueDate] = @issueDate
					  ,[Hash] = @hash
					WHERE [EntryID] = @entryID

			SELECT [EntryID] FROM [dbo].[Newspapers] 
			WHERE [Hash] = @hash or [EntryID] = @entryID

		END	
	END
END
GO
/****** Object:  StoredProcedure [dbo].[AddPatent]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Adding a new patent in catalog
-- =============================================
CREATE PROCEDURE [dbo].[AddPatent]
	@name nvarchar(300), @numberOfPages int, @notes nvarchar(2000) NULL,	
	@country nvarchar(200), @registrationNumber int, @dateOfApplication date NULL,
	@publishingDate date, @hash int, @entryID int, @inventors as [dbo].[AuthorNames] READONLY
AS
BEGIN
	IF NOT EXISTS (SELECT [EntryID] FROM [Patents] WHERE [Hash] = @hash AND IsDeleted = 0 AND [EntryID] <> @entryID)
	BEGIN

		-- Add country for publication place	
		EXEC [dbo].[AddCountry] @name = @country

		-- If entry isn't exists
		IF @entryID = 0
		BEGIN	
	
			-- Adding to 'Patents' table
			INSERT INTO [dbo].[Patents]
				   ([Name]			   
				   ,[NumberOfPages]
				   ,[Notes]			   
				   ,[Country]
				   ,[RegistrationNumber]
				   ,[DateOfApplication]
				   ,[PublishingDate]
				   ,[Hash])
			 VALUES
				   (@name			   
				   ,@numberOfPages
				   ,@notes			  
				   ,@country
				   ,@registrationNumber
				   ,@dateOfApplication
				   ,@publishingDate
				   ,@hash)

			 SET @entryID = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN

			UPDATE [dbo].[Patents]
			SET [Name] = @name
			,[Notes] = @notes
			,[NumberOfPages] = @numberOfPages
			,[Country] = @country
			,[RegistrationNumber] = @registrationNumber
			,[DateOfApplication] = @dateOfApplication
			,[PublishingDate] = @publishingDate
			,[Hash] =@hash		
			WHERE [EntryID] = @entryID

		END

		-- Adding authors
		MERGE [dbo].[Authors] targetTable 
			USING @inventors sourseTable
		ON (targetTable.[FirstName] = sourseTable.[FirstName] AND targetTable.[LastName] = sourseTable.[LastName])	
		WHEN NOT MATCHED BY TARGET 
			THEN INSERT ([FirstName], [LastName])
			VALUES (sourseTable.[FirstName], sourseTable.[LastName]);

		DECLARE @tmpTable TABLE
		(
			[Entry_ID] int,
			[Author_ID] int
		)

		-- Getting author IDs for added patent
		INSERT INTO @tmpTable 
			([Entry_ID], [Author_ID])
		SELECT @entryID, [AuthorID] FROM [dbo].[Authors] authors1
		JOIN @inventors authors2 
		ON authors1.[FirstName] = authors2.[FirstName] AND authors1.[LastName] = authors2.[LastName]
	
		-- Setting relationships
		MERGE [dbo].[PatentsAndInventors] targetTable 
			USING @tmpTable sourseTable
		ON (targetTable.[Author_ID] = sourseTable.[Author_ID] AND targetTable.[Entry_ID] = sourseTable.[Entry_ID])	
		WHEN NOT MATCHED BY TARGET 
			THEN INSERT ([Entry_ID], [Author_ID])
			VALUES (sourseTable.[Entry_ID], sourseTable.[Author_ID])
		WHEN NOT MATCHED BY SOURCE AND targetTable.[Entry_ID] = @entryId
			THEN DELETE;
		
		SELECT [Entry_ID], [Author_ID] FROM @tmpTable
	
	END
END
GO
/****** Object:  StoredProcedure [dbo].[AddUser]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 18.11.2020
-- Description:	Adding a new user
-- =============================================
CREATE PROCEDURE [dbo].[AddUser]
	@login varchar(100), @passwordHash nvarchar(200), @roleID int
AS
BEGIN
	
	IF NOT EXISTS (SELECT [UserID] FROM [dbo].[Users] WHERE [Login] = @login)
	BEGIN

		INSERT INTO [dbo].[Users]
           ([Login]
           ,[PasswordHash])
		VALUES
           (@login
           ,@passwordHash)

		DECLARE @userID int
		SET @userID = SCOPE_IDENTITY()

		IF NOT EXISTS (SELECT [UserID] FROM [dbo].[UsersAndRoles] WHERE [UserID] = @userID AND [RoleID] = @roleID)
		BEGIN

			INSERT INTO [dbo].[UsersAndRoles]
			   ([UserID]
			   ,[RoleID])
			 VALUES
			   (@userID
			   ,@roleID)

		END

		RETURN 1

	END

	RETURN 0

END
GO
/****** Object:  StoredProcedure [dbo].[DeleteBook]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 10/31/2020
-- Description:	Deleting a book from catalog
-- =============================================
CREATE PROCEDURE [dbo].[DeleteBook] 
	@entryID int
AS
BEGIN
	
	UPDATE [Books] SET IsDeleted = 1 WHERE [EntryID] = @entryID

END
GO
/****** Object:  StoredProcedure [dbo].[DeleteNewspaper]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Deleting a newspaper from catalog
-- =============================================
CREATE PROCEDURE [dbo].[DeleteNewspaper] 
	@entryID int, @entryHash int
AS
BEGIN
	
	UPDATE [Newspapers] SET IsDeleted = 1 WHERE [EntryID] = @entryID

END
GO
/****** Object:  StoredProcedure [dbo].[DeletePatent]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Deleting a patent from catalog
-- =============================================
CREATE PROCEDURE [dbo].[DeletePatent] 
	@entryID int, @entryHash int
AS
BEGIN
	
	UPDATE [Patents] SET IsDeleted = 1 WHERE [EntryID] = @entryID

END
GO
/****** Object:  StoredProcedure [dbo].[DeleteUser]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteUser]
	@userId int
AS
BEGIN
	
	DELETE FROM [Users] WHERE [UserID] = @userId

END
GO
/****** Object:  StoredProcedure [dbo].[EditUser]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EditUser]
	@userId int, @login varchar(100), @passwordHash nvarchar(200), @roleID int
AS
BEGIN
	
	IF NOT EXISTS (SELECT [UserID] FROM [dbo].[Users] WHERE [Login] = @login)
	BEGIN

		UPDATE [Users] SET [Login] = @login, [PasswordHash] = @passwordHash
		WHERE [UserID] = @userId

		IF NOT EXISTS (SELECT [UserID] FROM [dbo].[UsersAndRoles] WHERE [UserID] = @userID AND [RoleID] = @roleID)
		BEGIN

			UPDATE [UsersAndRoles] SET [RoleID] = @roleID WHERE [UserID] = @userId

		END

		RETURN 1

	END

	RETURN 0

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllAuthors]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 10/31/2020
-- Description:	Getting all authors from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetAllAuthors] 
AS
BEGIN
	
	SELECT [AuthorID]
      ,[FirstName]
      ,[LastName]      
	FROM [dbo].[Authors]

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllBooks]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Getting all books from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetAllBooks] 	
AS
BEGIN

	SELECT * FROM GetAllBooksFn()
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllCities]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/11/2020
-- Description:	Getting all cities from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetAllCities]
AS
BEGIN

	SELECT [CityID]
      ,[CountryID]
      ,[Name]
    FROM [dbo].[Cities]
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllCountries]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/12/2020
-- Description:	Getting all countries from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetAllCountries] 
AS
BEGIN

	SELECT [CountryID]
    ,[Name]
	FROM [dbo].[Countries]

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllNewspapers]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Getting all newspapers from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetAllNewspapers] 	
AS
BEGIN

	SELECT * FROM GetAllNewspapersFn()
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllPatents]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/06/2020
-- Description:	Getting all patents from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPatents] 	
AS
BEGIN

	SELECT * FROM GetAllPatentsFn()
	
END
GO
/****** Object:  StoredProcedure [dbo].[GetAllUsers]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 18.11.2020
-- Description:	Getting all users
-- =============================================
CREATE PROCEDURE [dbo].[GetAllUsers]
AS
BEGIN

	SELECT users.[Id], [UserName], [PasswordHash], roles.[Name] FROM [dbo].[AspNetUsers] users
	INNER JOIN [dbo].[AspNetUserRoles] usersRoles
	ON users.[Id] = usersRoles.[UserId]
	INNER JOIN [dbo].[AspNetRoles] roles
	ON usersRoles.[RoleId] = roles.[Id]	
	
	--SELECT users.[UserID], [Login], [PasswordHash], [Role] FROM [dbo].[Users] users
	--INNER JOIN [dbo].[UsersAndRoles] usersRoles
	--ON users.[UserID] = usersRoles.[UserID]
	--INNER JOIN [dbo].[Roles] roles
	--ON usersRoles.[RoleID] = roles.[RoleID]

END
GO
/****** Object:  StoredProcedure [dbo].[GetAllUsersFromSqlTable]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllUsersFromSqlTable]
AS
BEGIN	
	SELECT users.[UserID], [Login], [PasswordHash], [Role] FROM [dbo].[Users] users
	INNER JOIN [dbo].[UsersAndRoles] usersRoles
	ON users.[UserID] = usersRoles.[UserID]
	INNER JOIN [dbo].[Roles] roles
	ON usersRoles.[RoleID] = roles.[RoleID]
END
GO
/****** Object:  StoredProcedure [dbo].[GetAuthorById]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/13/2020
-- Description:	Getting author by id from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetAuthorById]	 
	@authorId int
AS
BEGIN

	SELECT [AuthorID]
      ,[FirstName]
      ,[LastName]     
	FROM [dbo].[Authors]
	WHERE [AuthorID] = @authorId

END
GO
/****** Object:  StoredProcedure [dbo].[GetBookById]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/12/2020
-- Description:	Getting book by id from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetBookById]
	@id int
AS
BEGIN
	
	SELECT [EntryID]
      ,[Name]
      ,[PublicationPlace]
      ,[PublishingHouse]
      ,[PublicationYear]
      ,[NumberOfPages]
      ,[Notes]
      ,[ISBN]
      ,[Hash]
	FROM [dbo].[Books]
	WHERE [EntryID] = @id AND IsDeleted = 0      

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByAuthor]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Getting all books by author name from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetBooksByAuthor]
	@firstName nvarchar(50), @lastName nvarchar(200)
AS
BEGIN

	DECLARE @authorId int
	SELECT @authorId = [AuthorID] FROM [dbo].[Authors] WHERE [FirstName] = @firstName AND [LastName] = @lastName

	SELECT * FROM GetAllBooksFn() WHERE [AuthorID] = @authorId	

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByName]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Getting books by name from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetBooksByName]
	@nameForSearching nvarchar(300)
AS
BEGIN

	SELECT * FROM GetAllBooksFn()
	WHERE [Name] LIKE @nameForSearching + '%'	

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksByPublishingHouse]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Getting all books by publishing house from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetBooksByPublishingHouse]
	@publishingHouseForSearching nvarchar(200)
AS
BEGIN
	
	SELECT * FROM GetAllBooksFn()
	WHERE [PublishingHouse] LIKE @publishingHouseForSearching + '%'

END
GO
/****** Object:  StoredProcedure [dbo].[GetBooksForPage]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBooksForPage]
	@page int,
	@pageSize int,
	@name NVARCHAR(MAX),
	@sort NVARCHAR(4),
	@author NVARCHAR(MAX)
AS	
BEGIN
	DECLARE @booksId ListOfId

	IF @author = ''
	BEGIN
		INSERT INTO @booksId (Id) SELECT EntryID FROM Books WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%'
	END
	ELSE
	BEGIN
		INSERT INTO @booksId (Id) SELECT b.EntryID FROM (SELECT EntryID FROM Books WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%') b 
		JOIN BooksAndAuthors ab ON ab.Entry_ID = b.EntryID
		JOIN Authors a ON a.AuthorID = ab.Author_ID AND (a.FirstName + ' ' + a.LastName LIKE '%' + @author + '%' OR a.LastName + ' ' + a.FirstName LIKE '%' + @author + '%')		
	END

		SELECT o.EntryID, o.Name, o.PublicationYear, o.NumberOfPages, o.ISBN, o.PublicationPlace, o.PublishingHouse, o.Notes, a.FirstName as FirstName, a.LastName as LastName FROM
		(
				SELECT [EntryID], [Name], [ISBN], [PublicationYear], [NumberOfPages], [PublicationPlace], [PublishingHouse], [Notes] FROM [Books] WHERE EXISTS (SELECT Id FROM @booksId WHERE Id = [EntryID])
				ORDER BY
					CASE WHEN @sort = 'DESC'  
								THEN PublicationYear  
					END DESC,  
					CASE WHEN @sort = 'ASC'  
								THEN PublicationYear  
					END ASC
					OFFSET (@page - 1) * @pageSize ROWS  
					FETCH NEXT @pageSize ROWS ONLY
		) o
		LEFT JOIN BooksAndAuthors ab ON ab.Entry_ID = o.EntryID
		LEFT JOIN Authors a ON ab.Author_ID = a.AuthorID

		SELECT COUNT(*) AS TotalCount FROM 
		(
			SELECT [EntryID] FROM [Books] WHERE EXISTS (SELECT Id FROM @booksId WHERE Id = [EntryID])
		) o
END
GO
/****** Object:  StoredProcedure [dbo].[GetCatalogForPage]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCatalogForPage]
	@page int,
	@pageSize int,
	@name NVARCHAR(MAX),
	@sort NVARCHAR(4),
	@author NVARCHAR(MAX)
AS	
BEGIN
	DECLARE @booksId ListOfId, @newspapersId ListOfId, @patentsId ListOfId

	IF @author = ''
	BEGIN
		INSERT INTO @booksId (Id) SELECT EntryID FROM Books WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%'	
		INSERT INTO @newspapersId (Id) SELECT EntryID FROM Newspapers WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%'
		INSERT INTO @patentsId (Id) SELECT EntryID FROM Patents WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%'
	END
	ELSE
	BEGIN
		INSERT INTO @booksId (Id) SELECT b.EntryID FROM (SELECT EntryID FROM Books WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%') b 
		JOIN BooksAndAuthors ab ON ab.Entry_ID = b.EntryID
		JOIN Authors a ON a.AuthorID = ab.Author_ID AND (a.FirstName + ' ' + a.LastName LIKE '%' + @author + '%' OR a.LastName + ' ' + a.FirstName LIKE '%' + @author + '%')
		
		INSERT INTO @patentsId (Id) SELECT p.EntryID FROM (SELECT EntryID FROM Patents WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%') p 
		JOIN PatentsAndInventors ip ON ip.Entry_ID = p.EntryID
		JOIN Authors a ON a.AuthorID = ip.Author_ID AND (a.FirstName + ' ' + a.LastName LIKE '%' + @author + '%' OR a.LastName + ' ' + a.FirstName LIKE '%' + @author + '%')
	END
		; WITH Tree AS 
		(
				SELECT 'Book' as ObjectType, [EntryID], [EntryID] as BookID, [Name], [ISBN] as Identifier, [PublicationYear] as Year, [NumberOfPages], null as IssueNumber, null as PublishingDate FROM [Books] WHERE EXISTS (SELECT Id FROM @booksId WHERE Id = [EntryID])
				UNION ALL SELECT 'Newspaper' as ObjectType, [EntryID], null as BookID, [Name], [ISSN] as Identifier, [PublicationYear] as Year, [NumberOfPages], [IssueNumber], null as PublishingDate FROM [Newspapers] WHERE EXISTS (SELECT Id FROM @newspapersId WHERE Id = [EntryID]) 
				UNION ALL SELECT 'Patent' as ObjectType, [EntryID], null as BookID, [Name], [RegistrationNumber] as Identifier, YEAR([PublishingDate]) as Year, [NumberOfPages], null as IssueNumber, [PublishingDate] FROM [Patents] WHERE EXISTS (SELECT Id FROM @patentsId WHERE Id = [EntryID])
		)

		SELECT o.ObjectType, o.EntryID, o.Name, o.Year, o.NumberOfPages, o.IssueNumber, o.PublishingDate, o.Identifier, a.FirstName as AuthorName, a.LastName as AuthorSurname FROM
		(
				SELECT ObjectType, [EntryID], BookID, [Name], Identifier, Year, [NumberOfPages], IssueNumber, PublishingDate FROM Tree
				ORDER BY
					CASE WHEN @sort = 'DESC'  
								THEN Year  
					END DESC,  
					CASE WHEN @sort = 'ASC'  
								THEN Year  
					END ASC
					OFFSET (@page - 1) * @pageSize ROWS  
					FETCH NEXT @pageSize ROWS ONLY
		) o
		LEFT JOIN BooksAndAuthors ab ON ab.Entry_ID = o.BookID
		LEFT JOIN Authors a ON ab.Author_ID = a.AuthorID

		SELECT COUNT(*) AS TotalCount FROM 
		(
			SELECT 1 as ObjectType, [EntryID], [EntryID] as BookID, [Name], [ISBN] as Identifier, [PublicationYear] as Year, [NumberOfPages], null as IssueNumber, null as PublishingDate FROM [Books] WHERE EXISTS (SELECT Id FROM @booksId WHERE Id = [EntryID])
				UNION ALL SELECT 2 as ObjectType, [EntryID], null as BookID, [Name], [ISSN] as Identifier, [PublicationYear] as Year, [NumberOfPages], [IssueNumber], null as PublishingDate FROM [Newspapers] WHERE EXISTS (SELECT Id FROM @newspapersId WHERE Id = [EntryID]) 
				UNION ALL SELECT 3 as ObjectType, [EntryID], null as BookID, [Name], [RegistrationNumber] as Identifier, YEAR([PublishingDate]) as Year, [NumberOfPages], null as IssueNumber, [PublishingDate] FROM [Patents] WHERE EXISTS (SELECT Id FROM @patentsId WHERE Id = [EntryID])
		) o
END
GO
/****** Object:  StoredProcedure [dbo].[GetCountOfActiveAndDeletedEntities]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCountOfActiveAndDeletedEntities]
AS
BEGIN
	DECLARE @countOfActiveBooks int, @countOfActiveNewspapers int, @countOfActivePatents int,
	@countOfDeletedBooks int, @countOfDeletedNewspapers int, @countOfDeletedPatents int

	SET @countOfActiveBooks = (SELECT COUNT(*) FROM Books WHERE IsDeleted = 0)
	SET @countOfActiveNewspapers = (SELECT COUNT(*) FROM Newspapers WHERE IsDeleted = 0)
	SET @countOfActivePatents = (SELECT COUNT(*) FROM Patents WHERE IsDeleted = 0)
	SET @countOfDeletedBooks = (SELECT COUNT(*) FROM Books WHERE IsDeleted = 1)
	SET @countOfDeletedNewspapers = (SELECT COUNT(*) FROM Newspapers WHERE IsDeleted = 1)
	SET @countOfDeletedPatents = (SELECT COUNT(*) FROM Patents WHERE IsDeleted = 1)

	SELECT @countOfActiveBooks as CountOfActiveBooks, @countOfActiveNewspapers as CountOfActiveNewspapers, @countOfActivePatents as CountOfActivePatents,
		@countOfDeletedBooks as CountOfDeletedBooks, @countOfDeletedNewspapers as CountOfDeletedNewspapers, @countOfDeletedPatents as CountOfDeletedPatents,
		@countOfActiveBooks + @countOfActiveNewspapers + @countOfActivePatents as CountOfActiveEntities,
		@countOfDeletedBooks + @countOfDeletedNewspapers + @countOfDeletedPatents as CountOfDeletedEntities
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspaperById]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/12/2020
-- Description:	Getting newspaper by id from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetNewspaperById]
	@id int
AS
BEGIN
	
	SELECT [EntryID]
      ,[Name]
      ,[PublicationPlace]
      ,[PublishingHouse]
      ,[PublicationYear]
      ,[NumberOfPages]
      ,[Notes]
      ,[ISSN]
      ,[IssueNumber]
      ,[IssueDate]
      ,[Hash]
	FROM [dbo].[Newspapers]
	WHERE [EntryID] = @id AND IsDeleted = 0

END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersByName]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Getting newspapers by name from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetNewspapersByName]
	@nameForSearching nvarchar(300)
AS
BEGIN

	SELECT * FROM GetAllNewspapersFn()
	WHERE [Name] LIKE @nameForSearching + '%'	

END
GO
/****** Object:  StoredProcedure [dbo].[GetNewspapersForPage]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetNewspapersForPage]
	@page int,
	@pageSize int,
	@name NVARCHAR(MAX),
	@sort NVARCHAR(4)
AS	
BEGIN

		SELECT [EntryID], [Name], [ISSN], [PublicationYear], [NumberOfPages], [IssueNumber], [IssueDate], [Notes], [PublicationPlace], [PublishingHouse] FROM [Newspapers] WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%'
				ORDER BY
					CASE WHEN @sort = 'DESC'  
								THEN PublicationYear  
					END DESC,  
					CASE WHEN @sort = 'ASC'  
								THEN PublicationYear  
					END ASC
					OFFSET (@page - 1) * @pageSize ROWS  
					FETCH NEXT @pageSize ROWS ONLY

		SELECT COUNT(*) AS TotalCount FROM 
		(
			SELECT [EntryID] FROM [Newspapers] WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%' 				
		) o
END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentById]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/12/2020
-- Description:	Getting patent by id from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetPatentById]
	@id int
AS
BEGIN
	
	SELECT [EntryID]
      ,[Name]
      ,[Notes]
      ,[NumberOfPages]
      ,[Country]
      ,[RegistrationNumber]
      ,[DateOfApplication]
      ,[PublishingDate]
      ,[Hash]
	FROM [dbo].[Patents]
	WHERE [EntryID] = @id AND IsDeleted = 0

END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByInventor]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Getting all books by inventor name from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetPatentsByInventor]
	@firstName nvarchar(50), @lastName nvarchar(200)
AS
BEGIN

	DECLARE @authorId int
	SELECT @authorId = [AuthorID] FROM [dbo].[Authors] WHERE [FirstName] = @firstName AND [LastName] = @lastName
	
	SELECT * FROM GetAllPatentsFn() WHERE [AuthorID] = @authorId

END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsByName]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan S.
-- Create date: 11/01/2020
-- Description:	Getting patents by name from catalog
-- =============================================
CREATE PROCEDURE [dbo].[GetPatentsByName]
	@nameForSearching nvarchar(300)
AS
BEGIN

	SELECT * FROM GetAllPatentsFn()
	WHERE [Name] LIKE @nameForSearching + '%'	

END
GO
/****** Object:  StoredProcedure [dbo].[GetPatentsForPage]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPatentsForPage]
	@page int,
	@pageSize int,
	@name NVARCHAR(MAX),
	@sort NVARCHAR(4),
	@author NVARCHAR(MAX)
AS	
BEGIN
	DECLARE @patentsId ListOfId

	IF @author = ''
	BEGIN
		INSERT INTO @patentsId (Id) SELECT EntryID FROM Patents WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%'
	END
	ELSE
	BEGIN
		INSERT INTO @patentsId (Id) SELECT p.EntryID FROM (SELECT EntryID FROM Patents WHERE IsDeleted = 0 AND Name LIKE '%' + @name + '%') p 
		JOIN PatentsAndInventors ip ON ip.Entry_ID = p.EntryID
		JOIN Authors a ON a.AuthorID = ip.Author_ID AND (a.FirstName + ' ' + a.LastName LIKE '%' + @author + '%' OR a.LastName + ' ' + a.FirstName LIKE '%' + @author + '%')
	END

		SELECT o.EntryID, o.Name, o.NumberOfPages, o.PublishingDate, o.RegistrationNumber, o.Country, o.DateOfApplication, o.Notes, a.FirstName as FirstName, a.LastName as LastName FROM
		(
				SELECT [EntryID], [Name], [RegistrationNumber], [NumberOfPages], [Country], [DateOfApplication], [PublishingDate], [Notes] FROM [Patents] WHERE EXISTS (SELECT Id FROM @patentsId WHERE Id = [EntryID])
				ORDER BY
					CASE WHEN @sort = 'DESC'  
								THEN PublishingDate  
					END DESC,  
					CASE WHEN @sort = 'ASC'  
								THEN PublishingDate  
					END ASC
					OFFSET (@page - 1) * @pageSize ROWS  
					FETCH NEXT @pageSize ROWS ONLY
		) o
		LEFT JOIN PatentsAndInventors ab ON ab.Entry_ID = o.EntryID
		LEFT JOIN Authors a ON ab.Author_ID = a.AuthorID

		SELECT COUNT(*) AS TotalCount FROM 
		(
			SELECT [EntryID] FROM [Patents] WHERE EXISTS (SELECT Id FROM @patentsId WHERE Id = [EntryID])
		) o
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserById]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserById]
	@id int
AS
BEGIN	
	SELECT users.[UserID], [Login], [PasswordHash], [Role] 
	FROM (SELECT [UserID], [Login], [PasswordHash] FROM [dbo].[Users] WHERE [UserID] = @id) users
	INNER JOIN [dbo].[UsersAndRoles] usersRoles
	ON users.[UserID] = usersRoles.[UserID]
	INNER JOIN [dbo].[Roles] roles
	ON usersRoles.[RoleID] = roles.[RoleID]
END
GO
/****** Object:  StoredProcedure [dbo].[SetUserRole]    Script Date: 12/6/2020 8:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan S.
-- Create date: 18.11.2020
-- Description:	Set role for user
-- =============================================
CREATE PROCEDURE [dbo].[SetUserRole]
	@userID int, @roleID int
AS
BEGIN

	DECLARE @userAndRoles TABLE
	(
		[UserID] int,
		[RoleID] int
	)

	INSERT INTO @userAndRoles 
		([UserID], [RoleID])
	VALUES
		(@userID, @roleID)

	MERGE [dbo].[UsersAndRoles] targetTable 
		USING @userAndRoles sourseTable
	ON (targetTable.[UserID] = sourseTable.[UserID])	
	WHEN MATCHED THEN UPDATE
		SET targetTable.[RoleID] = sourseTable.[RoleID]
	WHEN NOT MATCHED BY TARGET 
		THEN INSERT ([UserID], [RoleID])
        VALUES (sourseTable.[UserID], sourseTable.[RoleID])
	WHEN NOT MATCHED BY SOURCE AND targetTable.[UserID] = @userID
		THEN DELETE;

END
GO
USE [master]
GO
ALTER DATABASE [LibraryDB] SET  READ_WRITE 
GO
