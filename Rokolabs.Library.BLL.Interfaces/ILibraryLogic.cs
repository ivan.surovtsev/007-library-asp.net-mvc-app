﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface ILibraryLogic
    {
        HashSet<LibraryEntry> GetAllLibraryEntries();

        HashSet<LibraryEntry> GetLibraryEntriesByName(string name);

        Dictionary<string, HashSet<LibraryEntry>> GetGroupedEntriesByPublishingHouse(HashSet<LibraryEntry> entries);

        HashSet<IGrouping<short?, LibraryEntry>> GetCatalogForGroupingByYear();

        HashSet<LibraryEntry> GetSortedEntriesByYear(HashSet<LibraryEntry> entries, bool ascending = true);

        HashSet<LibraryEntry> GetBooksAndPatentsByAuthor(string firstName, string lastName);

        HashSet<LibraryEntry> GetLibraryEntriesByPublishingHouse(string publishingHouse);

        Page<LibraryEntry> GetCatalogForPage(Page<LibraryEntry> page);

        Statistics GetCountOfActiveAndDeletedEntities();

        string GetCountOfActiveAndDeletedEntitiesForCsv();

        bool TryAddEntities(CommonEntity entities, out CommonEntity addedEntities);
    }
}
