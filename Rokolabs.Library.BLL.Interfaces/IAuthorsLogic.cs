﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface IAuthorsLogic
    {
        void AddAuthor(Author author);

        HashSet<Author> GetAllAuthors();

        Author GetAuthorById(int id);
    }
}
