﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface IPatentsLogic
    {
        bool AddPatent(Patent patent);

        bool DeletePatent(Patent patent);

        bool DeletePatentById(int id);

        Patent GetPatentById(int id);

        HashSet<Patent> GetAllPatents();

        Page<Patent> GetPatentsForPage(Page<Patent> page);

        HashSet<Patent> GetPatentsByName(string name);

        HashSet<Patent> GetPatentsByInventor(string firstName, string lastName);
    }
}
