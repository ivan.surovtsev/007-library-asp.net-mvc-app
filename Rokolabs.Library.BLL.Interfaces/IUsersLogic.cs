﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface IUsersLogic
    {
        bool AddUser(User user);

        bool EditUser(User user);

        bool DeleteUserById(int id);

        List<User> GetAllUsers();

        List<User> GetAllUsersFromSqlTable();

        User GetUserById(int id);

        bool SetUserRole(int userId, UserRoles role);
    }
}
