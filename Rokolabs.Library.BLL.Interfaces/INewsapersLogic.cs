﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface INewsapersLogic
    {
        bool AddNewspaper(Newspaper newspaper, bool isIssue);

        bool DeleteNewspaper(Newspaper newspaper);

        bool DeleteNewspaperById(int id);

        Newspaper GetNewspaperById(int id);

        HashSet<Newspaper> GetAllNewspapers();

        Page<Newspaper> GetNewspapersForPage(Page<Newspaper> page);

        HashSet<Newspaper> GetNewspapersByName(string name);
    }
}
