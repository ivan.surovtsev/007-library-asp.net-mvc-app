﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface ICountriesLogic
    {
        void AddCountry(Country country);

        HashSet<Country> GetAllCountries();
    }
}
