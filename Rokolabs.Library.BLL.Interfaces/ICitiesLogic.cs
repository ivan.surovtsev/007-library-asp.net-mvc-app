﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface ICitiesLogic
    {
        void AddCity(City city);

        HashSet<City> GetAllCities();
    }
}
