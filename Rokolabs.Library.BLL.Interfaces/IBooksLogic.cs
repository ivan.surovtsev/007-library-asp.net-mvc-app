﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.BLL.Interfaces
{
    public interface IBooksLogic
    {
        bool AddBook(Book book);

        bool DeleteBook(Book book);

        bool DeleteBookById(int id);

        Book GetBookById(int id);

        HashSet<Book> GetAllBooks();

        Page<Book> GetBooksForPage(Page<Book> page);

        HashSet<Book> GetBooksByAuthor(string firstName, string lastName);

        HashSet<Book> GetBooksByPublishingHouse(string publishingHouseName);

        HashSet<IGrouping<string, Book>> GetBooksGroupedByPublishingHouse(string publishingHouseName);

        HashSet<Book> GetBooksByName(string name);
    }
}
