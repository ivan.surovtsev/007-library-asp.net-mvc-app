﻿using System;

namespace Rokolabs.Library.Exceptions
{
    public class LibraryDALException : Exception
    {
        public LibraryDALException(string message)
            : base(message)
        {
        }

        public LibraryDALException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
