﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Logic
{
    public class UsersLogic : IUsersLogic
    {
        private IUsersDao usersDao;

        public UsersLogic(IUsersDao usersDao)
        {
            this.usersDao = usersDao;
        }

        public bool AddUser(User user)
        {
            return usersDao.AddUser(user);                     
        }

        public bool EditUser(User user)
        {
            return usersDao.EditUser(user);
        }

        public bool DeleteUserById(int id)
        {
            return usersDao.DeleteUserById(id);
        }

        public List<User> GetAllUsers()
        {
            return usersDao.GetAllUsers();            
        }

        public List<User> GetAllUsersFromSqlTable()
        {
            return usersDao.GetAllUsersFromSqlTable();
        }

        public User GetUserById (int id)
        {
            return usersDao.GetUserById(id);
        }

        public bool SetUserRole(int userId, UserRoles role)
        {
            return usersDao.SetUserRole(userId, role);           
        }
    }
}
