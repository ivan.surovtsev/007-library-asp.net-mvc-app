﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Rokolabs.Library.Tools;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.CommonObjects;
using System.Text;

namespace Rokolabs.Library.BLL.Logic
{
    public class LibraryLogic : ILibraryLogic
    {        
        private IBooksLogic booksLogic;
        private INewsapersLogic newspapersLogic;
        private IPatentsLogic patentsLogic;
        private ILibraryDao dao;

        public LibraryLogic(IBooksLogic booksLogic, INewsapersLogic newspapersLogic, IPatentsLogic patentsLogic, ILibraryDao dao)
        {
            this.booksLogic = booksLogic;
            this.newspapersLogic = newspapersLogic;
            this.patentsLogic = patentsLogic;
            this.dao = dao;
        } 

        public HashSet<LibraryEntry> GetAllLibraryEntries()
        {
            var books = booksLogic.GetAllBooks();
            var newspapers = newspapersLogic.GetAllNewspapers();
            var patents = patentsLogic.GetAllPatents();

            HashSet<LibraryEntry> entries = new HashSet<LibraryEntry>();

            entries = AddLibraryEntriesInCommonCollection(entries, books);
            entries = AddLibraryEntriesInCommonCollection(entries, newspapers);
            entries = AddLibraryEntriesInCommonCollection(entries, patents);

            return entries;
        }
        
        public Page<LibraryEntry> GetCatalogForPage(Page<LibraryEntry> page)
        {
            if (String.IsNullOrEmpty(page.Sort))
            {
                page.Sort = "DESC";
            }
            if (page.NameOfObject == null)
            {
                page.NameOfObject = "";
            }
            if (page.PageNumber == 0)
            {
                page.PageNumber = 1;
            }
            if (page.Author == null)
            {
                page.Author = "";
            }
            page.PageSize = Constants.pageSize;
            return dao.GetCatalogForPage(page);
        }

        public HashSet<LibraryEntry> GetLibraryEntriesByName(string name)
        {
            var books = booksLogic.GetBooksByName(name);
            var newspapers = newspapersLogic.GetNewspapersByName(name);
            var patents = patentsLogic.GetPatentsByName(name);

            HashSet<LibraryEntry> entries = new HashSet<LibraryEntry>();

            entries = AddLibraryEntriesInCommonCollection(entries, books);
            entries = AddLibraryEntriesInCommonCollection(entries, newspapers);
            entries = AddLibraryEntriesInCommonCollection(entries, patents);

            return entries;
        }        

        public Dictionary<string, HashSet<LibraryEntry>> GetGroupedEntriesByPublishingHouse(HashSet<LibraryEntry> entries)
        {
            if (entries == null)
            {
                throw new ArgumentNullException("Collection \"entries\" was empty");
            }

            try
            {
                var groups = from entry in entries
                             group entry by entry.PublishingHouse;

                return GetGroupedData(groups);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "LibraryLogic", "GetGroupedEntriesByPublishingHouse", ex.Message);              
                throw new Exception("Unexpected error was occured");
            }
        }

        public HashSet<IGrouping<short?, LibraryEntry>> GetCatalogForGroupingByYear()
        {
            try
            {
                HashSet<LibraryEntry> catalog = GetAllLibraryEntries();
                return catalog.GroupBy(c => c.PublicationYear).ToHashSet();
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "LibraryLogic", "GetCatalogForGroupingByYear", ex.Message);
                throw new Exception("Unexpected error was occured");
            }
        }

        public HashSet<LibraryEntry> GetSortedEntriesByYear(HashSet<LibraryEntry> entries, bool ascending = true)
        {
            if (entries == null)
            {
                throw new ArgumentNullException("Collection \"entries\" was empty");
            }

            HashSet<LibraryEntry> result = new HashSet<LibraryEntry>();

            IOrderedEnumerable<LibraryEntry> query = null;

            if (ascending)
            {
                query = from entry in entries
                        orderby entry                      
                        select entry;
            }
            else
            {
                query = from entry in entries
                        orderby entry descending
                        select entry;
            }

            foreach (var item in query)
            {
                result.Add(item);
            }

            return result;
        }        

        public HashSet<LibraryEntry> GetBooksAndPatentsByAuthor(string firstName, string lastName)
        {
            var books = booksLogic.GetBooksByAuthor(firstName, lastName);
            var patents = patentsLogic.GetPatentsByInventor(firstName, lastName);

            HashSet<LibraryEntry> entries = new HashSet<LibraryEntry>();

            entries = AddLibraryEntriesInCommonCollection(entries, books);
            entries = AddLibraryEntriesInCommonCollection(entries, patents);

            return entries;
        }

        public HashSet<LibraryEntry> GetLibraryEntriesByPublishingHouse(string publishingHouse)
        {
            var entries = GetAllLibraryEntries();

            var query = from entry in entries
                        where entry.PublishingHouse?.Contains(publishingHouse) ?? false
                        select entry;

            return query.ToHashSet();
        }

        public Statistics GetCountOfActiveAndDeletedEntities()
        {
            return dao.GetCountOfActiveAndDeletedEntities();
        }

        public string GetCountOfActiveAndDeletedEntitiesForCsv()
        {
            Statistics statistics = GetCountOfActiveAndDeletedEntities();
            var builder = new StringBuilder();

            builder.AppendLine($"\"CountOfActiveBooks\",\"{statistics.CountOfActiveBooks}\"");
            builder.AppendLine($"\"CountOfActiveNewspapers\",\"{statistics.CountOfActiveNewspapers}\"");
            builder.AppendLine($"\"CountOfActivePatents\",\"{statistics.CountOfActivePatents}\"");
            builder.AppendLine($"\"CountOfDeletedBooks\",\"{statistics.CountOfDeletedBooks}\"");
            builder.AppendLine($"\"CountOfDeletedNewspapers\",\"{statistics.CountOfDeletedNewspapers}\"");
            builder.AppendLine($"\"CountOfDeletedPatents\",\"{statistics.CountOfDeletedPatents}\"");
            builder.AppendLine($"\"CountOfActiveEntities\",\"{statistics.CountOfActiveEntities}\"");
            builder.AppendLine($"\"CountOfDeletedEntities\",\"{statistics.CountOfDeletedEntities}\"");

            return builder.ToString();
        }

        public bool TryAddEntities(CommonEntity entities, out CommonEntity addedEntities)
        {
            addedEntities = new CommonEntity();

            foreach (var book in entities.Books)
            {
                if (booksLogic.AddBook(book))
                {
                    addedEntities.Books.Add(book);
                }
                else
                {
                    return false;
                }
            }

            foreach (var newspaper in entities.Newspapers)
            {
                if (newspapersLogic.AddNewspaper(newspaper, false))
                {
                    addedEntities.Newspapers.Add(newspaper);
                }
                else
                {
                    return false;
                }
            }

            foreach (var patent in entities.Patents)
            {
                if (patentsLogic.AddPatent(patent))
                {
                    addedEntities.Patents.Add(patent);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        private Dictionary<T, HashSet<LibraryEntry>> GetGroupedData<T>(IEnumerable<IGrouping<T, LibraryEntry>> groups)
        {
            Dictionary<T, HashSet<LibraryEntry>> groupedData = new Dictionary<T, HashSet<LibraryEntry>>();

            foreach (IGrouping<T, LibraryEntry> group in groups)
            {
                foreach (var item in group)
                {
                    if (group.Key == null)
                    {
                        continue;
                    }

                    if (groupedData.ContainsKey(group.Key))
                    {
                        groupedData[group.Key].Add(item);
                    }
                    else
                    {
                        groupedData.Add(group.Key, new HashSet<LibraryEntry> { item });
                    }
                }
            }

            return groupedData;
        }

        private HashSet<LibraryEntry> AddLibraryEntriesInCommonCollection<T>(HashSet<LibraryEntry> collection, HashSet<T> entries) where T : LibraryEntry
        {
            if (collection == null)
            {
                return null;
            }

            if (entries == null)
            {
                return collection;
            }

            foreach (var entry in entries)
            {
                collection.Add(entry);
            }

            return collection;
        }
    }
}
