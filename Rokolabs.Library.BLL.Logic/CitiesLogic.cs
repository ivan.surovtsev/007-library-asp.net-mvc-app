﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Logic
{
    public class CitiesLogic : ICitiesLogic
    {
        private ICitiesDao citiesDao;

        public CitiesLogic(ICitiesDao citiesDao)
        {
            this.citiesDao = citiesDao;
        }

        public void AddCity(City city)
        {
            citiesDao.AddCity(city);           
        }

        public HashSet<City> GetAllCities()
        {
            return citiesDao.GetAllCities();            
        }
    }
}
