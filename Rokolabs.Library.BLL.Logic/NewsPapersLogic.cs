﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.CommonObjects;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using Rokolabs.Library.Validators;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.BLL.Logic
{
    public class NewspapersLogic : INewsapersLogic
    {
        private INewspapersDao newspapersDao;
        private static ConcurrentDictionary<int, Newspaper> cache = new ConcurrentDictionary<int, Newspaper>();

        public NewspapersLogic(INewspapersDao newsPapersDao)
        {
            this.newspapersDao = newsPapersDao;
        }

        public ILibraryValidator Validator { get; set; }

        public bool AddNewspaper(Newspaper newspaper, bool isIssue)
        {
            Validator?.Check(newspaper);

            if (!(Validator?.IsValid ?? false))
            {
                return false;
            }

            try
            {
                newspaper.Hash = GetNewspaperHash(newspaper);
                newspaper = newspapersDao.AddNewspaper(newspaper, isIssue);

                if (newspaper != null)
                {
                    cache.AddOrUpdate(newspaper.EntryId, newspaper, (entryId, entryForUpdate) => newspaper);

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "NewspapersLogic", "AddNewspaper", ex.Message);
                throw;
            }
        }

        public bool DeleteNewspaper(Newspaper newspaper)
        {
            Validator?.Check(newspaper);

            if (!(Validator?.IsValid ?? false))
            {
                return false;
            }

            try
            {
                if (newspapersDao.DeleteNewspaper(newspaper))
                {
                    cache.TryRemove(newspaper.EntryId, out newspaper);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "NewspapersLogic", "DeleteNewspaper", ex.Message);
                throw;
            }            
        }

        public bool DeleteNewspaperById(int id)
        {
            try
            {
                if (newspapersDao.DeleteNewspaperById(id))
                {                    
                    cache.TryRemove(id, out Newspaper dump);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "NewspapersLogic", "DeleteNewspaperById", ex.Message);
                throw;
            }
        }

        public Newspaper GetNewspaperById(int id)
        {
            try
            {
                Newspaper newspaper = cache.Values.FirstOrDefault(e => e.EntryId == id);

                if (newspaper != null)
                {
                    return newspaper;
                }

                return newspapersDao.GetNewspaperById(id);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "NewspapersLogic", "GetNewspaperById", ex.Message);
                throw;
            }            
        }

        public HashSet<Newspaper> GetAllNewspapers()
        {
            try
            {
                HashSet<Newspaper> retNewspapeps = new HashSet<Newspaper>();

                var newspapers = newspapersDao.GetAllNewspapers();

                if (newspapers != null)
                {
                    cache.Clear();

                    foreach (var newspaper in newspapers)
                    {
                        cache.AddOrUpdate(newspaper.EntryId, newspaper, (entryId, entryForUpdate) => newspaper);
                        retNewspapeps.Add(newspaper);
                    }
                }

                return retNewspapeps;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "NewspapersLogic", "GetAllNewspapers", ex.Message);
                throw;
            }           
        }

        public Page<Newspaper> GetNewspapersForPage(Page<Newspaper> page)
        {
            try
            {
                if (String.IsNullOrEmpty(page.Sort))
                {
                    page.Sort = "DESC";
                }
                if (page.NameOfObject == null)
                {
                    page.NameOfObject = "";
                }
                if (page.PageNumber == 0)
                {
                    page.PageNumber = 1;
                }
                page.PageSize = Constants.pageSize;
                return newspapersDao.GetNewspapersForPage(page);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Error, "BLL", "NewspapersLogic", "GetNewspapersForPage", ex.Message);
                throw ex;
            }
        }

        public HashSet<Newspaper> GetNewspapersByName(string name)
        {
            var errors = LibraryEntryValidatorsRules.CheckName(name);

            if (errors != null && errors.Any())
            {
                throw new ArgumentException("Invalid name");
            }

            try
            {
                var query = from newspaper in cache.Values
                            where newspaper.IssueNumber != null && newspaper.Name.ToLower().Contains(name.ToLower())
                            orderby newspaper.IssueNumber descending
                            select newspaper;

                HashSet<Newspaper> newspapers = new HashSet<Newspaper>();              

                newspapers = query.ToHashSet();

                if (newspapers != null && newspapers.Any())
                {
                    return newspapers;
                }

                return newspapersDao.GetNewspapersByName(name);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "NewspapersLogic", "GetNewspapersByName", ex.Message);
                throw;
            }          
        }

        private int GetNewspaperHash(Newspaper newspaper)
        {
            int hash = 0;

            for (int i = 0; i < newspaper.Name?.Length; i++)
            {
                hash += newspaper.Name[i];
            }

            for (int i = 0; i < newspaper.PublishingHouse?.Length; i++)
            {
                hash += newspaper.PublishingHouse[i];
            }

            string date = newspaper.IssueDate?.ToLongDateString();

            for (int i = 0; i < date?.Length; i++)
            {
                hash += date[i];
            }

            return hash;
        }
    }
}
