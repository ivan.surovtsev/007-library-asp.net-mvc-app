﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Logic
{
    public class AuthorsLogic : IAuthorsLogic
    {
        private IAuthorsDao authorsDao;

        public AuthorsLogic(IAuthorsDao authorsDao)
        {
            this.authorsDao = authorsDao;
        }

        public void AddAuthor(Author author)
        {
            authorsDao.AddAuthor(author);        
        }

        public HashSet<Author> GetAllAuthors()
        {
            return authorsDao.GetAllAuthors();
        }

        public Author GetAuthorById(int id)
        {
            return authorsDao.GetAuthorById(id);            
        }
    }
}
