﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.CommonObjects;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Tools;
using Rokolabs.Library.Validators;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.BLL.Logic
{
    public class PatentsLogic : IPatentsLogic
    {
        private IPatentsDao patentsDao;
        private static ConcurrentDictionary<int, Patent> cache = new ConcurrentDictionary<int, Patent>();

        public PatentsLogic(IPatentsDao patentsDao)
        {
            this.patentsDao = patentsDao;
        }

        public ILibraryValidator Validator { get; set; }

        public bool AddPatent(Patent patent)
        {
            Validator?.Check(patent);

            if (!(Validator?.IsValid ?? false))
            {
                return false;
            }

            try
            {
                patent.Hash = GetPatentHash(patent);
                patent = patentsDao.AddPatent(patent);

                if (patent != null)
                {
                    cache.AddOrUpdate(patent.EntryId, patent, (entryId, entryForUpdate) => patent);

                    return true;
                }

                return false;                
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "PatentsLogic", "AddPatent", ex.Message);
                throw;
            }          
        }

        public bool DeletePatent(Patent patent)
        {
            Validator?.Check(patent);

            if (!(Validator?.IsValid ?? false))
            {
                return false;
            }

            try
            {
                if (patentsDao.DeletePatent(patent))
                {
                    cache.TryRemove(patent.EntryId, out patent);
                    return true;
                }

                return false;               
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "PatentsLogic", "DeletePatent", ex.Message);
                throw;
            }            
        }

        public bool DeletePatentById(int id)
        {
            try
            {
                if (patentsDao.DeletePatentById(id))
                {
                    cache.TryRemove(id, out Patent dump);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "PatentsLogic", "DeletePatentById", ex.Message);
                throw;
            }          
        }

        public Patent GetPatentById(int id)
        {
            try
            {
                Patent patent = cache.Values.FirstOrDefault(e => e.EntryId == id);

                if (patent != null)
                {
                    return patent;
                }

                return patentsDao.GetPatentById(id);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "PatentsLogic", "GetPatentById", ex.Message);
                throw;
            }           
        }

        public HashSet<Patent> GetAllPatents()
        {
            try
            {
                HashSet<Patent> retPatents = new HashSet<Patent>();

                var patents = patentsDao.GetAllPatents();

                if (patents != null)
                {
                    cache.Clear();

                    foreach (var patent in patents)
                    {
                        cache.AddOrUpdate(patent.EntryId, patent, (entryId, entryForUpdate) => patent);
                        retPatents.Add(patent);
                    }
                }

                return retPatents;               
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "PatentsLogic", "GetAllPatents", ex.Message);
                throw;
            }           
        }

        public Page<Patent> GetPatentsForPage(Page<Patent> page)
        {
            try
            {
                if (String.IsNullOrEmpty(page.Sort))
                {
                    page.Sort = "DESC";
                }
                if (page.NameOfObject == null)
                {
                    page.NameOfObject = "";
                }
                if (page.PageNumber == 0)
                {
                    page.PageNumber = 1;
                }
                if (page.Author == null)
                {
                    page.Author = "";
                }
                page.PageSize = Constants.pageSize;
                return patentsDao.GetPatentsForPage(page);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Error, "BLL", "PatentsLogic", "GetPatentsForPage", ex.Message);
                throw ex;
            }
        }

        public HashSet<Patent> GetPatentsByName(string name)
        {
            try
            {
                var query = from patent in cache.Values
                            where patent.Name.ToLower().Contains(name.ToLower())
                            select patent;

                HashSet<Patent> patents = new HashSet<Patent>();

                patents = query.ToHashSet();

                if (patents != null && patents.Any())
                {
                    return patents;
                }

                return patentsDao.GetPatentByName(name);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "PatentsLogic", "GetPatentsByName", ex.Message);
                throw;
            }           
        }

        public HashSet<Patent> GetPatentsByInventor(string firstName, string lastName)
        {
            var firstNameErrors = LibraryEntryValidatorsRules.CheckAuthorFirstName(firstName);

            if (firstNameErrors != null && firstNameErrors.Any())
            {
                throw new ArgumentException("First name is not valid");
            }

            var lastNameErrors = LibraryEntryValidatorsRules.CheckAuthorLastName(lastName);

            if (lastNameErrors != null && lastNameErrors.Any())
            {
                throw new ArgumentException("Last name is not valid");
            }

            try
            {
                HashSet<Patent> patents = new HashSet<Patent>();

                foreach (var patent in cache.Values)
                {
                    foreach (var author in patent.Authors)
                    {
                        if (author.FirstName == firstName && author.LastName == lastName)
                        {
                            patents.Add(patent);
                        }
                    }
                }

                if (patents.Any())
                {
                    return patents;
                }

                return patentsDao.GetPatentsByInventor(firstName, lastName);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "PatentsLogic", "GetPatentsByInventor", ex.Message);
                throw;
            }            
        }

        private int GetPatentHash(Patent patent)
        {
            if (patent == null)
            {
                return -1;
            }

            int hash = patent.RegistrationNumber.HasValue ? patent.RegistrationNumber.Value : 0;

            for (int i = 0; i < patent.Country?.Length; i++)
            {
                hash += patent.Country[i];
            }

            return hash;
        }
    }
}
