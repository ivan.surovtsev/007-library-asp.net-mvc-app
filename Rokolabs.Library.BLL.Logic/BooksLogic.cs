﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using Rokolabs.Library.Validators;
using System.Collections.Generic;
using System;
using Rokolabs.Library.Tools;
using System.Collections.Concurrent;
using System.Linq;
using Rokolabs.Library.CommonObjects;

namespace Rokolabs.Library.BLL.Logic
{
    public class BooksLogic : IBooksLogic
    {
        private IBooksDao booksDao;
        private static ConcurrentDictionary<int, Book> cache = new ConcurrentDictionary<int, Book>();

        public BooksLogic(IBooksDao booksDao)
        {
            this.booksDao = booksDao;
        }

        public ILibraryValidator Validator { get; set; }        

        public bool AddBook(Book book)
        {
            Validator?.Check(book);

            if (!(Validator?.IsValid ?? false))
            {
                return false;
            }

            try
            {  
                book.Hash = GetBookHash(book);
                book = booksDao.AddBook(book);

                if (book != null)
                {    
                    cache.AddOrUpdate(book.EntryId, book, (entryId, bookForUpdate) => book);

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "AddBook", ex.Message);
                throw;
            }         
        }

        public bool DeleteBook(Book book)
        {
            Validator?.Check(book);

            if (!(Validator?.IsValid ?? false))
            {
                return false;
            }

            try
            {
                if (booksDao.DeleteBook(book))
                {
                    cache.TryRemove(book.EntryId, out book);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "DeleteBook", ex.Message);
                throw;
            }            
        }

        public bool DeleteBookById(int id)
        {
            try
            {
                if (booksDao.DeleteBookById(id))
                {
                    Book dump;
                    cache.TryRemove(id, out dump);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "DeleteBookById", ex.Message);
                throw;
            }          
        }

        public Book GetBookById(int id)
        {
            try
            {
                Book book = cache.Values.FirstOrDefault(e => e.EntryId == id);

                if (book != null)
                {
                    return book;
                }

                return booksDao.GetBookById(id);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "GetBookById", ex.Message);
                throw;
            }          
        }

        public HashSet<Book> GetAllBooks()
        {
            try
            {
                HashSet<Book> retBooks = new HashSet<Book>();

                var books = booksDao.GetAllBooks();

                if (books != null)
                {
                    cache.Clear();

                    foreach (var book in books)
                    {
                        cache.AddOrUpdate(book.EntryId, book, (entryId, bookForUpdate) => book);
                        retBooks.Add(book);
                    }
                }

                return retBooks;
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "GetAllBooks", ex.Message);
                throw;
            }            
        }

        public Page<Book> GetBooksForPage(Page<Book> page)
        {
            try
            {
                if (String.IsNullOrEmpty(page.Sort))
                {
                    page.Sort = "DESC";
                }
                if (page.NameOfObject == null)
                {
                    page.NameOfObject = "";
                }
                if (page.PageNumber == 0)
                {
                    page.PageNumber = 1;
                }
                if (page.Author == null)
                {
                    page.Author = "";
                }
                page.PageSize = Constants.pageSize;
                return booksDao.GetBooksForPage(page);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Error, "BLL", "BooksLogic", "GetBooksForPage", ex.Message);
                throw ex;
            }
        }

        public HashSet<Book> GetBooksByAuthor(string firstName, string lastName)
        {
            var firstNameErrors = LibraryEntryValidatorsRules.CheckAuthorFirstName(firstName);

            if (firstNameErrors != null && firstNameErrors.Any())
            {
                throw new ArgumentException("First name is not valid");
            }

            var lastNameErrors = LibraryEntryValidatorsRules.CheckAuthorLastName(lastName);           

            if (lastNameErrors != null && lastNameErrors.Any())
            {
                throw new ArgumentException("Last name is not valid");
            }

            try
            {
                HashSet<Book> books = new HashSet<Book>();

                foreach (var book in cache.Values)
                {
                    foreach (var author in book.Authors)
                    {
                        if (author.FirstName == firstName && author.LastName == lastName)
                        {                   
                            books.Add(book);
                        }
                    }
                }

                if (books.Any())
                {
                    return books;
                }

                return booksDao.GetBooksByAuthor(firstName, lastName);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "GetBooksByAuthor", ex.Message);
                throw;
            }  
        }

        public HashSet<Book> GetBooksByName(string name)
        {
            var errors = LibraryEntryValidatorsRules.CheckName(name);

            if (errors != null && errors.Any())
            {
                throw new ArgumentException("Invalid name");
            }

            try
            {
                HashSet<Book> books = new HashSet<Book>();

                var query = from book in cache.Values
                            where book.Name.ToLower().Contains(name.ToLower())
                            select book;

                books = query.ToHashSet();

                if (books != null && books.Any())
                {
                    return books;
                }

                return booksDao.GetBooksByName(name);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "GetBooksByName", ex.Message);
                throw;
            }            
        }

        public HashSet<Book> GetBooksByPublishingHouse(string publishingHouseName)
        {
            try
            {
                return booksDao.GetBooksByPublishingHouse(publishingHouseName);
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "GetBooksByPublishingHouse", ex.Message);
                throw;
            }            
        }

        public HashSet<IGrouping<string, Book>> GetBooksGroupedByPublishingHouse(string publishingHouseName)
        {
            try
            {
                return GetBooksByPublishingHouse(publishingHouseName).GroupBy(c => c.PublishingHouse).ToHashSet();
            }
            catch (Exception ex)
            {
                Logger.AddEvent(LogEventType.Fatal, "BLL", "BooksLogic", "GetBooksGroupedByPublishingHouse", ex.Message);
                throw;
            }
        }

        private int GetBookHash(Book book)
        {
            int hash = 0;

            for (int i = 0; i < book.Name?.Length; i++)
            {
                hash += book.Name[i];
            }

            if (book.Authors != null)
            {
                foreach (var author in book.Authors)
                {
                    hash += author.GetHashCode();
                }
            }

            hash += book.PublicationYear.HasValue ? book.PublicationYear.Value : 0;

            return hash;
        }
    }
}
