﻿using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.DAL.Interfaces;
using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Logic
{
    public class CountriesLogic : ICountriesLogic
    {
        private ICountriesDao countriesDao;

        public CountriesLogic(ICountriesDao countriesDao)
        {
            this.countriesDao = countriesDao;
        }

        public void AddCountry(Country country)
        {
            countriesDao.AddCountry(country);
        }

        public HashSet<Country> GetAllCountries()
        {
            return countriesDao.GetAllCountries();           
        }
    }
}
