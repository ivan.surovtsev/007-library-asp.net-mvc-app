﻿using NUnit.Framework;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.BLL.Logic;
using Rokolabs.Library.DAL.MsSqlDal;
using Rokolabs.Library.Entities;
using System;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Tests
{
    [TestFixture]
    public class PatentsLogicTests
    {
        private IPatentsLogic patentsLogic;
        private List<Patent> testPatents;
        private Author testAuthor;

        public PatentsLogicTests()
        {
            patentsLogic = new PatentsLogic(new PatentsDao());

            testPatents = new List<Patent>
            {
                new Patent
                {
                    Name = "Test patent 1",
                    PublicationYear = 2018,
                    Authors = new HashSet<Author>()
                },

                new Patent
                {
                    Name = "Test patent 2",
                    PublicationYear = 2018,
                    Authors = new HashSet<Author>()
                }
            };

            testAuthor = new Author
            {
                FirstName = "Anonim",
                LastName = "Anonim"
            };

            testPatents.ForEach(p => p.Authors.Add(testAuthor));
        }

        [Test]
        public void AddPatent()
        {
            patentsLogic.DeletePatent(testPatents[0]);

            bool patentIsAdded = patentsLogic.AddPatent(testPatents[0]);

            Assert.That(patentIsAdded, "Patent wasn't added in libary");

            patentsLogic.DeletePatent(testPatents[0]);
        }


        [Test]
        public void DeletePatent()
        {
            patentsLogic.AddPatent(testPatents[0]);

            bool patentIsDeleted = patentsLogic.DeletePatent(testPatents[0]);

            Assert.That(patentIsDeleted, "Patent wasn't deleted from libary");
        }

        [Test]
        public void AddSamePatent()
        {
            patentsLogic.DeletePatent(testPatents[0]);

            patentsLogic.AddPatent(testPatents[0]);
            bool patentIsNotAdded = !patentsLogic.AddPatent(testPatents[0]);

            Assert.That(patentIsNotAdded, "Patent was added in libary again");

            patentsLogic.DeletePatent(testPatents[0]);
        }

        [Test]
        public void AddEntryWithWrongPublishingDate()
        {
            Patent entry = new Patent { Name = "Name", DateOfApplication = DateTime.Now, PublishingDate = Convert.ToDateTime("11/2/2020") };

            Assert.Throws<ArgumentException>(() => { patentsLogic.AddPatent(entry); }, "Here a \'ArgumentException\' must be");
        }
    }
}
