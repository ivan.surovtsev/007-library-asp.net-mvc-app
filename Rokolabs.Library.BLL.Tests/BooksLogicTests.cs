﻿using NUnit.Framework;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.BLL.Logic;
using Rokolabs.Library.DAL.MsSqlDal;
using Rokolabs.Library.Entities;
using System;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Tests
{
    [TestFixture]
    public class BooksLogicTests
    {
        private IBooksLogic booksLogic;
        private List<Book> testBooks;
        private Author testAuthor;

        public BooksLogicTests()
        {
            booksLogic = new BooksLogic(new BooksDao());

            testBooks = new List<Book>
            {
                new Book
                {
                    Name = "Test book 1",
                    PublishingHouse = "Some house",
                    PublicationYear = 2020,
                    Authors = new HashSet<Author>()
                },

                new Book
                {
                    Name = "Test book 2",
                    PublishingHouse = "Some house",
                    PublicationYear = 2020,
                    Authors = new HashSet<Author>()
                }
            };

            testAuthor = new Author
            {
                FirstName = "Anonim",
                LastName = "Anonim"
            };

            testBooks.ForEach(b => b.Authors.Add(testAuthor));
        }

        //[Test]
        //public void Gen()
        //{
        //    var newspaperLogic = new NewsPapersLogic(new NewsPapersDAO());
        //    var patentLogic = new PatentsLogic(new PatentsDAO());
        //    Random rnd = new Random();
        //    Random rndChar = new Random();
        //    List<LibraryEntry> entries = new List<LibraryEntry>();

        //    for (int i = 0; i < 500; i++)
        //    {
        //        LibraryEntry entry = null;
        //        int num = rnd.Next(1, 5);

        //        switch (num)
        //        {
        //            case 1:
        //            case 2:

        //                entry = new Book
        //                {
        //                    Name = $"Book {i + 100}",
        //                    PublishingHouse = "House",
        //                    PublicationYear = (short)(2015 + num),
        //                    Authors = new HashSet<Author>
        //                    {
        //                        new Author
        //                        {
        //                            FirstName = $"{(char)rndChar.Next(0x41, 0x5A)}{(char)rndChar.Next(0x61, 0x7A)}",
        //                            LastName = $"{(char)rndChar.Next(0x41, 0x50)}{(char)rndChar.Next(0x69, 0x73)}"
        //                        }
        //                    }
        //                };

        //                break;

        //            case 3:
        //            case 4:

        //                entry = new Newspaper
        //                {
        //                    Name = $"Newspaper {i + 100}",
        //                    PublishingHouse = "Newspaper publishing house",
        //                    PublicationYear = (short)(2000 + num)
        //                };

        //                break;

        //            case 5:

        //                entry = new Patent
        //                {
        //                    Name = $"Patent {i + 100}",
        //                    PublicationYear = (short)(2000 + num),
        //                    DateOfApplication = DateTime.Now.Date.AddDays(-2 * num),
        //                    PublishingDate = DateTime.Now.Date.AddDays(-1 * num),
        //                    Authors = new HashSet<Author>
        //                    {
        //                        new Author
        //                        {
        //                            FirstName = $"{(char)rndChar.Next(0x41, 0x5A)}{(char)rndChar.Next(0x61, 0x7A)}",
        //                            LastName = $"{(char)rndChar.Next(0x41, 0x50)}{(char)rndChar.Next(0x69, 0x73)}"
        //                        }
        //                    }
        //                };

        //                break;
        //        }

        //        entries.Add(entry);
        //    }

        //    entries.ForEach(entry =>
        //    {
        //        if (entry is Book)
        //        {
        //            booksLogic.AddBook(entry as Book);
        //        }
        //        else if (entry is Newspaper)
        //        {
        //            newspaperLogic.AddNewspaper(entry as Newspaper);
        //        }
        //        else if (entry is Patent)
        //        {
        //            patentLogic.AddPatent(entry as Patent);
        //        }
        //    });
        //}

        [Test]
        public void AddBook()
        {
            booksLogic.DeleteBook(testBooks[0]);

            bool bookIsAdded = booksLogic.AddBook(testBooks[0]);

            Assert.That(bookIsAdded, "Book wasn't added in libary");

            booksLogic.DeleteBook(testBooks[0]);
        }

        [Test]
        public void DeleteBook()
        {
            booksLogic.AddBook(testBooks[0]);

            bool bookIsDeleted = booksLogic.DeleteBook(testBooks[0]);

            Assert.That(bookIsDeleted, "Book wasn't deleted from libary");
        }

        [Test]
        public void AddSameBook()
        {
            booksLogic.DeleteBook(testBooks[0]);

            booksLogic.AddBook(testBooks[0]);
            bool bookIsNotAdded = !booksLogic.AddBook(testBooks[0]);

            Assert.That(bookIsNotAdded, "Book was added in libary again");

            booksLogic.DeleteBook(testBooks[0]);
        }

        [Test]
        public void AddBooksWithWrongISBN()
        {
            Book book = new Book { Name = "Name", ISBN = "ISBN 9945-123-123-X" };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(book); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void GetBooksByPublishingHouse()
        {
            foreach (var book in testBooks)
            {
                booksLogic.DeleteBook(book);
                booksLogic.AddBook(book);
            }

            int count = booksLogic.GetBooksByPublishingHouse(testBooks[0].PublishingHouse).Count;

            Assert.That(count == testBooks.Count, $"There are must be {testBooks.Count} books in result");

            foreach (var book in testBooks)
            {
                booksLogic.DeleteBook(book);
                booksLogic.DeleteBook(book);
            }
        }

        [Test]
        public void GetBooksByAuthor()
        {
            foreach (var book in testBooks)
            {
                booksLogic.DeleteBook(book);
                booksLogic.AddBook(book);
            }

            int count = booksLogic.GetBooksByAuthor(testAuthor.FirstName, testAuthor.LastName).Count;

            Assert.That(count == testBooks.Count, $"There are must be {testBooks.Count} books in result");

            foreach (var book in testBooks)
            {
                booksLogic.DeleteBook(book);
                booksLogic.DeleteBook(book);
            }
        }
    }
}
