﻿using NUnit.Framework;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.BLL.Logic;
using Rokolabs.Library.DAL.MsSqlDal;
using Rokolabs.Library.Entities;
using System;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Tests
{
    [TestFixture]
    public class LibraryLogicTests
    {
        private ILibraryLogic logic;     
        private Author testAuthor;
        private Book testBook;
        private IBooksLogic booksLogic;

        public LibraryLogicTests()
        {
            booksLogic = new BooksLogic(new BooksDao());

            logic = new LibraryLogic(
                booksLogic,
                new NewspapersLogic(new NewspapersDao()), 
                new PatentsLogic(new PatentsDao()));

            testAuthor = new Author
            {
                FirstName = "Anonim",
                LastName = "Anonim"
            };

            testBook = new Book
            {
                Name = "Test book 1",
                PublishingHouse = "Some house",
                PublicationYear = 2020,
                Authors = new HashSet<Author>
                {
                    testAuthor
                }
            };    
        }     

        [Test]
        public void AddSameAuthor()
        {
            testBook.Authors.Add(testAuthor);
            testBook.Authors.Add(testAuthor);

            bool authorIsNotAdded = testBook.Authors.Count == 1;

            Assert.That(authorIsNotAdded, "The same author was added in authors collection");
        }

        [Test]
        public void AddNullEntry()
        {
            Assert.Throws<ArgumentNullException>(() => { booksLogic.AddBook(null); }, "Here a \'ArgumentNullException\' must be");
        }

        [Test]
        public void AddEntryWithEmptyName()
        {
            Book entry = new Book { Name = string.Empty };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithLongName()
        {
            Book entry = new Book { Name = new string(' ', 301) };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithPublicationPlaceLongName()
        {
            Book entry = new Book { Name = "Name", PublicationPlace = new string(' ', 201) };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithPublicationPlaceBadName()
        {
            Book entry = new Book { Name = "Name", PublicationPlace = "df-" };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithPublishingHouseLongName()
        {
            Book entry = new Book { Name = "Name", PublishingHouse = new string(' ', 301) };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithWrongPublicationYear()
        {
            Book entry = new Book { Name = "Name", PublicationYear = (short?)DateTime.Now.AddYears(1).Year };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithWrongNumberOfPages()
        {
            Book entry = new Book { Name = "Name", NumberOfPages = -1 };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithBigNotes()
        {
            Book entry = new Book { Name = "Name", Notes = new string(' ', 2001) };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }        

        [Test]
        public void AddEntryWithBadAuthorFirstName()
        {
            Book entry = new Book
            { 
                Name = "Name", 

                Authors = new HashSet<Author> 
                { 
                    new Author             
                    { 
                        FirstName = "-A", 
                        LastName = "Name" 
                    } 
                } 
            };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }

        [Test]
        public void AddEntryWithBadAuthorLastName()
        {
            Book entry = new Book
            {
                Name = "test",
                Authors = new HashSet<Author>
                {
                    new Author
                    {
                        FirstName = "Name",
                        LastName = "dd-"
                    }
                }
            };

            Assert.Throws<ArgumentException>(() => { booksLogic.AddBook(entry); }, "Here a \'ArgumentException\' must be");
        }        
    }
}
