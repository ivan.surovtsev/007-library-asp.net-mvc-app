﻿using NUnit.Framework;
using Rokolabs.Library.BLL.Interfaces;
using Rokolabs.Library.BLL.Logic;
using Rokolabs.Library.DAL.MsSqlDal;
using Rokolabs.Library.Entities;
using System;
using System.Collections.Generic;

namespace Rokolabs.Library.BLL.Tests
{
    [TestFixture]
    public class NewsPapersLogicTests
    {
        private INewsapersLogic newsPapersLogic;
        private List<Newspaper> testNewspapers;

        public NewsPapersLogicTests()
        {
            newsPapersLogic = new NewspapersLogic(new NewspapersDao());

            testNewspapers = new List<Newspaper>
            {
                //new Newspaper
                //{
                //    Name = "АиФ",
                //    PublicationPlace = "Москва",
                //    PublishingHouse = "Москва Медиа",
                //    PublicationYear = 2020,
                //    NumberOfPages = 35,
                //    ISSN = "ISSN 8005-4321",
                //    IssueNumber = "127",
                //    IssueDate = new DateTime(2020, 11, 5)
                //},

                new Newspaper
                {
                    Name = "Test newspaper 1",
                    PublishingHouse = "Some house",
                    PublicationYear = 2019
                },

                new Newspaper
                {
                    Name = "Test newspaper 2",
                    PublishingHouse = "Some house",
                    PublicationYear = 2019
                }
            };            
        }

        [Test]
        public void AddNewspaper()
        {
            newsPapersLogic.DeleteNewspaper(testNewspapers[0]);

            bool newspaperIsAdded = newsPapersLogic.AddNewspaper(testNewspapers[0]);

            Assert.That(newspaperIsAdded, "Newspaper wasn't added in libary");

            newsPapersLogic.DeleteNewspaper(testNewspapers[0]);
        }

        [Test]
        public void DeleteNewspaper()
        {
            newsPapersLogic.AddNewspaper(testNewspapers[0]);

            bool newspaperIsDeleted = newsPapersLogic.DeleteNewspaper(testNewspapers[0]);

            Assert.That(newspaperIsDeleted, "Newspaper wasn't deleted from libary");
        }

        [Test]
        public void AddSameNewspaper()
        {
            newsPapersLogic.DeleteNewspaper(testNewspapers[0]);

            newsPapersLogic.AddNewspaper(testNewspapers[0]);
            bool newspaperIsNotAdded = !newsPapersLogic.AddNewspaper(testNewspapers[0]);

            Assert.That(newspaperIsNotAdded, "Newspaper was added in libary again");

            newsPapersLogic.DeleteNewspaper(testNewspapers[0]);
        }

        [Test]
        public void AddEntryWithWrongISSN()
        {
            Newspaper newspaper = new Newspaper { Name = "Name", ISSN = "ISSN 123-5678" };

            Assert.Throws<ArgumentException>(() => { newsPapersLogic.AddNewspaper(newspaper); }, "Here a \'ArgumentException\' must be");
        }
    }
}
