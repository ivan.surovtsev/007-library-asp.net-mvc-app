﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.Validators
{
    public interface ILibraryValidator
    {
        bool IsValid { get; }        

        List<string> this[string propertyName] { get; set; }

        void Check(LibraryEntry entry);
    }
}
