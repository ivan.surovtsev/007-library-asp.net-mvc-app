﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.Validators
{
    public class NewspapersValidator : ILibraryValidator
    {
        private Dictionary<string, List<string>> errors;

        public NewspapersValidator()
        {
            errors = new Dictionary<string, List<string>>();
        }

        public List<string> this[string propertyName]
        {
            get
            {
                if (errors.ContainsKey(propertyName))
                {
                    return errors[propertyName];
                }

                return null;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                if (!errors.ContainsKey(propertyName))
                {
                    errors.Add(propertyName, value);
                }
                else
                {
                    errors[propertyName].AddRange(value);
                }
            }
        }

        public bool IsValid { get => !errors?.Any() ?? false; }

        public void Check(LibraryEntry entry)
        {
            if (entry == null || !(entry is Newspaper))
            {
                return;
            }

            errors.Clear();

            Newspaper newspaper = entry as Newspaper;

            this["Name"] = LibraryEntryValidatorsRules.CheckName(newspaper.Name);
            this["PublicationPlace"] = LibraryEntryValidatorsRules.CheckPublicationPlace(newspaper.PublicationPlace);
            this["PublishingHouse"] = LibraryEntryValidatorsRules.CheckPublishingHouse(newspaper.PublishingHouse);
            this["PublicationYear"] = LibraryEntryValidatorsRules.CheckPublicationYear(newspaper.PublicationYear);
            this["NumberOfPages"] = LibraryEntryValidatorsRules.CheckNumberOfPages(newspaper.NumberOfPages);
            this["Notes"] = LibraryEntryValidatorsRules.CheckNotes(newspaper.Notes);
            this["IssueDate"] = LibraryEntryValidatorsRules.CheckIssueDate(newspaper.IssueDate, newspaper.PublicationYear);
            this["ISSN"] = LibraryEntryValidatorsRules.CheckISSN(newspaper.ISSN);
        }
    }
}
