﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.Validators
{
    public class BooksValidator : ILibraryValidator
    {
        private Dictionary<string, List<string>> errors;  

        public BooksValidator()
        {
            errors = new Dictionary<string, List<string>>();
        }

        public bool IsValid { get => !errors?.Any() ?? false; }

        public List<string> this[string propertyName]
        {
            get
            {
                if (errors.ContainsKey(propertyName))
                {
                    return errors[propertyName];
                }

                return null;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                if (!errors.ContainsKey(propertyName))
                {
                    errors.Add(propertyName, value);
                }
                else
                {
                    errors[propertyName].AddRange(value);
                }
            }
        }

        public void Check(LibraryEntry entry)
        {
            if (entry == null || !(entry is Book))
            {
                return;
            }

            errors.Clear();

            Book book = entry as Book;

            this["Name"] = LibraryEntryValidatorsRules.CheckName(book.Name);
            this["PublicationPlace"] = LibraryEntryValidatorsRules.CheckPublicationPlace(book.PublicationPlace);
            this["PublishingHouse"] = LibraryEntryValidatorsRules.CheckPublishingHouse(book.PublishingHouse);
            this["PublicationYear"] = LibraryEntryValidatorsRules.CheckPublicationYear(book.PublicationYear);
            this["NumberOfPages"] = LibraryEntryValidatorsRules.CheckNumberOfPages(book.NumberOfPages);
            this["Notes"] = LibraryEntryValidatorsRules.CheckNotes(book.Notes);
            this["ISBN"] = LibraryEntryValidatorsRules.CheckISBN(book.ISBN);
            this["Authors"] = LibraryEntryValidatorsRules.CheckAuthors(book.Authors);
        }
    }
}
