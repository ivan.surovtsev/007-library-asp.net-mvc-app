﻿using Rokolabs.Library.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Rokolabs.Library.Validators
{
    public static class LibraryEntryValidatorsRules
    {
        private const int nameMaxLength = 300;
        private const int publicationPlaceMaxLength = 200;
        private const int publishingHouseMaxLength = 300;
        private const int notesMaxLength = 2000;
        private const int isbnLength = 10;
        private const int firstNameMaxLength = 50;
        private const int lastNameMaxLength = 200;
        private const int countryMaxLength = 200;

        private static Regex publicationPlacePattern = new Regex(@"^([A-Z][a-z\s]*(-[A-Z])?[a-z]*(\s[A-Z])?[a-z]*)$|([А-ЯЁ][а-яё\s]*(-[А-ЯЁ])?[а-яё]*(\s[А-ЯЁ])?[а-яё]*)$");
        private static Regex isbnPattern = new Regex(@"^ISBN ([0—7]|((8[0-9])|(9[0-4]))|(9[5-9][0-3])|(99[4-8][0-9])|(999[0-9][0-9]))-([0-9]{1,7})-([0-9]{1,7})-([0-9X])$");
        private static Regex firstNamePattern = new Regex(@"^(([A-Z][a-z]*(-[A-Z])?[a-z]*)$|([А-ЯЁ][а-яё]*(-[А-ЯЁ])?[а-яё]*))$");

        private static Regex lastNameEnPattern = new Regex(@"^[A-Z][a-z]*((-|')([A-Z]))?((\s[a-z]{2,3}\s)([A-Z]))?[a-z]*$");
        private static Regex lastNameRuPattern = new Regex(@"^[А-ЯЁ][а-яё]*((-|')([А-ЯЁ]))?((\s[а-яё]{2,3}\s)([А-ЯЁ]))?[а-яё]*$");
        private static Regex issnPattern = new Regex(@"^ISSN [0-9]{4}-[0-9]{4}$");
        private static Regex countryPattern = new Regex(@"^([A-Z]*)$|^([А-ЯЁ]*)$|^([A-Z][a-z]*)$|^([А-ЯЁ][а-яё]*)$");

        public static List<string> CheckName(string name)
        {
            List<string> errorList = null;
            
            if (string.IsNullOrEmpty(name))
            {
                errorList = new List<string> { "Name mustn't be a null or empty" };                
            }

            if (name.Length > nameMaxLength)
            {
                errorList.Add($"Name must be less or equal {nameMaxLength} characters");
            }

            return errorList;
        }

        public static List<string> CheckPublicationPlace(string publicationPlace)
        {
            if (string.IsNullOrEmpty(publicationPlace))
            {
                return null;
            }

            List<string> errorList = null;

            if (publicationPlace.Length > publicationPlaceMaxLength)
            {
                errorList = new List<string> { $"Publication place must contains less or equal {publicationPlaceMaxLength} characters" };               
            }            

            if (!publicationPlacePattern.IsMatch(publicationPlace))
            {
                errorList.Add("Invalid publication place value");
            }

            return errorList;
        }

        public static List<string> CheckPublishingHouse(string publishingHouse)
        {
            if (string.IsNullOrEmpty(publishingHouse))
            {
                return null;
            }

            List<string> errorList = null;

            if (publishingHouse.Length > publishingHouseMaxLength)
            {
                errorList = new List<string> { $"Publishing house must contains less or equal {publishingHouseMaxLength} characters" };
            }

            return errorList;
        }

        public static List<string> CheckPublicationYear(short? publicationYear)
        {
            if (!publicationYear.HasValue)
            {
                return null;
            }

            List<string> errorList = null;

            if (publicationYear > DateTime.Now.Year)
            {
                errorList = new List<string> { "Publication year must be less or equal current year" };
            }

            return errorList;
        }

        public static List<string> CheckNumberOfPages(int? numberOfPages)
        {
            if (!numberOfPages.HasValue)
            {
                return null;
            }

            List<string> errorList = null;

            if (numberOfPages.Value < 0)
            {
                errorList = new List<string> { "Number of pages must be a positive number" };
            }

            return errorList;
        }

        public static List<string> CheckNotes(string notes)
        {
            if (string.IsNullOrEmpty(notes))
            {
                return null;
            }

            List<string> errorList = null;

            if (notes.Length > notesMaxLength)
            {
                errorList = new List<string> { $"Notes must contain less or equal {notesMaxLength} characters" };
            }

            return errorList;
        }

        public static List<string> CheckISBN(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
            {
                return null;
            }

            List<string> errorList = null;

            string match = isbnPattern.Match(isbn).Value;            

            if (string.IsNullOrEmpty(match))
            {
                errorList = new List<string> { $"Invalid ISBN value" };
                return errorList;
            }

            int len = match.Remove(0, 5).Replace("-", string.Empty).Length;

            if (len != isbnLength)
            {
                errorList = new List<string> { $"Invalid ISBN value. It contains {isbnLength} digit" };
            }

            return errorList;
        }

        public static List<string> CheckAuthors(IEnumerable<Author> authors)
        {
            if (authors == null)
            {
                return null;
            }

            List<string> errorList = null;

            foreach (var author in authors)
            {
                var errors = CheckAuthorFirstName(author.FirstName);

                if (errors != null)
                {
                    errorList = errorList ?? new List<string>();
                    errorList.AddRange(errors);
                }

                errors = CheckAuthorLastName(author.LastName);

                if (errors != null)
                {
                    errorList = errorList ?? new List<string>();
                    errorList.AddRange(errors);
                }              

                if (errorList != null && errorList.Any())
                {
                    break;
                }
            }

            return errorList;
        }

        public static List<string> CheckAuthorFirstName(string firstName)
        {
            if (string.IsNullOrEmpty(firstName))
            {
                return null;
            }

            List<string> errorList = new List<string>();

            if (firstName.Length > firstNameMaxLength)
            {
                errorList.Add($"First name must contains less or equal {firstNameMaxLength} characters");
            }            

            if (!firstNamePattern.IsMatch(firstName))
            {
                errorList.Add("Invalid first name");
            }

            if (errorList.Any())
            {
                return errorList;
            }
            else
            {
                return null;
            }
        }

        public static List<string> CheckAuthorLastName(string lastName)
        {
            if (string.IsNullOrEmpty(lastName))
            {
                return null;
            }

            List<string> errorList = null;

            if (lastName.Length > lastNameMaxLength)
            {
                errorList = new List<string> { $"LastName must contains less or equal {lastNameMaxLength} characters" };
            }

            if (!(lastNameEnPattern.IsMatch(lastName) || lastNameRuPattern.IsMatch(lastName)))
            {
                errorList.Add("Invalid last name");
            }

            return errorList;
        }

        public static List<string> CheckIssueDate(DateTime? issueDate, short? publicationYear)
        {
            if (issueDate == null || publicationYear == null)
            {
                return null;
            }

            List<string> errorList = null;

            if (issueDate.Value.Year != publicationYear.Value)
            {
                errorList = new List<string> { "Issue date don't match to publication year's value" };               
            }

            return errorList;
        }

        public static List<string> CheckISSN(string issn)
        {
            if (string.IsNullOrEmpty(issn))
            {
                return null;
            }

            List<string> errorList = null;

            if (!issnPattern.IsMatch(issn))
            {
                errorList = new List<string> { "Invalid ISSN value" };              
            }

            return errorList;
        }

        public static List<string> CheckRegistrationNumber(int? number)
        {
            List<string> errorList = new List<string>();

            if (!number.HasValue)
            {
                errorList.Add("Registration number is required");

                return errorList;
            }

            if (number.Value.ToString().Length > 9)
            {
                errorList.Add("Number must be contains less or equal 9 digit");
            }

            if (errorList.Any())
            {
                return errorList;
            }
            else
            {
                return null;
            }
        }

        public static List<string> CheckDateOfApplication(DateTime? dateOfApplication)
        {
            if (!dateOfApplication.HasValue)
            {
                return null;
            }

            List<string> errorList = new List<string>();

            if (dateOfApplication.Value > DateTime.Now)
            {
                errorList.Add("Application date must be less or equal then current date");
            }

            if (dateOfApplication.Value.Year < 1475)
            {
                errorList.Add("Date must be > 1473");
            }

            if (errorList.Any())
            {
                return errorList;
            }
            else
            {
                return null;
            }
        }

        public static List<string> CheckPublishingDate(DateTime? publishingDate, DateTime? dateOfApplication)
        {
            if (!(publishingDate.HasValue || dateOfApplication.HasValue))
            {
                return null;
            }

            List<string> errorList = new List<string>();

            if (publishingDate.Value > DateTime.Now)
            {
                errorList.Add("Publishing date must be less or equal then current date");              
            }

            if (publishingDate.Value < dateOfApplication.Value)
            {
                errorList.Add("Publishing date must be bigger or equal then date of application");
            }

            if (publishingDate.Value.Year < 1475)
            {
                errorList.Add("Date must be > 1473");
            }

            if (errorList.Any())
            {
                return errorList;
            }
            else
            {
                return null;
            }            
        }

        public static List<string> CheckCountry(string country)
        {
            if (string.IsNullOrEmpty(country))
            {
                return null;
            }

            List<string> errorList = null;

            if (country.Length > countryMaxLength)
            {
                errorList = new List<string> { $"Country must contains less or equal {countryMaxLength} characters" };      
            }

            if (!countryPattern.IsMatch(country))
            {
                errorList.Add("Invalid country value");
            }

            return errorList;
        }
    }
}
