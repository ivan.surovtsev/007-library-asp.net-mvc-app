﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Rokolabs.Library.Validators
{
    public class PatentsValidator : ILibraryValidator
    {
        private Dictionary<string, List<string>> errors;

        public PatentsValidator()
        {
            errors = new Dictionary<string, List<string>>();
        }

        public List<string> this[string propertyName]
        {
            get
            {
                if (errors.ContainsKey(propertyName))
                {
                    return errors[propertyName];
                }

                return null;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                if (!errors.ContainsKey(propertyName))
                {
                    errors.Add(propertyName, value);
                }
                else
                {
                    errors[propertyName].AddRange(value);
                }
            }
        }

        public bool IsValid { get => !errors?.Any() ?? false; }

        public void Check(LibraryEntry entry)
        {
            if (entry == null || !(entry is Patent))
            {
                return;
            }

            errors.Clear();

            Patent patent = entry as Patent;

            this["Name"] = LibraryEntryValidatorsRules.CheckName(patent.Name);            
            this["NumberOfPages"] = LibraryEntryValidatorsRules.CheckNumberOfPages(patent.NumberOfPages);
            this["Notes"] = LibraryEntryValidatorsRules.CheckNotes(patent.Notes);
            this["RegistrationNumber"] = LibraryEntryValidatorsRules.CheckRegistrationNumber(patent.RegistrationNumber);
            this["DateOfApplication"] = LibraryEntryValidatorsRules.CheckDateOfApplication(patent.DateOfApplication);
            this["PublishingDate"] = LibraryEntryValidatorsRules.CheckPublishingDate(patent.PublishingDate, patent.DateOfApplication);
            this["Country"] = LibraryEntryValidatorsRules.CheckCountry(patent.Country);
            this["Authors"] = LibraryEntryValidatorsRules.CheckAuthors(patent.Authors);
        }
    }
}
