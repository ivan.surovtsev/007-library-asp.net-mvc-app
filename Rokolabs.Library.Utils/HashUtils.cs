﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Rokolabs.Library.Utils
{
    public static class HashUtils
    {
        public static string GetHash(string inputValue)
        {
            byte[] hash;

            using (var sha512 = SHA512.Create())
            {
                hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(inputValue));
            }

            return Convert.ToBase64String(hash);
        }
    }
}
