﻿using Rokolabs.Library.Entities;
using System.Collections.Generic;

namespace Rokolabs.Library.Comparers
{
    public class NewsPaperEqualityComparer : IEqualityComparer<Newspaper>
    {
        public bool Equals(Newspaper n1, Newspaper n2)
        {
            if (n2 == null && n1 == null)
                return true;
            else if (n1 == null || n2 == null)
                return false;
            else if (n1.Name == n2.Name)
                return true;
            else
                return false;
        }

        public int GetHashCode(Newspaper newspaper)
        {
            return newspaper.Name.GetHashCode();
        }
    }
}
